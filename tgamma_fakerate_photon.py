# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.

import pepper
import awkward as ak
import logging
from functools import partial
from copy import copy
import numpy as np
from config_topgamma import ConfigTopGamma
from pepper.scale_factors import (TopPtWeigter, PileupWeighter, BTagWeighter,
                                  get_evaluator, ScaleFactors)

from tools import gen_col_defs

from processor_basic import BasicFuncs

logger = logging.getLogger(__name__)

class Processor(BasicFuncs):

    config_class = ConfigTopGamma
    
    def __init__(self, config, eventdir):
        
        super().__init__(config, eventdir)
        config["histogram_format"] = "root"
        
    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, definine objects and/or
        # compute event weights
        era = self.get_era(selector.data, is_mc)

        if dsname.startswith("TTTo"):
            selector.set_column("gent_lc", self.gentop, lazy=True)
            if "top_pt_reweighting" in self.config:
                selector.add_cut(
                    "Top_pt_reweighting", self.do_top_pt_reweighting,
                    no_callback=True)

        if not is_mc:
            selector.add_cut("Lumi", partial(
                self.good_lumimask, is_mc, dsname))

        # apply MET filter
        selector.add_cut("MET_filters", partial(self.met_filters, is_mc))

        # apply the number of good PV is at least 1 
        selector.add_cut("atLeastOnePV", self.add_PV_cut)

        if is_mc and "pileup_reweighting" in self.config:
            selector.add_cut("Pileup_reweighting", partial(
                self.do_pileup_reweighting, dsname))

        if is_mc:
            selector.set_column("GenLepton", partial(gen_col_defs.build_genlepton_column, is_mc))#for info in removal procedure
            selector.set_column("GenPhoton", partial(gen_col_defs.build_genphoton_column, is_mc))#for info in removal procedure

        if self.config["compute_systematics"] and is_mc:
            self.add_generator_uncertainies(dsname, selector)        

        pos_triggers, neg_triggers = pepper.misc.get_trigger_paths_for(
            dsname, is_mc, self.config["dataset_trigger_map"],
            self.config["dataset_trigger_order"])
#        selector.add_cut("Trigger", partial(
#            self.passing_trigger, pos_triggers, neg_triggers))

        if is_mc and self.config["year"] in ("2016", "2017", "ul2016pre","ul2016post", "ul2017"):
            selector.add_cut("L1_prefiring", self.add_l1_prefiring_weights)

        # Pick electrons satisfying our criterias
        selector.set_multiple_columns(self.pick_electrons)
        # Pick muons satisfying our criterias
        selector.set_multiple_columns(self.pick_muons)

	# Combine electron and muon to lepton
        selector.set_column("Lepton", partial(self.build_lepton_column, is_mc, selector.rng))
        selector.add_cut("OneLep",self.one_lepton)

        # Define lepton categories, the number of lepton cut applied here
        selector.set_multiple_columns(self.lepton_categories)
        selector.set_cat("channel",{"ele", "muon"})

        selector.add_cut("pass_trig_muon",partial(self.passing_hlt,self.config['trigger_muon_path']),categories={"channel": ["muon"]})
        selector.add_cut("pass_trig_ele",partial(self.passing_hlt,self.config['trigger_ele_path']),categories={"channel": ["ele"]})
        selector.add_cut("exact_one_muon",self.exact_one_muon,categories={"channel": ["muon"]})
        selector.add_cut("exact_one_ele",self.exact_one_ele,categories={"channel": ["ele"]})

        # Only accept MC events with prompt lepton 
        if is_mc:
           selector.add_cut("isPromptLepton", self.isprompt_lepton)
           selector.set_column("Lepton_isPrompt", self.build_lepton_prompt)

        selector.add_cut("muon_sf",partial(self.apply_muon_sf, is_mc))
        selector.add_cut("electron_sf",partial(self.apply_electron_sf, is_mc))

        selector.set_column("Lepton_charge", self.lepton_charge)

        # Pick photons satisfying our criterias
        selector.set_column("Photon", self.pick_loose_photons)

        if (is_mc and self.config["compute_systematics"]
                and dsname not in self.config["dataset_for_systematics"]):
            if hasattr(filler, "sys_overwrite"):
                assert filler.sys_overwrite is None
            for variarg in self.get_jetmet_variation_args():
                selector_copy = copy(selector)
                filler.sys_overwrite = variarg.name
                self.process_selection_jet_part(selector_copy, is_mc,
                                                variarg, dsname, filler, era)
                if self.eventdir is not None:
                    logger.debug(f"Saving per event info for variation"
                                 f" {variarg.name}")
                    self.save_per_event_info(
                        dsname + "_" + variarg.name, selector_copy, False)
            filler.sys_overwrite = None

        # Jet nominal
        self.process_selection_jet_part(selector, is_mc,
                                        self.get_jetmet_nominal_arg(),
                                        dsname, filler, era)
        logger.debug("Selection done")

    def process_selection_jet_part(self, selector, is_mc, variation, dsname,
                                   filler, era):

        # Pick Jets satisfying our criterias
        logger.debug(f"Running jet_part with variation {variation.name}")
        reapply_jec = ("reapply_jec" in self.config
                       and self.config["reapply_jec"])
        # comput jetfac from jer
        selector.set_multiple_columns(partial(
            self.compute_jet_factors, is_mc, reapply_jec, variation.junc,
            variation.jer, selector.rng))

        selector.set_column("OrigJet", selector.data["Jet"])
        selector.set_column("Jet", partial(self.build_jet_column, is_mc))
        if "jet_puid_sf" in self.config and is_mc:
            selector.add_cut("JetPUIdSFs", partial(self.compute_jet_puid_sfs,is_mc))
        selector.set_column("Jet", self.jets_with_puid)
        selector.set_column("CentralJet", self.build_centraljet_column)
        selector.set_column("ForwardJet", self.build_forwardjet_column)

        smear_met = "smear_met" in self.config and self.config["smear_met"]
        selector.set_column(
            "MET", partial(self.build_met_column, is_mc, variation.junc,
                           variation.jer if smear_met else None, selector.rng,
                           era, variation=variation.met))
                
        selector.set_column("bJet", self.build_bjet_column)
        selector.set_column("nbtag", self.num_btags)
        selector.set_column("njet", self.num_jets)

        #do first cuts: overlap removal and >=2 leptons
        selector.add_cut("removeTTNLO_pow_overlap", partial(self.remove_ttnlo_powheg_overlap,dsname))
        selector.add_cut("removeSToverlap", partial(self.remove_stnlo_overlap,dsname))
        selector.add_cut("removeZoverlap", partial(self.remove_z_overlap,dsname))
        selector.add_cut("removeWoverlap", partial(self.remove_w_overlap,dsname))
        #selector.add_cut("removeTToverlap", partial(self.remove_ttlo_overlap,dsname))
        #selector.add_cut("removeSTWoverlap", partial(self.remove_stW_overlap,dsname))
        
        selector.add_cut("atLeastOnePhoton",self.one_good_photon)
        selector.add_cut("photon_sf",partial(self.apply_photon_sf, is_mc))
        selector.add_cut("psv_sf",partial(self.apply_psv_sf, is_mc))
        selector.set_column("chiso", self.build_chiso) 
        selector.set_column("sieie", self.build_sieie) 

        #SPLIT ALL MC IN NONPROMPT AND PROMPT PHOTON CONTRIBUTIONS
        if is_mc:
            selector.set_multiple_columns(self.photon_categories)
            selector.set_cat("photon_type",{"prompt","ele_matched","nonprompt","allphoton"},safe=False)
        else:
           selector.set_multiple_columns(self.photon_categories_data)
           selector.set_cat("photon_type", {'allphoton'} )

        selector.add_cut("preselection", self.dummycut)

        #ONLY FOR COMPUTING RATIO
        selector.set_multiple_columns(self.ABCDmethod_masks)
        selector.set_cat("ABCD", {'A','B','C','D','C1','D1','C2','D2','appl','meas'},safe=False)

        selector.add_cut("finalselection", self.dummycut)

    def one_lepton(self,data):
        return (ak.num(data["Lepton"])>0)

    def exact_one_ele(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nele==1) & (nmuon==0)

        return accept

    def exact_one_muon(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nmuon==1) & (nele==0)

        return accept

    def lepton_categories(self,data):
        cat = {}
        leps = data["Lepton"]
        cat['ele']  =  (abs(leps[:, 0].pdgId) == 11)
        cat['muon'] =  (abs(leps[:, 0].pdgId) == 13)

        return cat

    def one_good_photon(self,data):
        return ak.num(data["Photon"])>0

    def build_chiso(self,data):
        photons = data["Photon"]
        chiso = photons[:,0]["pfRelIso03_chg"]*photons[:,0]["pt"]
        return chiso

    def build_sieie(self,data):
        photons = data["Photon"]
        sieie = photons[:,0]["sieie"]
        return sieie

    def ABCDmethod_masks(self, data):

        photons = data["Photon"]
        isBarrel = photons[:,0]["isScEtaEB"]
        sieie = photons[:,0]["sieie"]
        chIso = photons[:,0]["pfRelIso03_chg"]*photons[:,0]["pt"]

        atleast1j = ak.num(data["Jet"]) >= 1
        atleast2j = ak.num(data["Jet"]) >= 2
        atleast1b = (ak.sum(data["Jet"]["btagged"], axis=1) >= 1)

        #chIso medium 1.141 for B and 1.051 for E
        #sieie medium 0.01015 for B and 0.0272 for E

        is_appl = (ak.where(isBarrel,(chIso < 1.141),(chIso < 1.051)))  

        is_meas = (ak.where(isBarrel,(chIso > 1.141),(chIso > 1.051))) 
        
        is_A = (is_appl) & (ak.where(isBarrel,(sieie < 0.01015),(sieie < 0.0272)))# SR
        is_B = (is_appl) & (ak.where(isBarrel,(sieie > 0.0110) ,(sieie > 0.028))) # leave a gap for other regions
        is_C = (is_meas) & (ak.where(isBarrel,(sieie < 0.01015),(sieie < 0.0272)))# 
        is_D = (is_meas) & (ak.where(isBarrel,(sieie > 0.0110) ,(sieie > 0.028)))

        is_C1 =  (chIso < 10) & ( chIso > 4) & (ak.where(isBarrel,(sieie < 0.01015),(sieie < 0.0272)))# for uncertainty
        is_D1 =  (chIso < 10) & ( chIso > 4) & (ak.where(isBarrel,(sieie > 0.0110) ,(sieie > 0.028))) # for uncertainty
        is_C2 =  (chIso < 4 ) & ( chIso > 1.5) & (ak.where(isBarrel,(sieie < 0.01015),(sieie < 0.0272)))# for uncertainty
        is_D2 =  (chIso < 4 ) & ( chIso > 1.5) & (ak.where(isBarrel,(sieie > 0.0110) ,(sieie > 0.028))) # for uncertainty
        
        region = {}
        region['A'] = (is_A)
        region['B'] = (is_B)
        region['C'] = (is_C)
        region['D'] = (is_D)
        region['C1'] = (is_C1)
        region['D1'] = (is_D1)
        region['C2'] = (is_C2)
        region['D2'] = (is_D2)
        region['appl'] = (is_A | is_B)
        region['meas'] = (is_C | is_D)
    
        return region

    def isprompt_lepton(self, data):
        promptmatch = self.lepton_isprompt(data)
        n_prompt_lep = ak.sum(promptmatch,axis=1)
        accept = n_prompt_lep > 0

        return accept

    def lepton_isprompt(self,data):
        lepton = data["Lepton"]
        genpart = data["GenPart"]

        genmatchID = lepton.genPartIdx[(lepton.genPartIdx!=-1)]
        matched_genlepton = genpart[genmatchID]
        promptmatch =  matched_genlepton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | ( matched_genlepton.hasFlags(['isPromptTauDecayProduct'])) |
                        ( matched_genlepton.hasFlags(["fromHardProcess"])))

        return promptmatch

    def apply_electron_sf(self,is_mc,data):
        if is_mc and ("electron_sf" in self.config
                       and len(self.config["electron_sf"]) > 0):
           weight, systematics = self.compute_electron_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_muon_sf(self,is_mc,data):
        if is_mc and ("muon_sf" in self.config
                       and len(self.config["muon_sf"]) > 0):
           weight, systematics = self.compute_muon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_psv_sf(self,is_mc,data):
        if is_mc and ("psv_sf" in self.config
                       and len(self.config["psv_sf"]) > 0):
           weight, systematics = self.compute_psv_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_photon_sf(self,is_mc,data):
        if is_mc and ("photon_sf" in self.config
                       and len(self.config["photon_sf"]) > 0):
           weight, systematics = self.compute_photon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_btag_sf(self, is_mc, data):
        """Apply btag scale factors."""
        if is_mc and (
                "btag_sf" in self.config and len(self.config["btag_sf"]) != 0):
            weight, systematics = self.compute_weight_btag(data)
            return weight, systematics
        else:
            return np.ones(len(data))

