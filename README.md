# tgamma_singleLep



## Getting started (for testing code)
``git clone https://gitlab.cern.ch/pepper/pepper.git``<br>
``git clone https://gitlab.cern.ch/yian/tgamma_singlelep.git``<br>
``cd tgamma_singlelep``<br>
``source environment.sh``<br>
``python3 -m pepper.runproc tgamma_processor.py config/test_config.json --dataset TGJets_TuneCP5_13TeV-amcatnlo-madspin-pythia8 -R --out output``<br>
