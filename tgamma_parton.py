# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.

import pepper
import awkward as ak
import logging
from functools import partial
from copy import copy
import numpy as np
from tools.utils import DeltaR
from tools import top_reco
from tools import gen_col_defs

logger = logging.getLogger(__name__)

class Processor(pepper.ProcessorBasicPhysics):
    config_class = pepper.ConfigTTbarLL
    def __init__(self, config, eventdir):
        super().__init__(config, eventdir)
        config["histogram_format"] = "root"
        
    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, definine objects and/or
        # compute event weights
        
        era = self.get_era(selector.data, is_mc)
        # Implement the selection steps: add cuts, definine objects and/or
        # compute event weights
        if self.config["compute_systematics"] and is_mc:
            self.add_generator_uncertainies(dsname, selector)
        #MEPhotons
        if is_mc:

            selector.set_column("GenPhoton", partial(gen_col_defs.build_genphoton_column, is_mc))#for info in removal procedure
            selector.set_column("ParticleLepton", partial(gen_col_defs.build_particle_lepton_column, is_mc))
            selector.set_column("ParticlePhoton", partial(gen_col_defs.build_particle_isolated_genphoton, is_mc))
            selector.set_column("ParticleJet", partial(gen_col_defs.build_particle_jet_column, is_mc))

            selector.add_cut("filteronelepton", self.filter_onelepton)
            selector.set_multiple_columns(self.lepton_cats)
            selector.set_cat("channel",{"ele", "muon"})

            selector.add_cut("Cut_flow", self.dummy)
            selector.add_cut("removeSToverlap", partial(self.remove_stnlo_overlap,dsname))
            selector.add_cut("removeTToverlap", partial(self.remove_ttnlo_overlap,dsname))
            selector.add_cut("1Photon", self.one_gen_photon)

            selector.set_column("ParticleMlg", partial(self.build_genmlg_column, is_mc))

            # the ParticlePhoton collection is reqired to be prompt, don't need prompt cut here
            #selector.set_multiple_columns(self.genphoton_categories)
            #selector.set_cat("genphoton_type", {'Prompt','Nonprompt','Others'} )

            selector.add_cut("2Jet",self.atLeast2jet)
            
            selector.set_column("genDRphoLep",self.build_genDRphoLep)
            selector.set_column("genDRphoJet",self.build_genDRphoJet)
            selector.set_column("genDRlepJet1",self.build_genDRlepJet1)
            selector.set_column("genDRlepJet2",self.build_genDRlepJet2)

        logger.debug("Selection done")

    def filter_onelepton(self,data):

        genlep = data["ParticleLepton"]

        return (ak.num(genlep) == 1)

    def lepton_cats(self,data):
        genlepton = data["ParticleLepton"]
        cat = {}
        cat['ele'] = abs(genlepton[:,0].pdgId)==11
        cat['muon'] = abs(genlepton[:,0].pdgId)==13

        return cat

    def one_gen_photon(self,data):
        genphoton = data["ParticlePhoton"]
        accept = (ak.num(genphoton) >= 1)
        return accept

    def atLeast2jet(self,data):
        return ak.num(data["ParticleJet"])>=2
 
    def build_genmlg_column(self, is_mc, data):
        if is_mc:
            genlepton = ak.pad_none(data["ParticleLepton"],1,axis=1)
            genphoton = ak.pad_none(data["ParticlePhoton"],1,axis=1)
            gen_Mlg = (genlepton[:,0]+genphoton[:,0]).mass
            gen_Mlg = ak.fill_none(gen_Mlg, 0)
            #gen_Mlg = ak.unflatten(gen_Mlg, 1)
            #gen_Mlg = ak.fill_none(gen_Mlg,0,axis=1)

        return gen_Mlg

    def genphoton_categories(self,data):

        cat = {}
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]

        promptmatch = genphoton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | (genphoton.hasFlags(['isPromptTauDecayProduct'])) |
                        (genphoton.hasFlags(["fromHardProcess"])))    
        prompt_photon = genphoton[promptmatch]
        nonprompt_photon = genphoton[~promptmatch]
        
        cat['Prompt'] =  (ak.num(prompt_photon)>0)
        cat['Nonprompt'] = ( (ak.num(prompt_photon)==0) & (ak.num(nonprompt_photon)>0) )
        cat['Others'] = ( (ak.num(prompt_photon)==0) & (ak.num(nonprompt_photon)==0) )
 
        return cat

    def remove_ttnlo_overlap(self,dsname, data):
    
        if (not "TTTo" in dsname and not dsname.startswith("TTGJets") ) :
            accept = ak.num(data["ParticleLepton"]) > 0
            return accept
    
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
   
        # 'isPrompt': nor from hadron, muon or tau decay
        promptmatch = genphoton.hasFlags(['isPrompt'])

        mother = genphoton.parent
        not_from_top = (genphoton['pdgId']==22) #always from True 
        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
            mother = mother.parent

        mother = genphoton.parent
        from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
        from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
                 & (~not_from_top) )
        from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
                 & (~not_from_top) )

        from_decayProd = (from_lepton | from_W | from_b)

        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.05, axis=2)
    
        genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close]
    
        if "TTTo" in dsname :
            accept = (ak.num(genphoton)==0)
        elif dsname.startswith("TTGJets"):
            accept = (ak.num(genphoton)>0)
        return accept

    def remove_stnlo_overlap(slef,dsname, data):
        if (not "ST_t-channel" in dsname and not dsname.startswith("TGJets_lepton")) :
            accept = ak.num(data["ParticleLepton"]) > 0
            return accept

        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
  
        # 'isPrompt': nor from hadron, muon or tau decay
        promptmatch = genphoton.hasFlags(['isPrompt'])

        mother = genphoton.parent
        not_from_top = (genphoton['pdgId']==22) #always from True 
        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
            mother = mother.parent

        #mother = genphoton.parent
        #not_from_tau = (genphoton['pdgId']==22) #always from True 
        #while not ak.all(ak.is_none(mother, axis=1)):
        #    not_from_tau = (not_from_tau & (ak.fill_none(abs(mother["pdgId"])!= 15, True) ))
        #    mother = mother.parent

        mother = genphoton.parent
        from_lepton = ( ( ((ak.fill_none(abs(mother["pdgId"]), 0)==11) ) |
                          ((ak.fill_none(abs(mother["pdgId"]), 0)==13) ) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
        from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
                 & (~not_from_top) )
        from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
                 & (~not_from_top) )

        from_decayProd = (from_lepton | from_W | from_b)

        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.05, axis=2)
    
        genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close] 

        if "ST_t-channel" in dsname :
            accept = (ak.num(genphoton)==0)
        elif dsname.startswith("TGJets_lepton") :
            accept = (ak.num(genphoton)>0)
        return accept

    def build_genDRphoLep(self,data):

        genphoton = data["ParticlePhoton"]
        genlepton = data["ParticleLepton"]
        genDRphoLep = DeltaR(genphoton[:,0],genlepton[:,0])

        return genDRphoLep

    def build_genDRphoJet(self,data):

        genphoton = data["ParticlePhoton"]
        genjet = data["GenJet"]
        genDRphoJet = DeltaR(genphoton[:,0],genjet[:,0])

        return genDRphoJet

    def build_genDRphobJet(self,data):

        genphoton = data["ParticlePhoton"]
        genbjet = data["GenbJet"]
        genDRphobJet = DeltaR(genphoton[:,0],genbjet[:,0])

        return genDRphobJet

    def build_genDRlepJet1(self,data):

        genlep = data["ParticleLepton"]
        genjet = data["ParticleJet"]
        genDRlepJet = DeltaR(genlep[:,0],genjet[:,0])

        return genDRlepJet

    def build_genDRlepJet2(self,data):

        genlep = data["ParticleLepton"]
        genjet = data["ParticleJet"]
        genDRlepJet = DeltaR(genlep[:,0],genjet[:,1])

        return genDRlepJet

    def apply_genpho_isolation(self, data):

        genphotons = data["GenPhoton"]
        genpart = data["GenPart"]

        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

        relevant_part = genpart[is_relevant_part]

        has_part_close = ak.any(
                genphotons.metric_table(relevant_part) < 0.1, axis=2)

        genlepton = data["ParticleLepton"]
        has_lep_close = ak.any(genphotons.metric_table(genlepton) < 0.4, axis=2)

        genphotons = genphotons[~has_part_close & ~has_lep_close]

        accept = (ak.num(genphotons)>0)

        return accept

    def dummy(self,data):

        genlep = data["ParticleLepton"]
        return (ak.num(genlep) >= 1)
