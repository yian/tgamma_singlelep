# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.
# Here we create a very simplified version of the ttbar-to-dilep processor.
# One can run this processor using
# 'python3 -m pepper.runproc --debug example_processor.py example_config.json'
# Above command probably will need a little bit of time before all cuts are
# applied once. This is because a chunk of events are processed simultaneously.
# You change adjust the number of events in a chunk and thereby the memory
# usage by using the --chunksize parameter (the default value is 500000).

import pepper
import awkward as ak
import logging
from functools import partial
from copy import copy
import numpy as np
from config_topgamma import ConfigTopGamma
from pepper.scale_factors import (TopPtWeigter, PileupWeighter, BTagWeighter,
                                  get_evaluator, ScaleFactors)
from tools import gen_col_defs
from processor_basic import BasicFuncs

logger = logging.getLogger(__name__)

# All processors should inherit from pepper.ProcessorBasicPhysics
class Processor(BasicFuncs):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = ConfigTopGamma

    def __init__(self, config, eventdir):
        # Initialize the class, maybe overwrite some config variables and
        # load additional files if needed
        # Can set and modify configuration here as well
#        config["histogram_format"] = "root"
        # Need to call parent init to make histograms and such ready
        config["histogram_format"] = "root"

        super().__init__(config, eventdir)
        # It is not recommended to put anything as member variable into a
        # a Processor because the Processor instance is sent as raw bytes
        # between nodes when running on HTCondor.

    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, define objects and/or
        # compute event weights

        # Add a cut only allowing events according to the golden JSON
        # The good_lumimask method is specified in pepper.ProcessorBasicPhysics
        # It also requires a lumimask to be specified in config
        era = self.get_era(selector.data, is_mc)

        if dsname.startswith("TTTo"):
            selector.set_column("gent_lc", self.gentop, lazy=True)
            if "top_pt_reweighting" in self.config:
                selector.add_cut(
                    "Top_pt_reweighting", self.do_top_pt_reweighting,
                    no_callback=True)

        if is_mc:
            selector.set_column("GenLepton", partial(gen_col_defs.build_genlepton_column, is_mc))
            selector.set_column("GenPhoton", partial(gen_col_defs.build_genphoton_column, is_mc))

        if not is_mc:
            selector.add_cut("Lumi", partial(
                self.good_lumimask, is_mc, dsname))

        # apply MET filter
        selector.add_cut("MET_filters", partial(self.met_filters, is_mc))

        # apply the number of good PV is at least 1 
        selector.add_cut("atLeastOnePV", self.add_PV_cut)

        if is_mc and "pileup_reweighting" in self.config:
            selector.add_cut("Pileup_reweighting", partial(
                self.do_pileup_reweighting, dsname))

        if self.config["compute_systematics"] and is_mc:
            self.add_generator_uncertainies(dsname, selector)

        # Only allow events that pass triggers specified in config
        # This also takes into account a trigger order to avoid triggering
        # the same event if it's in two different data datasets.
        pos_triggers, neg_triggers = pepper.misc.get_trigger_paths_for(
            dsname, is_mc, self.config["dataset_trigger_map"],
            self.config["dataset_trigger_order"])
#        selector.add_cut("Trigger", partial(
#            self.passing_trigger, pos_triggers, neg_triggers))
    
        if is_mc and self.config["year"] in ("2016", "2017", "ul2016pre","ul2016post", "ul2017"):
            selector.add_cut("L1_prefiring", self.add_l1_prefiring_weights)

        # HEM issue cut
        if (self.config["hem_cut_if_ele"] or self.config["hem_cut_if_muon"]
                or self.config["hem_cut_if_jet"]):
            selector.add_cut("HEM_cut", self.hem_cut)

        # Pick electrons satisfying our criterias
        selector.set_multiple_columns(self.pick_electrons)
        # Pick muons satisfying our criterias
        selector.set_multiple_columns(self.pick_muons)

        # Combine electron and muon to lepton
        selector.set_column("Lepton", partial(self.build_lepton_column, is_mc, selector.rng))
        selector.add_cut("OneLep",self.one_lepton)

        # Define lepton categories
        selector.set_multiple_columns(self.lepton_categories)
        selector.set_cat("channel",{"ele", "muon"})

        selector.add_cut("pass_trig_muon",partial(self.passing_hlt,self.config['trigger_muon_path']),categories={"channel": ["muon"]})
        selector.add_cut("pass_trig_ele",partial(self.passing_hlt,self.config['trigger_ele_path']),categories={"channel": ["ele"]})
        selector.add_cut("exact_one_muon",self.exact_one_muon,categories={"channel": ["muon"]})
        selector.add_cut("exact_one_ele",self.exact_one_ele,categories={"channel": ["ele"]})

        selector.add_cut("muon_sf",partial(self.apply_muon_sf, is_mc))
        selector.add_cut("electron_sf",partial(self.apply_electron_sf, is_mc))

        selector.set_column("Lepton_charge", self.lepton_charge)

        # Pick photons satisfying our criterias
        selector.set_column("Photon", self.pick_medium_photons)

        # JME unc
        if (is_mc and self.config["compute_systematics"]
                and dsname not in self.config["dataset_for_systematics"]):
            if hasattr(filler, "sys_overwrite"):
                assert filler.sys_overwrite is None
            for variarg in self.get_jetmet_variation_args():
                selector_copy = copy(selector)
                filler.sys_overwrite = variarg.name
                self.process_selection_jet_part(selector_copy, is_mc,
                                                variarg, dsname, filler, era)
                if self.eventdir is not None:
                    logger.debug(f"Saving per event info for variation"
                                 f" {variarg.name}")
                    self.save_per_event_info(
                        dsname + "_" + variarg.name, selector_copy, False)
            filler.sys_overwrite = None

        #JME selection -> remove for photon category
        self.process_selection_jet_part(selector, is_mc,
                                        self.get_jetmet_nominal_arg(),
                                        dsname, filler, era)
        logger.debug("Selection done")

    def process_selection_jet_part(self, selector, is_mc, variation, dsname,
                                   filler, era):

        # Pick Jets satisfying our criterias
        logger.debug(f"Running jet_part with variation {variation.name}")
        reapply_jec = ("reapply_jec" in self.config
                       and self.config["reapply_jec"])
        # comput jetfac from jer 
        selector.set_multiple_columns(partial(
            self.compute_jet_factors, is_mc, reapply_jec, variation.junc,
            variation.jer, selector.rng))

        selector.set_column("OrigJet", selector.data["Jet"])
        selector.set_column("Jet", partial(self.build_jet_column, is_mc))
        if "jet_puid_sf" in self.config and is_mc:
            selector.add_cut("JetPUIdSFs", partial(self.compute_jet_puid_sfs,is_mc))
        selector.set_column("Jet", self.jets_with_puid)
        selector.set_column("CentralJet", self.build_centraljet_column)
        selector.set_column("ForwardJet", self.build_forwardjet_column)

        smear_met = "smear_met" in self.config and self.config["smear_met"]
        selector.set_column(
            "MET", partial(self.build_met_column, is_mc, variation.junc,
                           variation.jer if smear_met else None, selector.rng,
                           era, variation=variation.met))

        selector.set_column("mTW",self.build_mtw_column)
        selector.set_column("bJet", self.build_bjet_column)
        selector.set_column("loose_bJet", self.build_loose_bjet_column)
        selector.set_column("nbtag", self.num_btags)
        selector.set_column("n_loose_btag", self.num_loose_btags)
        selector.set_column("ncentral_jets", self.num_centralJets)
        selector.set_column("nforward_jets", self.num_forwardJets)

        #do first cuts: overlap removal and >=1 lepton
        selector.add_cut("removeSToverlap", partial(self.remove_stnlo_overlap,dsname))
        selector.add_cut("removeTTNLO_powheg_overlap", partial(self.remove_ttnlo_powheg_overlap,dsname))
        selector.add_cut("removeZoverlap", partial(self.remove_z_overlap,dsname))
        selector.add_cut("removeWoverlap", partial(self.remove_w_overlap,dsname))
        # we don't have StWa single lepton sample, just estimate photon contribution from STW sample
        #selector.add_cut("removeSTWoverlap", partial(self.remove_stW_overlap,dsname))
        #selector.add_cut("removeTToverlap", partial(self.remove_ttlo_overlap,dsname)) # LO removal
        #selector.add_cut("removeTTNLO_mg5_overlap", partial(self.remove_ttnlo_mg5_overlap,dsname))

        # Only accept events that have at least one photon
        selector.add_cut("atLeastOnePhoton",self.one_good_photon)

        selector.set_column("deltaR_lg",self.build_deltaR_lgamma)
        selector.set_column("mlg",self.mass_lg,no_callback=True)

        if is_mc:
           selector.set_multiple_columns(self.photon_categories)
           selector.set_cat("photon_type",{"allphoton","prompt", "prompt_decayProd","prompt_from_lepton","prompt_from_W","prompt_from_b", "prompt_ISR", "nonprompt","hadronic","unmatched","ele_matched"},safe=False)
        else:
           selector.set_multiple_columns(self.photon_categories_data)
           selector.set_cat("photon_type", {'allphoton'} )

        selector.add_cut("preselection", self.dummycut)
        selector.add_cut("photon_sf",partial(self.apply_photon_sf, is_mc))
        selector.add_cut("psv_sf",partial(self.apply_psv_sf, is_mc))

        # Only accept events that have at least two jets and one bjet
        selector.add_cut("atLeast2jet",self.has_jets)
        selector.set_column("deltaR_jg",self.build_deltaR_jgamma)
        selector.set_column("deltaR_lj",self.build_deltaR_ljet)

        #Build different categories according to the number of jets
        selector.set_multiple_columns(self.btag_categories)
        selector.set_cat("jet_btag", {"j2+_b0","j2+_b1+"},safe=False)
        selector.add_cut("btag_sf",partial(self.apply_btag_sf, is_mc))

        # Only accept events with MET pt more than 20 GeV
        selector.add_cut("Req_MET", self.met_requirement)

        # Only accept MC events with prompt lepton 
        if is_mc:
           selector.add_cut("isPromptLepton", self.isprompt_lepton)
           selector.set_column("Lepton_isPrompt", self.build_lepton_prompt)

        selector.add_cut("finalselection", self.dummycut)

    def one_lepton(self,data):
        return (ak.num(data["Lepton"])>0)

    def exact_one_ele(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nele==1) & (nmuon==0)

        return accept

    def exact_one_muon(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nmuon==1) & (nele==0)

        return accept

    def lepton_categories(self,data):
        cat = {}
        leps = data["Lepton"]
        cat['ele']  =  (abs(leps[:, 0].pdgId) == 11)
        cat['muon'] =  (abs(leps[:, 0].pdgId) == 13)

        return cat

    def isprompt_lepton(self, data):
        promptmatch = self.lepton_isprompt(data)
        n_prompt_lep = ak.sum(promptmatch,axis=1)
        accept = n_prompt_lep > 0

        return accept

    def lepton_isprompt(self,data):
        lepton = data["Lepton"]
        genpart = data["GenPart"]

        genmatchID = lepton.genPartIdx[(lepton.genPartIdx!=-1)]
        matched_genlepton = genpart[genmatchID]
        promptmatch =  matched_genlepton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | ( matched_genlepton.hasFlags(['isPromptTauDecayProduct'])) |
                        ( matched_genlepton.hasFlags(["fromHardProcess"])))

        return promptmatch

    def build_lepton_prompt(self,data):
        lepton = data["Lepton"]
        prompt = self.lepton_isprompt(data)
        return ak.sum(prompt,axis=1)>0 

    def one_good_photon(self,data):
        return ak.num(data["Photon"])>0
   
    def mass_lg(self, data):
        """Return invariant mass of lepton plus photon"""
        return (data["Lepton"][:, 0] + data["Photon"][:, 0]).mass

    def z_cut(self,data):
        is_out_window = abs(data['mlg'] - 91.2) > 10
        return is_out_window    

    def photon_categories_data(self,data):
        leptons = data["Lepton"]

        cats = {}
        cats["allphoton"] = (ak.num(leptons)>0)
        return cats   
       
    def photon_categories(self,data):
        cats = {}
        photons = data["Photon"]
        genpart = data["GenPart"]

        # photons matched to a gen Pho
        true_photons = photons[ak.fill_none(abs(photons.matched_gen.pdgId) == 22, False)]
        # photons matched to a gen Ele
        electron_matched = photons[ak.fill_none(abs(photons.matched_gen.pdgId) == 11, False)]
        # photons can't matched to any gen Obj
        unmatched_photons = photons[ak.is_none(photons.matched_gen,axis=1)]

        promptmatch = true_photons.matched_gen.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | (true_photons.matched_gen.hasFlags(['isPromptTauDecayProduct'])) |
                        (true_photons.matched_gen.hasFlags(["fromHardProcess"])))
       
        #DR between gen and reco photon
        dr_reco_gen = ak.any(true_photons.matched_gen.metric_table(true_photons) < 0.3, axis=2)

        promptmatch = promptmatch & dr_reco_gen 

        mother = true_photons.matched_gen.parent
        not_from_top = (true_photons.matched_gen.pdgId==22) #always from True 
        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
            mother = mother.parent


        mother = true_photons.matched_gen.parent 
        #need to use fill_none, since the genPhoton might not have a mother, otherwise the none is selected and satisfy the following case
        from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
        from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
                 & (~not_from_top) )
        from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
                 & (~not_from_top) )

        from_decayProd = (from_lepton | from_W | from_b)

        from_ISR = ~(from_decayProd)

        prompt_photons = true_photons[promptmatch]
        prompt_decayProd = true_photons[promptmatch & from_decayProd]
        prompt_from_lepton = true_photons[promptmatch & from_lepton]
        prompt_from_W = true_photons[promptmatch & from_W]
        prompt_from_b = true_photons[promptmatch & from_b]
        prompt_ISR = true_photons[promptmatch & ~from_decayProd]
        hadronic_photons = true_photons[~(promptmatch)]

        cats["prompt"] = (ak.num(prompt_photons)>0)
        cats["prompt_decayProd"] = (ak.num(prompt_decayProd)>0)
        cats["prompt_from_lepton"] = (ak.num(prompt_from_lepton)>0)
        cats["prompt_from_W"] = (ak.num(prompt_from_W)>0)
        cats["prompt_from_b"] = (ak.num(prompt_from_b)>0)
        cats["prompt_ISR"] = (ak.num(prompt_ISR)>0)
        cats["ele_matched"] =( (ak.num(prompt_photons)==0) & (ak.num(electron_matched)>0) )
        cats["nonprompt"] = ( (ak.num(prompt_photons)==0) & ( (ak.num(hadronic_photons)>0) | (ak.num(unmatched_photons)>0) ) )
        cats["hadronic"] = ( (ak.num(prompt_photons)==0) & ( (ak.num(hadronic_photons)>0) ) )
        cats["unmatched"] = ( (ak.num(prompt_photons)==0) & ( (ak.num(unmatched_photons)>0) ) )
        cats["allphoton"] = (ak.num(photons)>0) 
        return cats

    def btag_categories(self,data):
        cats = {}
        
        num_btagged = data["nbtag"]
        njet = ak.num(data["Jet"])

        cats["j2+_b0"] = (num_btagged == 0) & (njet >= 2)
        cats["j2+_b1+"] = (num_btagged >= 1) & (njet >= 2)

        return cats

    def met_requirement(self, data):
        met = data["MET"].pt
        return met > self.config["met_min_met"]

    def apply_electron_sf(self,is_mc,data):
        if is_mc and ("electron_sf" in self.config
                       and len(self.config["electron_sf"]) > 0):
           weight, systematics = self.compute_electron_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_muon_sf(self,is_mc,data):
        if is_mc and ("muon_sf" in self.config
                       and len(self.config["muon_sf"]) > 0):
           weight, systematics = self.compute_muon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_psv_sf(self,is_mc,data):
        if is_mc and ("psv_sf" in self.config
                       and len(self.config["psv_sf"]) > 0):
           weight, systematics = self.compute_psv_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_photon_sf(self,is_mc,data):
        if is_mc and ("photon_sf" in self.config
                       and len(self.config["photon_sf"]) > 0):
           weight, systematics = self.compute_photon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_btag_sf(self, is_mc, data):
        """Apply btag scale factors."""
        if is_mc and (
                "btag_sf" in self.config and len(self.config["btag_sf"]) != 0):
            weight, systematics = self.compute_weight_btag(data)
            return weight, systematics
        else:
            return np.ones(len(data))

