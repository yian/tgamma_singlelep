# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.
# Here we create a very simplified version of the ttbar-to-dilep processor.
# One can run this processor using
# 'python3 -m pepper.runproc --debug example_processor.py example_config.json'
# Above command probably will need a little bit of time before all cuts are
# applied once. This is because a chunk of events are processed simultaneously.
# You change adjust the number of events in a chunk and thereby the memory
# usage by using the --chunksize parameter (the default value is 500000).

import pepper
import awkward as ak
import math
import logging
from functools import partial
from copy import copy
import numpy as np
from config_topgamma import ConfigTopGamma
from pepper.scale_factors import (TopPtWeigter, PileupWeighter, BTagWeighter,
                                  get_evaluator, ScaleFactors)
from tools import gen_col_defs

from processor_basic import BasicFuncs

logger = logging.getLogger(__name__)

# All processors should inherit from pepper.ProcessorBasicPhysics
class Processor(BasicFuncs):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = ConfigTopGamma

    def __init__(self, config, eventdir):
        # Initialize the class, maybe overwrite some config variables and
        # load additional files if needed
        # Can set and modify configuration here as well
        # Need to call parent init to make histograms and such ready

        config["histogram_format"] = "root"

        super().__init__(config, eventdir)

    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, define objects and/or
        # compute event weights

        # Add a cut only allowing events according to the golden JSON
        # The good_lumimask method is specified in pepper.ProcessorBasicPhysics
        # It also requires a lumimask to be specified in config
        era = self.get_era(selector.data, is_mc)

        if dsname.startswith("TTTo"):
            selector.set_column("gent_lc", self.gentop, lazy=True)
            if "top_pt_reweighting" in self.config:
                selector.add_cut(
                    "Top_pt_reweighting", self.do_top_pt_reweighting,
                    no_callback=True)

        if is_mc:
            selector.set_column("GenLepton", partial(gen_col_defs.build_genlepton_column, is_mc))#for info in removal procedure
            selector.set_column("GenPhoton", partial(gen_col_defs.build_genphoton_column, is_mc))#for info in removal procedure

        if not is_mc:
            selector.add_cut("Lumi", partial(
                self.good_lumimask, is_mc, dsname))

        # apply MET filter
        selector.add_cut("MET_filters", partial(self.met_filters, is_mc))

        # apply the number of good PV is at least 1 
        selector.add_cut("atLeastOnePV", self.add_PV_cut)

        if is_mc and "pileup_reweighting" in self.config:
            selector.add_cut("Pileup_reweighting", partial(
                self.do_pileup_reweighting, dsname))

        if self.config["compute_systematics"] and is_mc:
            self.add_generator_uncertainies(dsname, selector)

        # Only allow events that pass triggers specified in config
        # This also takes into account a trigger order to avoid triggering
        # the same event if it's in two different data datasets.
        pos_triggers, neg_triggers = pepper.misc.get_trigger_paths_for(
            dsname, is_mc, self.config["dataset_trigger_map"],self.config["dataset_trigger_order"])
        #print("Trigger ",pos_triggers,neg_triggers)
        #selector.add_cut("Trigger", partial(self.passing_trigger, pos_triggers, neg_triggers))
    
        if is_mc and self.config["year"] in ("2016", "2017", "ul2016pre","ul2016post", "ul2017"):
            selector.add_cut("L1_prefiring", self.add_l1_prefiring_weights)

        # Pick electrons satisfying our criterias
        selector.set_column("Electron",self.pick_veto_electrons)
        # Pick muons satisfying our criterias
        selector.set_column("Muon",self.pick_veto_muons)

        # Combine electron and muon to lepton
        selector.set_column("Lepton", partial(self.build_lepton_column, is_mc, selector.rng))
        selector.add_cut("OneLep",self.one_lepton)

        # Define lepton categories, the number of lepton cut applied here
        selector.set_multiple_columns(self.lepton_categories)
        selector.set_cat("channel",{"ele", "muon"})

        selector.add_cut("pass_trig_muon",partial(self.passing_hlt,self.config['trigger_muon_path']),categories={"channel": ["muon"]})
        selector.add_cut("pass_trig_ele",partial(self.passing_hlt,self.config['trigger_ele_path']),categories={"channel": ["ele"]})

        selector.add_cut("exact_one_muon",self.exact_one_muon,categories={"channel": ["muon"]})
        selector.add_cut("exact_one_ele",self.exact_one_ele,categories={"channel": ["ele"]})
  
        # Only accept MC events with prompt lepton 
        #if is_mc:
        #   selector.add_cut("isPromptLepton", self.isprompt_lepton)
        #   selector.set_column("Lepton_isPrompt", self.build_lepton_prompt)

        if is_mc:
           selector.set_multiple_columns( partial(self.lepton_source_cat,is_mc))
           selector.set_cat("lepton_type",{"prompt_lep", "nonprompt_lep"})
        else:
           selector.set_multiple_columns( partial(self.lepton_source_cat,is_mc))
           selector.set_cat("lepton_type",{"all_lepton"})

        selector.set_column("n_loose_lepton",self.num_loose_lepton)       
        selector.set_column("n_tight_lepton",self.num_tight_lepton)       
        selector.set_column("n_fake_lepton",self.num_fake_lepton)       

        selector.add_cut("muon_sf",partial(self.apply_muon_sf, is_mc))
        selector.add_cut("electron_sf",partial(self.apply_electron_sf, is_mc))

        selector.set_column("Lepton_charge", self.lepton_charge)

        # JME unc
        if (is_mc and self.config["compute_systematics"]
                and dsname not in self.config["dataset_for_systematics"]):
            if hasattr(filler, "sys_overwrite"):
                assert filler.sys_overwrite is None
            for variarg in self.get_jetmet_variation_args():
                selector_copy = copy(selector)
                filler.sys_overwrite = variarg.name
                self.process_selection_jet_part(selector_copy, is_mc,
                                                variarg, dsname, filler, era)
                if self.eventdir is not None:
                    logger.debug(f"Saving per event info for variation"
                                 f" {variarg.name}")
                    self.save_per_event_info(
                        dsname + "_" + variarg.name, selector_copy, False)
            filler.sys_overwrite = None

        #JME selection -> remove for photon category
        self.process_selection_jet_part(selector, is_mc,
                                        self.get_jetmet_nominal_arg(),
                                        dsname, filler, era)
        logger.debug("Selection done")

    def process_selection_jet_part(self, selector, is_mc, variation, dsname,
                                   filler, era):

        # Pick Jets satisfying our criterias
        logger.debug(f"Running jet_part with variation {variation.name}")
        reapply_jec = ("reapply_jec" in self.config
                       and self.config["reapply_jec"])
        # comput jetfac from jer 
        selector.set_multiple_columns(partial(
            self.compute_jet_factors, is_mc, reapply_jec, variation.junc,
            variation.jer, selector.rng))

        selector.set_column("OrigJet", selector.data["Jet"])
        selector.set_column("Jet", partial(self.build_jet_column, is_mc))
        if "jet_puid_sf" in self.config and is_mc:
            selector.add_cut("JetPUIdSFs", partial(self.compute_jet_puid_sfs,is_mc))
        selector.set_column("Jet", self.jets_with_puid)

        #selector.set_column("CentralJet", self.build_centraljet_column)
        #selector.set_column("ForwardJet", self.build_forwardjet_column)

        smear_met = "smear_met" in self.config and self.config["smear_met"]
        selector.set_column(
            "MET", partial(self.build_met_column, is_mc, variation.junc,
                           variation.jer if smear_met else None, selector.rng,
                           era, variation=variation.met))

        selector.set_column("mTW",self.build_mtw_column)

        #selector.set_column("bJet", self.build_bjet_column)
        #selector.set_column("loose_bJet", self.build_loose_bjet_column)
        #selector.set_column("nbtag", self.num_btags)
        #selector.set_column("n_loose_btag", self.num_loose_btags)
        #selector.set_column("ncentral_jets", self.num_centralJets)
        #selector.set_column("nforward_jets", self.num_forwardJets)

        # Only accept events that have at least two jets and one bjet
        selector.add_cut("atLeast1jet",self.has_jets) # one jet
        #selector.set_column("deltaR_lj",self.build_deltaR_ljet)

        #Build different categories according to the number of jets
        selector.set_multiple_columns(self.measurement_sideband)
        selector.set_cat("MR_SD", {"MR", "MR1","MR2"},safe=False)

        # Only accept events with MET pt more than 20 GeV
        #selector.add_cut("Req_MET", self.met_requirement)
        #selector.add_cut("mTW_cut", self.mTW_requirement)

        selector.add_cut("finalselection", self.dummycut)

    def pick_veto_electrons(self, data):
        ele = data["Electron"]

        # We do not want electrons that are between the barrel and the end cap
        # For this, we need the eta of the electron with respect to its
        # supercluster
        sc_eta_abs = abs(ele.eta + ele.deltaEtaSC)
        is_in_transreg = (1.4442 < sc_eta_abs) & (sc_eta_abs < 1.566)
        impact = ( (sc_eta_abs<1.4442) & (abs(ele.dz) < 0.1) & (abs(ele.dxy) < 0.05) ) | ( (sc_eta_abs>1.566) & (abs(ele.dz) < 0.2) & (abs(ele.dxy) < 0.1) )

        # Electron ID, as an example we use the MVA one here
#        has_id = ele.mvaFall17V2Iso_WP90
        has_id = ele.cutBased >= 3

        # Finally combine all the requirements
        is_good = (
            has_id
            & impact
            & (~is_in_transreg)
            & (self.config["ele_eta_min"] < ele.eta)
            & (ele.eta < self.config["ele_eta_max"])
            & (self.config["good_ele_pt_min"] < ele.pt))
 
        veto_id = ele.cutBased >=1

        is_veto = (
                veto_id
              & impact 
              & (~is_in_transreg)
              & (self.config["ele_eta_min"] < ele.eta)
              & (ele.eta < self.config["ele_eta_max"])
              & (self.config["good_ele_pt_min"] < ele.pt))

        #is_fake = ( (veto_id & ~has_id)
        #          & impact 
        #          & (~is_in_transreg)   
        #          & (self.config["ele_eta_min"] < ele.eta) 
        #          & (ele.eta < self.config["ele_eta_max"])
        #          & (self.config["good_ele_pt_min"] < ele.pt)
        #          )

        is_fake = is_veto & ~is_good
        ele["is_good"] = is_good
        ele["is_fake"] = is_fake

        # Return all electrons with are deemed to be good
        return ele[is_veto]

    def pick_veto_muons(self, data):
        muon = data["Muon"]
        etacuts = (self.config["muon_eta_min"] < muon.eta) & (muon.eta < self.config["muon_eta_max"])

        good_id = muon.tightId
        good_iso = muon.pfIsoId > 3
        is_good = (
            good_id
            & good_iso
            & etacuts
            & (self.config["good_muon_pt_min"] < muon.pt))
        
        veto_id = muon.looseId
        veto_iso = muon.pfIsoId >= 1
        is_veto = (
            veto_id
            & veto_iso
            & etacuts
            & (self.config["good_muon_pt_min"] < muon.pt))

        #fake_iso = (muon.pfIsoId >= 1) & (muon.pfIsoId <= 3)
        #is_fake = ( good_id 
        #          & fake_iso 
        #          & etacuts 
        #          & (self.config["good_muon_pt_min"] < muon.pt) )

        is_fake = is_veto & ~is_good
        muon["is_good"] = is_good
        muon["is_fake"] = is_fake

        return muon[is_veto]

    def build_lepton_column(self, is_mc, rng, data):
        """Build a lepton column containing electrons and muons."""
        electron = data["Electron"]
        muon = data["Muon"]
        # Apply Rochester corrections to muons
        #print(muon.pt)
        if "muon_rochester" in self.config:
            muon = self.apply_rochester_corr(muon, rng, is_mc)
            #print("after correction:",muon.pt)
        columns = ["pt", "eta", "phi", "mass", "pdgId","charge","is_good","is_fake"]
        if is_mc:
           columns.append("genPartIdx")
        lepton = {}
        for column in columns:
            lepton[column] = ak.concatenate([electron[column], muon[column]],
                                            axis=1)
        lepton = ak.zip(lepton, with_name="PtEtaPhiMLorentzVector",
                        behavior=data.behavior)

        # Sort leptons by pt
        # Also workaround for awkward bug using ak.values_astype
        # https://github.com/scikit-hep/awkward-1.0/issues/1288
        lepton = lepton[
            ak.values_astype(ak.argsort(lepton["pt"], ascending=False), int)]
        return lepton

    def one_lepton(self,data):
        return (ak.num(data["Lepton"])>0)

    def exact_one_ele(self,data):
        nele = ak.num(data['Electron'])
        nmuon = ak.num(data['Muon'])
        accept = (nele==1) & (nmuon==0)

        return accept

    def exact_one_muon(self,data):
        nele = ak.num(data['Electron'])
        nmuon = ak.num(data['Muon'])
        accept = (nmuon==1) & (nele==0)

        return accept

    def num_loose_lepton(self,data):
        loose_lep = data['Lepton']
        n_loose_lep = ak.num(loose_lep) 
      
        return n_loose_lep

    def num_tight_lepton(self,data):
        tight_lep = data['Lepton']['is_good']
        n_tight_lep = ak.sum(tight_lep,axis=1) 
      
        return n_tight_lep

    def num_fake_lepton(self,data):
        fake_lep = data['Lepton']['is_fake']
        n_fake_lep = ak.sum(fake_lep,axis=1) 
      
        return n_fake_lep

    def lepton_categories(self,data):
        cat = {}
        leps = data["Lepton"]
        cat['ele']  =  (abs(leps[:, 0].pdgId) == 11)
        cat['muon'] =  (abs(leps[:, 0].pdgId) == 13)

        return cat

    def isprompt_lepton(self, data):
        promptmatch = self.lepton_isprompt(data)
        n_prompt_lep = ak.sum(promptmatch,axis=1)
        accept = n_prompt_lep > 0        

        return accept

    def build_lepton_prompt(self,data):
        lepton = data["Lepton"]
        prompt = self.lepton_isprompt(data)
        return ak.sum(prompt,axis=1)>0

    def lepton_isprompt(self,data):
        lepton = data["Lepton"]
        genpart = data["GenPart"]

        genmatchID = lepton.genPartIdx[(lepton.genPartIdx!=-1)]
        matched_genlepton = genpart[genmatchID]
        promptmatch =  matched_genlepton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | ( matched_genlepton.hasFlags(['isPromptTauDecayProduct'])) |
                        ( matched_genlepton.hasFlags(["fromHardProcess"])))

        return promptmatch

    def lepton_source_cat(self,is_mc,data):
        cats = {}
        isprompt_lepton = self.isprompt_lepton(data)
        print("TEST", isprompt_lepton)

        if is_mc:
           cats["prompt_lep"] = isprompt_lepton
           cats["nonprompt_lep"] = ~isprompt_lepton
        else:
           cats["all_lepton"] = ak.num(data["Lepton"])>0

        return cats

    def one_good_photon(self,data):
        return ak.num(data["Photon"])>0
   
    def mass_lg(self, data):
        """Return invariant mass of lepton plus photon"""
        return (data["Lepton"][:, 0] + data["Photon"][:, 0]).mass

    def z_cut(self,data):
        is_out_window = abs(data['mlg'] - 91.2) > 10
        return is_out_window    

    def met_requirement(self, data):
        met = data["MET"].pt
        return met < 20

    def mTW_requirement(self, data):
        mTW = data["mTW"]
        return mTW < 20

    def measurement_sideband(self,data):
        cats = {}
        met = data["MET"].pt
        mTW = data["mTW"]
        cats["MR"]  = (met<20) & (mTW<20)  
        cats["MR1"] = (met<30) & (mTW<30)
        cats["MR2"] = (met<30) & (mTW<20)

        return cats

    def apply_electron_sf(self,is_mc,data):
        if is_mc and ("electron_sf" in self.config
                       and len(self.config["electron_sf"]) > 0):
           weight, systematics = self.compute_electron_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))
       
    def apply_muon_sf(self,is_mc,data):
        if is_mc and ("muon_sf" in self.config
                       and len(self.config["muon_sf"]) > 0):
           weight, systematics = self.compute_muon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))
       
    def apply_psv_sf(self,is_mc,data):
        if is_mc and ("psv_sf" in self.config
                       and len(self.config["psv_sf"]) > 0):
           weight, systematics = self.compute_psv_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_photon_sf(self,is_mc,data):
        if is_mc and ("photon_sf" in self.config
                       and len(self.config["photon_sf"]) > 0):
           weight, systematics = self.compute_photon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_btag_sf(self, is_mc, data):
        """Apply btag scale factors."""
        if is_mc and (
                "btag_sf" in self.config and len(self.config["btag_sf"]) != 0):
            weight, systematics = self.compute_weight_btag(data)
            return weight, systematics
        else:
            return np.ones(len(data))

