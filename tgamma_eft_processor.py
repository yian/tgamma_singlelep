# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.
# Here we create a very simplified version of the ttbar-to-dilep processor.
# One can run this processor using
# 'python3 -m pepper.runproc --debug example_processor.py example_config.json'
# Above command probably will need a little bit of time before all cuts are
# applied once. This is because a chunk of events are processed simultaneously.
# You change adjust the number of events in a chunk and thereby the memory
# usage by using the --chunksize parameter (the default value is 500000).

import pepper
import awkward as ak
import math
import logging
from functools import partial
from copy import copy
import numpy as np
from math import pi
from config_topgamma import ConfigTopGamma
from pepper.scale_factors import (TopPtWeigter, PileupWeighter, BTagWeighter,
                                  get_evaluator, ScaleFactors)
from xgboost import XGBClassifier
import xgboost as xgb

from tools import neutrino_reco
from tools import top_reco
from tools import gen_col_defs
from tools.utils import DeltaR1
from tools.get_ML_output import get_score as get_score

from processor_basic import BasicFuncs

logger = logging.getLogger(__name__)

# All processors should inherit from pepper.ProcessorBasicPhysics
class Processor(BasicFuncs):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = ConfigTopGamma

    def __init__(self, config, eventdir):
        # Initialize the class, maybe overwrite some config variables and
        # load additional files if needed
        # Can set and modify configuration here as well
        # Need to call parent init to make histograms and such ready

        config["histogram_format"] = "root"

        super().__init__(config, eventdir)

    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, define objects and/or
        # compute event weights

        # Add a cut only allowing events according to the golden JSON
        # The good_lumimask method is specified in pepper.ProcessorBasicPhysics
        # It also requires a lumimask to be specified in config
        era = self.get_era(selector.data, is_mc)

        if dsname.startswith("TTTo"):
            selector.set_column("gent_lc", self.gentop, lazy=True)
            if "top_pt_reweighting" in self.config:
                selector.add_cut(
                    "Top pt reweighting", self.do_top_pt_reweighting,
                    no_callback=True)

        if is_mc:
            selector.set_column("GenLepton", partial(gen_col_defs.build_genlepton_column, is_mc))
            selector.set_column("GenPhoton", partial(gen_col_defs.build_genphoton_column, is_mc))
            selector.set_column("GenTop", self.gentop)
            if dsname.startswith("TGJets_lepton") :
                selector.set_column("Reweights",selector.data["Reweights"])
                selector.set_column("w_sm",selector.data["Reweights"][:,0])
                selector.set_column("w_ctg_m1",selector.data["Reweights"][:,1])
                selector.set_column("w_ctg_1",selector.data["Reweights"][:,2])
                selector.set_column("w_ctg_5",selector.data["Reweights"][:,7])
                selector.set_column("w_ctg_10",selector.data["Reweights"][:,10])
                selector.set_column("w_ctw_m1",selector.data["Reweights"][:,3])
                selector.set_column("w_ctw_1",selector.data["Reweights"][:,4])
                selector.set_column("w_ctw_5",selector.data["Reweights"][:,8])
                selector.set_column("w_ctw_10",selector.data["Reweights"][:,11])
                selector.set_column("w_ctb_m1",selector.data["Reweights"][:,5])
                selector.set_column("w_ctb_1",selector.data["Reweights"][:,6])
                selector.set_column("w_ctb_5",selector.data["Reweights"][:,9])
                selector.set_column("w_ctb_10",selector.data["Reweights"][:,12])
                selector.set_column("w_ctg_ctw_1",selector.data["Reweights"][:,13])
                selector.set_column("w_ctg_ctb_1",selector.data["Reweights"][:,14])
                selector.set_column("w_ctw_ctb_1",selector.data["Reweights"][:,15])
                selector.set_column("w_ctg_ctw_5",selector.data["Reweights"][:,16])
                selector.set_column("w_ctg_ctb_5",selector.data["Reweights"][:,17])
                selector.set_column("w_ctw_ctb_5",selector.data["Reweights"][:,18])
                selector.set_column("Unmatched",selector.data["Unmatched"])
                #print(">>>>>>>",selector.data["Unmatched"])

        if not is_mc:
            selector.add_cut("Lumi", partial(
                self.good_lumimask, is_mc, dsname))
            #selector.add_cut("Exclude_runsAfter_319337",partial(self.remove_runs, is_mc))

        # apply MET filter
        selector.add_cut("MET_filters", partial(self.met_filters, is_mc))

        # apply the number of good PV is at least 1 
        selector.add_cut("atLeastOnePV", self.add_PV_cut)

        if is_mc and "pileup_reweighting" in self.config:
            selector.add_cut("Pileup_reweighting", partial(
                self.do_pileup_reweighting, dsname))

        #self.add_pdf_uncertainties(dsname, selector,selector.data)
        diffGenbins = 'inclusive'
        if self.config["compute_systematics"] and is_mc and 'TTZToNuNu_TuneCP5_13TeV_amcatnlo-pythia8' not in dsname:
            self.add_pdf_uncertainties_per_bin(dsname, selector, diffGenbins)
            self.add_ps_uncertainties_per_bin(dsname, selector, diffGenbins)
            self.add_pdf_uncertainties_per_bin(dsname, selector, diffGenbins)

        # Only allow events that pass triggers specified in config
        # This also takes into account a trigger order to avoid triggering
        # the same event if it's in two different data datasets.
        pos_triggers, neg_triggers = pepper.misc.get_trigger_paths_for(
            dsname, is_mc, self.config["dataset_trigger_map"],self.config["dataset_trigger_order"])
        #print("Trigger ",pos_triggers,neg_triggers)
        #selector.add_cut("Trigger", partial(self.passing_trigger, pos_triggers, neg_triggers))
    
        if is_mc and self.config["year"] in ("2016", "2017", "ul2016pre","ul2016post", "ul2017"):
            selector.add_cut("L1 prefiring", self.add_l1_prefiring_weights)
        if is_mc:
            selector.set_column("trigobjs", selector.data["TrigObj"])

        # Pick electrons satisfying our criterias
        selector.set_multiple_columns(self.pick_electrons)
        # Pick muons satisfying our criterias
        selector.set_multiple_columns(self.pick_muons)

        # Combine electron and muon to lepton
        selector.set_column("Lepton", partial(self.build_lepton_column, is_mc, selector.rng))
        selector.add_cut("OneLep",self.one_lepton)

        # Define lepton categories, the number of lepton cut applied here
        selector.set_multiple_columns(self.lepton_categories)
        selector.set_cat("channel",{"ele", "muon"})

        selector.add_cut("pass_trig_muon",partial(self.passing_hlt,self.config['trigger_muon_path']),categories={"channel": ["muon"]})
        selector.add_cut("pass_trig_ele",partial(self.passing_hlt,self.config['trigger_ele_path']),categories={"channel": ["ele"]})
        selector.add_cut("exact_one_muon",self.exact_one_muon,categories={"channel": ["muon"]})
        selector.add_cut("exact_one_ele",self.exact_one_ele,categories={"channel": ["ele"]})

        selector.add_cut("muon_sf",partial(self.apply_muon_sf, is_mc))
        selector.add_cut("electron_sf",partial(self.apply_electron_sf, is_mc))

        selector.set_column("Lepton_charge", self.lepton_charge)

        # Pick photons satisfying our criterias
        selector.set_column("Photon", self.pick_medium_photons)
#        selector.set_column("Photon", self.pick_loose_photons)

        # JME unc
        if (is_mc and self.config["compute_systematics"]
                and dsname not in self.config["dataset_for_systematics"]):
            if hasattr(filler, "sys_overwrite"):
                assert filler.sys_overwrite is None
            for variarg in self.get_jetmet_variation_args():
                selector_copy = copy(selector)
                filler.sys_overwrite = variarg.name
                self.process_selection_jet_part(selector_copy, is_mc,
                                                variarg, dsname, filler, era)
                if self.eventdir is not None:
                    logger.debug(f"Saving per event info for variation"
                                 f" {variarg.name}")
                    self.save_per_event_info(
                        dsname + "_" + variarg.name, selector_copy, False)
            filler.sys_overwrite = None

        #JME selection -> remove for photon category
        self.process_selection_jet_part(selector, is_mc,
                                        self.get_jetmet_nominal_arg(),
                                        dsname, filler, era)
        logger.debug("Selection done")

    def process_selection_jet_part(self, selector, is_mc, variation, dsname,
                                   filler, era):

        # Pick Jets satisfying our criterias
        logger.debug(f"Running jet_part with variation {variation.name}")
        reapply_jec = ("reapply_jec" in self.config
                       and self.config["reapply_jec"])
        # comput jetfac from jer 
        selector.set_multiple_columns(partial(
            self.compute_jet_factors, is_mc, reapply_jec, variation.junc,
            variation.jer, selector.rng))

        selector.set_column("OrigJet", selector.data["Jet"])
        selector.set_column("Jet", partial(self.build_jet_column, is_mc))
        if "jet_puid_sf" in self.config and is_mc:
            selector.add_cut("JetPUIdSFs", partial(self.compute_jet_puid_sfs,is_mc))
        selector.set_column("Jet", self.jets_with_puid)
        selector.set_column("CentralJet", self.build_centraljet_column)
        selector.set_column("ForwardJet", self.build_forwardjet_column)

        smear_met = "smear_met" in self.config and self.config["smear_met"]
        selector.set_column(
            "MET", partial(self.build_met_column, is_mc, variation.junc,
                           variation.jer if smear_met else None, selector.rng,
                           era, variation=variation.met))

        selector.set_column("mTW",self.build_mtw_column)
        selector.set_column("bJet", self.build_bjet_column)
        selector.set_column("loose_bJet", self.build_loose_bjet_column)
        selector.set_column("noBjet", self.build_noBjet_column)
        selector.set_column("n_noBjet", self.num_noBjet)
        selector.set_column("nbtag", self.num_btags)
        selector.set_column("n_loose_btag", self.num_loose_btags)
        selector.set_column("ncentral_jets", self.num_centralJets)
        selector.set_column("nforward_jets", self.num_forwardJets)

        #do first cuts: overlap removal and >=1 leptons
        selector.add_cut("removeSToverlap", partial(self.remove_stnlo_overlap,dsname))
        selector.add_cut("removeTTNLO_pow_overlap", partial(self.remove_ttnlo_powheg_overlap,dsname))
        selector.add_cut("removeZoverlap", partial(self.remove_z_overlap,dsname))
        selector.add_cut("removeWoverlap", partial(self.remove_w_overlap,dsname))
        #selector.add_cut("removeTToverlap", partial(self.remove_ttlo_overlap,dsname))
        #selector.add_cut("removeTTNLO_mg5_overlap", partial(self.remove_ttnlo_mg5_overlap,dsname))
        #selector.add_cut("removeSTWoverlap", partial(self.remove_stW_overlap,dsname))

        # Only accept events that have at least one photon
        selector.add_cut("atLeastOnePhoton",self.one_good_photon)

        if self.config["photon_phi_removal"]:
           selector.add_cut("photon_phi_remove",self.remove_phi_spike)

        selector.set_column("deltaR_lg",self.build_deltaR_lgamma)
        selector.set_column("mlg",self.mass_lg,no_callback=True)
        #selector.add_cut("Zcut", self.z_cut,categories={"channel": ["ele"]})

        if is_mc:
           selector.set_multiple_columns(self.photon_categories)        
           selector.set_cat("photon_type",{"prompt","ele_matched", "nonprompt","allphoton"},safe=False)
        else:
           selector.set_multiple_columns(self.photon_categories_data)
           selector.set_cat("photon_type", {'allphoton'} )

        selector.add_cut("preselection", self.dummycut)
        selector.add_cut("photon_sf",partial(self.apply_photon_sf, is_mc))
        selector.add_cut("psv_sf",partial(self.apply_psv_sf, is_mc))

        # Only accept events that have at least two jets and one bjet
        selector.add_cut("atLeast2jet",self.has_jets)

        selector.set_column("deltaR_jg",self.build_deltaR_jgamma)
        selector.set_column("deltaR_lj",self.build_deltaR_ljet)
        #selector.add_cut("HasBtags", partial(self.btag_cut, is_mc))

        #Build different categories according to the number of jets
        selector.set_multiple_columns(self.btag_categories)
        selector.set_cat("jet_btag", {"j2+_b0", "j2+_b1","j2+_b2","j2+_b3+","j2+_b1+"},safe=False)
        selector.add_cut("btag_sf",partial(self.apply_btag_sf, is_mc))

        # Only accept events with MET pt more than 20 GeV
        selector.add_cut("Req_MET", self.met_requirement)

        # Only accept MC events with prompt lepton 
        if is_mc:
           selector.add_cut("isPromptLepton", self.isprompt_lepton)
           selector.set_column("Lepton_isPrompt", self.build_lepton_prompt)

        selector.add_cut("finalselection", self.dummycut)

	#ttbar semileptnic reconstruction
        selector.set_column("Neutrino1",neutrino_reco.neutrino_reco)          
        selector.set_column("reco_W",self.build_W_column)

        topVars = top_reco.topreco(selector.data)
        selector.set_column("tophad_m", topVars["mtophad"])           
        selector.set_column("tophad_pt", topVars["pttophad"])
        selector.set_column("tophad_eta", topVars["etatophad"])
        selector.set_column("tophad_phi", topVars["phitophad"])

        selector.set_column("toplep_m",   topVars["mtoplep"])
        selector.set_column("toplep_pt",  topVars["pttoplep"])
        selector.set_column("toplep_eta", topVars["etatoplep"])
        selector.set_column("toplep_phi", topVars["phitoplep"])
        #print(selector.data["toplep_m"], len(selector.data["toplep_m"]))
        #chi2_flat =  [y for x in topVars["chisquare"] for y in x]
        #chi2 = [i for i in chi2_flat if i is not None] 
        selector.set_column("chi2",topVars["chisquare"])       
        selector.set_column("dphi_lepTopg",topVars["dphi_lepTopg"])       
        selector.set_column("dphi_hadTopg",topVars["dphi_hadTopg"])       
        selector.set_column("deta_lepTopg",topVars["deta_lepTopg"])       
        selector.set_column("deta_hadTopg",topVars["deta_hadTopg"])       
        selector.set_column("deltaR_lepTopg",topVars["deltaR_lepTopg"])       
        selector.set_column("deltaR_hadTopg",topVars["deltaR_hadTopg"])       
        selector.set_column("score",partial(get_score,self.config["ML_model"],self.config["year"]))
        #print('score',selector.data['score'])

    def remove_runs(self,is_mc,data):
        accept = np.full(len(data), True) 
        run = np.array(data["run"])
        if '18' in self.config['year']:
            #print(run)
            remove_flag = run>=319337
            accept = accept & ~remove_flag

        return accept

    def one_lepton(self,data):
        return (ak.num(data["Lepton"])>0)

    def exact_one_ele(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nele==1) & (nmuon==0)

        return accept

    def exact_one_muon(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nmuon==1) & (nele==0)

        return accept

    def lepton_categories(self,data):
        cat = {}
        leps = data["Lepton"]
        cat['ele']  =  (abs(leps[:, 0].pdgId) == 11)
        cat['muon'] =  (abs(leps[:, 0].pdgId) == 13)

        return cat

    def one_good_photon(self,data):
        return ak.num(data["Photon"])>0
   
    def remove_phi_spike(self,data):
        phi = data["Photon"][:,0].phi
        if "18" in self.config["year"]:
            accept = ~((phi>0.5) & (phi<0.7))
        else:
            accept = (ak.num(data["Lepton"])>0)
 
        return accept

    def mass_lg(self, data):
        """Return invariant mass of lepton plus photon"""
        return (data["Lepton"][:, 0] + data["Photon"][:, 0]).mass

    def z_cut(self,data):
        is_out_window = abs(data['mlg'] - 91.2) > 10
        return is_out_window    

    def isprompt_lepton(self, data):
        promptmatch = self.lepton_isprompt(data)
        n_prompt_lep = ak.sum(promptmatch,axis=1)
        accept = n_prompt_lep > 0        

        return accept

    def build_lepton_prompt(self,data):
        lepton = data["Lepton"]
        prompt = self.lepton_isprompt(data)
        return ak.sum(prompt,axis=1)>0

    def lepton_isprompt(self,data):
        lepton = data["Lepton"]
        genpart = data["GenPart"]

        genmatchID = lepton.genPartIdx[(lepton.genPartIdx!=-1)]
        matched_genlepton = genpart[genmatchID]
        promptmatch =  matched_genlepton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | ( matched_genlepton.hasFlags(['isPromptTauDecayProduct'])) |
                        ( matched_genlepton.hasFlags(["fromHardProcess"])))

        return promptmatch
    
    def btag_categories(self,data):
        cats = {}
        
        num_btagged = data["nbtag"]
        #num_btagged = data["n_loose_btag"]
        njet = ak.num(data["Jet"])

        cats["j2+_b0"] = (num_btagged == 0) & (njet >= 2)
        cats["j2+_b1"] = (num_btagged == 1) & (njet >= 2)
        cats["j2+_b2"] = (num_btagged == 2) & (njet >= 2)
        cats["j2+_b3+"] = (num_btagged >= 3) & (njet >= 2)
        cats["j2+_b1+"] = (num_btagged >= 1) & (njet >= 2)

        return cats

    def met_requirement(self, data):
        met = data["MET"].pt
        return met > self.config["met_min_met"]

    def apply_electron_sf(self,is_mc,data):
        if is_mc and ("electron_sf" in self.config
                       and len(self.config["electron_sf"]) > 0):
           weight, systematics = self.compute_electron_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))
       
    def apply_muon_sf(self,is_mc,data):
        if is_mc and ("muon_sf" in self.config
                       and len(self.config["muon_sf"]) > 0):
           weight, systematics = self.compute_muon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))
       
    def apply_psv_sf(self,is_mc,data):
        if is_mc and ("psv_sf" in self.config
                       and len(self.config["psv_sf"]) > 0):
           weight, systematics = self.compute_psv_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_photon_sf(self,is_mc,data):
        if is_mc and ("photon_sf" in self.config
                       and len(self.config["photon_sf"]) > 0):
           weight, systematics = self.compute_photon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_btag_sf(self, is_mc, data):
        """Apply btag scale factors."""
        if is_mc and (
                "btag_sf" in self.config and len(self.config["btag_sf"]) != 0):
            weight, systematics = self.compute_weight_btag(data)
            return weight, systematics
        else:
            return np.ones(len(data))

