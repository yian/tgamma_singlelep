# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.
# Here we create a very simplified version of the ttbar-to-dilep processor.
# One can run this processor using
# 'python3 -m pepper.runproc --debug example_processor.py example_config.json'
# Above command probably will need a little bit of time before all cuts are
# applied once. This is because a chunk of events are processed simultaneously.
# You change adjust the number of events in a chunk and thereby the memory
# usage by using the --chunksize parameter (the default value is 500000).

import pepper
import awkward as ak
import math
import logging
from functools import partial,reduce
import operator

from copy import copy
import numpy as np
from config_topgamma import ConfigTopGamma
from pepper.scale_factors import (TopPtWeigter, PileupWeighter, BTagWeighter,
                                  get_evaluator, ScaleFactors)
from xgboost import XGBClassifier
import xgboost as xgb

from tools import neutrino_reco
from tools import top_reco
from tools import gen_col_defs
from tools.utils import DeltaR1
from tools.get_ML_output import get_score as get_score
from tools.get_ML_output import get_new_score as get_new_score

from processor_basic import BasicFuncs

logger = logging.getLogger(__name__)

# All processors should inherit from pepper.ProcessorBasicPhysics
class Processor(BasicFuncs):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = ConfigTopGamma

    def __init__(self, config, eventdir):
        # Initialize the class, maybe overwrite some config variables and
        # load additional files if needed
        # Can set and modify configuration here as well
        # Need to call parent init to make histograms and such ready

        #config["histogram_format"] = "root"

        super().__init__(config, eventdir)
        self.diff_binning = ak.Array([0.0,1.7,2.5,3.1])

    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, define objects and/or
        # compute event weights

        # Add a cut only allowing events according to the golden JSON
        # The good_lumimask method is specified in pepper.ProcessorBasicPhysics
        # It also requires a lumimask to be specified in config
        era = self.get_era(selector.data, is_mc)

        if dsname.startswith("TTTo"):
            selector.set_column("gent_lc", self.gentop, lazy=True)
            if "top_pt_reweighting" in self.config:
                selector.add_cut(
                    "Top_pt_reweighting", self.do_top_pt_reweighting,
                    no_callback=True)

        if is_mc:
            selector.set_column("GenLepton", partial(gen_col_defs.build_genlepton_column, is_mc))
            selector.set_column("GenPhoton", partial(gen_col_defs.build_genphoton_column, is_mc))

        if not is_mc:
            selector.add_cut("Lumi", partial(
                self.good_lumimask, is_mc, dsname))

        # apply MET filter
        selector.add_cut("MET_filters", partial(self.met_filters, is_mc))

        # apply the number of good PV is at least 1 
        selector.add_cut("atLeastOnePV", self.add_PV_cut)

        if is_mc and "pileup_reweighting" in self.config:
            selector.add_cut("Pileup_reweighting", partial(
                self.do_pileup_reweighting, dsname))


        if self.config["compute_systematics"] and is_mc and not self.config["only_nonprompt_syst"]:
            self.add_generator_uncertainies(dsname, selector)

        # Only allow events that pass triggers specified in config
        # This also takes into account a trigger order to avoid triggering
        # the same event if it's in two different data datasets.
        pos_triggers, neg_triggers = pepper.misc.get_trigger_paths_for(
            dsname, is_mc, self.config["dataset_trigger_map"],
            self.config["dataset_trigger_order"])
#        print("Trigger ",pos_triggers,neg_triggers)
#        selector.add_cut("Trigger", partial(
#            self.passing_trigger, pos_triggers, neg_triggers))
    
        if is_mc and self.config["year"] in ("2016", "2017", "ul2016pre","ul2016post", "ul2017"):
            selector.add_cut("L1_prefiring", self.add_l1_prefiring_weights)

        # Pick electrons satisfying our criterias
        selector.set_multiple_columns(self.pick_electrons)
        # Pick muons satisfying our criterias
        selector.set_multiple_columns(self.pick_muons)

        # Combine electron and muon to lepton
        selector.set_column("Lepton", partial(self.build_lepton_column, is_mc, selector.rng))
        selector.add_cut("OneLep",self.one_lepton)

        # Define lepton categories, the number of lepton cut applied here
        selector.set_multiple_columns(self.lepton_categories)
        selector.set_cat("channel",{"ele", "muon"})

        selector.add_cut("pass_trig_muon",partial(self.passing_hlt,self.config['trigger_muon_path']),categories={"channel": ["muon"]})
        selector.add_cut("pass_trig_ele",partial(self.passing_hlt,self.config['trigger_ele_path']),categories={"channel": ["ele"]})
        selector.add_cut("exact_one_muon",self.exact_one_muon,categories={"channel": ["muon"]})
        selector.add_cut("exact_one_ele",self.exact_one_ele,categories={"channel": ["ele"]})

        selector.add_cut("muon_sf",partial(self.apply_muon_sf, is_mc))
        selector.add_cut("electron_sf",partial(self.apply_electron_sf, is_mc))

        selector.set_column("Lepton_charge", self.lepton_charge)

        # Pick photons satisfying our criterias
#        selector.set_column("Photon", self.pick_medium_photons)
        selector.set_column("Photon", self.pick_loose_photons)

        # JME unc
        if (is_mc and self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]
                and dsname not in self.config["dataset_for_systematics"]):
            if hasattr(filler, "sys_overwrite"):
                assert filler.sys_overwrite is None
            for variarg in self.get_jetmet_variation_args():
                selector_copy = copy(selector)
                filler.sys_overwrite = variarg.name
                self.process_selection_jet_part(selector_copy, is_mc,
                                                variarg, dsname, filler, era)
                if self.eventdir is not None:
                    logger.debug(f"Saving per event info for variation"
                                 f" {variarg.name}")
                    self.save_per_event_info(
                        dsname + "_" + variarg.name, selector_copy, False)
            filler.sys_overwrite = None

        #JME selection -> remove for photon category
        self.process_selection_jet_part(selector, is_mc,
                                        self.get_jetmet_nominal_arg(),
                                        dsname, filler, era)
        logger.debug("Selection done")

    def process_selection_jet_part(self, selector, is_mc, variation, dsname,
                                   filler, era):

        # Pick Jets satisfying our criterias
        logger.debug(f"Running jet_part with variation {variation.name}")
        reapply_jec = ("reapply_jec" in self.config
                       and self.config["reapply_jec"])
        # comput jetfac from jer 
        selector.set_multiple_columns(partial(
            self.compute_jet_factors, is_mc, reapply_jec, variation.junc,
            variation.jer, selector.rng))

        selector.set_column("OrigJet", selector.data["Jet"])
        selector.set_column("Jet", partial(self.build_jet_column, is_mc))
        if "jet_puid_sf" in self.config and is_mc:
            selector.add_cut("JetPUIdSFs", partial(self.compute_jet_puid_sfs,is_mc))
        selector.set_column("Jet", self.jets_with_puid)
        selector.set_column("CentralJet", self.build_centraljet_column)
        selector.set_column("ForwardJet", self.build_forwardjet_column)

        smear_met = "smear_met" in self.config and self.config["smear_met"]
        selector.set_column(
            "MET", partial(self.build_met_column, is_mc, variation.junc,
                           variation.jer if smear_met else None, selector.rng,
                           era, variation=variation.met))

        selector.set_column("mTW",self.build_mtw_column)
        selector.set_column("bJet", self.build_bjet_column)
        selector.set_column("loose_bJet", self.build_loose_bjet_column)
        selector.set_column("noBjet", self.build_noBjet_column)
        selector.set_column("n_noBjet", self.num_noBjet)
        selector.set_column("nbtag", self.num_btags)
        selector.set_column("n_loose_btag", self.num_loose_btags)
        selector.set_column("ncentral_jets", self.num_centralJets)
        selector.set_column("nforward_jets", self.num_forwardJets)

        #do first cuts: overlap removal and >=1 leptons
        selector.add_cut("removeSToverlap", partial(self.remove_stnlo_overlap,dsname))
        selector.add_cut("removeTTNLO_pow_overlap", partial(self.remove_ttnlo_powheg_overlap,dsname))
        selector.add_cut("removeZoverlap", partial(self.remove_z_overlap,dsname))
        selector.add_cut("removeWoverlap", partial(self.remove_w_overlap,dsname))
        #selector.add_cut("removeTToverlap", partial(self.remove_ttlo_overlap,dsname))
        #selector.add_cut("removeTTNLO_mg5_overlap", partial(self.remove_ttnlo_mg5_overlap,dsname))
        #selector.add_cut("removeSTWoverlap", partial(self.remove_stW_overlap,dsname))

        # Only accept events that have at least one photon
        selector.add_cut("atLeastOnePhoton",self.one_good_photon)

        if self.config["photon_phi_removal"]:
           selector.add_cut("photon_phi_remove",self.remove_phi_spike)

        selector.add_cut("PhotonInB",self.ABCD_selectB)
        selector.set_column("mlg",self.mass_lg,no_callback=True)

        selector.set_column("deltaR_lg",self.build_deltaR_lgamma)
        #selector.add_cut("Zcut", self.z_cut,categories={"channel": ["ele"]})

        if is_mc:
        # Only accept MC events with prompt lepton 
           #selector.add_cut("isPromptLepton", self.isprompt_lepton)
           #selector.set_column("Lepton_isPrompt", self.build_lepton_prompt)

           #selector.add_cut("prompt_photon",self.IsPromptOrElematchedPhoton)
           selector.add_cut("MCsub",self.is_subtraction_for_MC)

        selector.add_cut("preselection", self.dummycut)

        # Add nonprompt photon SFs
        selector.add_cut("nonprompt_photon_sf",self.apply_nonprompt_photon_sf)
        #selector.add_cut("nonprompt_photon_sf",self.apply_nonprompt_photonsf_all)
        selector.add_cut("nonprompt_MCcorrection",self.apply_nonprompt_MCcorrection)

        selector.add_cut("photon_sf",partial(self.apply_photon_sf, is_mc))
        selector.add_cut("psv_sf",partial(self.apply_psv_sf, is_mc))

        # Only accept events that have at least two jets and one bjet
        selector.add_cut("atLeast2jet",self.has_jets)

        selector.set_column("deltaR_jg",self.build_deltaR_jgamma)
        selector.set_column("deltaR_lj",self.build_deltaR_ljet)
        #selector.add_cut("HasBtags", partial(self.btag_cut, is_mc))

	#ttbar semileptnic reconstruction
        selector.set_column("Neutrino1",neutrino_reco.neutrino_reco)          
        selector.set_column("reco_W",self.build_W_column)

        topVars = top_reco.topreco(selector.data)
        selector.set_column("tophad_m", topVars["mtophad"])           
        selector.set_column("tophad_pt", topVars["pttophad"])
        selector.set_column("tophad_eta", topVars["etatophad"])
        selector.set_column("tophad_phi", topVars["phitophad"])

        selector.set_column("toplep_m",   topVars["mtoplep"])
        selector.set_column("toplep_pt",  topVars["pttoplep"])
        selector.set_column("toplep_eta", topVars["etatoplep"])
        selector.set_column("toplep_phi", topVars["phitoplep"])

        selector.set_column("toplep",self.build_toplep_column)
        selector.set_column("deltaR_topg",self.build_deltaR_topg)

        diffRecobins = {'DRtopgRecoBin1','DRtopgRecoBin2','DRtopgRecoBin3','DRtopgRecoBin4','DRtopgRecoBinAll'}
        selector.set_multiple_columns(partial(self.DRtopg_recobins,dsname,self.diff_binning))
        selector.set_cat("dRtopg_recobins", diffRecobins,safe=False)

        chi2 = topVars["chisquare"]
        chi2 = ak.fill_none(chi2,-10,axis=1)
        selector.set_column("chi2",chi2)
        selector.set_column("dphi_lepTopg",topVars["dphi_lepTopg"])
        selector.set_column("dphi_hadTopg",topVars["dphi_hadTopg"])
        selector.set_column("deta_lepTopg",topVars["deta_lepTopg"])
        selector.set_column("deta_hadTopg",topVars["deta_hadTopg"])
        selector.set_column("deltaR_lepTopg",topVars["deltaR_lepTopg"])
        selector.set_column("deltaR_hadTopg",topVars["deltaR_hadTopg"])
        selector.set_column("score",partial(get_score,self.config["ML_model"],self.config["year"]))
        selector.set_column("new_score",get_new_score)

        #Build different categories according to the number of jets
        #selector.set_cat("jet_btag", {"j2+_b0", "j2+_b1","j2+_b2","j2+_b3+","j2+_b1+"})
        selector.set_multiple_columns(self.SR_CR_definition)
        selector.set_cat("jet_btag", {"tgQ_SR","ttg_SR","CR"},safe=False)
        selector.add_cut("btag_sf",partial(self.apply_btag_sf, is_mc))

        # Only accept events with MET pt more than 20 GeV
        selector.add_cut("Req_MET", self.met_requirement)

        selector.add_cut("finalselection", self.dummycut)

    def ABCD_selectB(self, data):

        photons = data["Photon"]
        isBarrel = photons[:,0]["isScEtaEB"]
        sieie = photons[:,0]["sieie"]
        chIso = photons[:,0]["pfRelIso03_chg"]*photons[:,0]["pt"]

        atleast1j = ak.num(data["Jet"]) >= 1
        atleast2j = ak.num(data["Jet"]) >= 2
        atleast1b = (ak.sum(data["Jet"]["btagged"], axis=1) >= 1)

        #chIso medium 1.141 for B and 1.051 for E
        #sieie medium 0.01015 for B and 0.0272 for E

        is_appl = (ak.where(isBarrel,(chIso < 1.141),(chIso < 1.051)))

        is_meas = (ak.where(isBarrel,(chIso > 1.141),(chIso > 1.051)))

        is_A = (is_appl) & (ak.where(isBarrel,(sieie < 0.01015),(sieie < 0.0272)))
        is_B = (is_appl) & (ak.where(isBarrel,(sieie > 0.0110) ,(sieie > 0.028)))
        is_C = (is_meas) & (ak.where(isBarrel,(sieie < 0.01015),(sieie < 0.0272)))
        is_D = (is_meas) & (ak.where(isBarrel,(sieie > 0.0110) ,(sieie > 0.028)))

        return is_B

    def one_lepton(self,data):
        return (ak.num(data["Lepton"])>0)

    def exact_one_ele(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nele==1) & (nmuon==0)

        return accept

    def exact_one_muon(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nmuon==1) & (nele==0)

        return accept

    def lepton_categories(self,data):
        cat = {}
        leps = data["Lepton"]
        cat['ele']  =  (abs(leps[:, 0].pdgId) == 11)
        cat['muon'] =  (abs(leps[:, 0].pdgId) == 13)

        return cat

    def one_good_photon(self,data):
        return ak.num(data["Photon"])>0

    def remove_phi_spike(self,data):
        phi = data["Photon"][:,0].phi
        if "18" in self.config["year"]:
            accept = ~((phi>0.5) & (phi<0.7))
        else:
            accept = (ak.num(data["Lepton"])>0)

        return accept

    def mass_lg(self, data):
        """Return invariant mass of lepton plus photon"""
        return (data["Lepton"][:, 0] + data["Photon"][:, 0]).mass

    def z_cut(self,data):
        is_out_window = abs(data['mlg'] - 91.2) > 10
        return is_out_window

    def lepton_isprompt(self,data):
        lepton = data["Lepton"]
        genpart = data["GenPart"]

        genmatchID = lepton.genPartIdx[(lepton.genPartIdx!=-1)]
        matched_genlepton = genpart[genmatchID]
        promptmatch =  matched_genlepton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | ( matched_genlepton.hasFlags(['isPromptTauDecayProduct'])) |
                        ( matched_genlepton.hasFlags(["fromHardProcess"])))

        return promptmatch

    def isprompt_lepton(self, data):
        promptmatch = self.lepton_isprompt(data)
        n_prompt_lep = ak.sum(promptmatch,axis=1)
        accept = n_prompt_lep > 0        

        return accept
       
    def build_lepton_prompt(self,data):
        lepton = data["Lepton"]
        prompt = self.lepton_isprompt(data)
        return ak.sum(prompt,axis=1)>0

    def IsPromptOrElematchedPhoton(self,data):

        photons = data["Photon"]
        genpart = data["GenPart"]

        # photons matched to a gen Pho
        true_photons = photons[ak.fill_none(abs(photons.matched_gen.pdgId) == 22, False)]
        # photons matched to a gen Ele
        electron_matched = photons[ak.fill_none(abs(photons.matched_gen.pdgId) == 11, False)]
        # photons can't matched to any gen Obj
        unmatched_photons = photons[ak.is_none(photons.matched_gen,axis=1)]

        promptmatch = true_photons.matched_gen.hasFlags(['isPrompt'])
        #promptmatch = ( (promptmatch) | (true_photons.matched_gen.hasFlags(['isDirectPromptTauDecayProduct'])) |
        promptmatch = ( (promptmatch) | (true_photons.matched_gen.hasFlags(['isPromptTauDecayProduct'])) |
                        (true_photons.matched_gen.hasFlags(["fromHardProcess"])))

        accept = ( (ak.sum(promptmatch,axis=1)>0) | (ak.num(electron_matched)>0) )

        return accept

    def is_subtraction_for_MC(self,data):

        photons = data["Photon"]
        leptons = data["Lepton"]

        all_photon = ak.num(photons)>0
        all_lepton = ak.num(leptons)>0

        isprompt_photon = self.IsPromptOrElematchedPhoton(data)
        isprompt_lepton = self.lepton_isprompt(data)

        subract_prompt_photon_prompt_lepton = isprompt_photon & isprompt_lepton
        subract_all_photon_nonprompt_lepton = all_photon & ~isprompt_lepton

        #final_subtraction = (subract_prompt_photon_prompt_lepton) | (subract_all_photon_nonprompt_lepton)
        final_subtraction = subract_prompt_photon_prompt_lepton

        accept = ak.sum(final_subtraction,axis=1)>0
        #print(">>> check final_subtraction ",final_subtraction)
        #print(">>> check accept",accept)

        return accept

    def btag_categories(self,data):
        cats = {}
        
        num_btagged = data["nbtag"]
        #num_btagged = data["n_loose_btag"]
        njet = ak.num(data["Jet"])

        cats["j2+_b0"] = (num_btagged == 0) & (njet >= 2)
        cats["j2+_b1"] = (num_btagged == 1) & (njet >= 2)
        cats["j2+_b2"] = (num_btagged == 2) & (njet >= 2)
        cats["j2+_b3+"] = (num_btagged >= 3) & (njet >= 2)
        cats["j2+_b1+"] = (num_btagged >= 1) & (njet >= 2)

        return cats

    def met_requirement(self, data):
        met = data["MET"].pt
        return met > self.config["met_min_met"]

    def apply_electron_sf(self,is_mc,data):
        if is_mc and ("electron_sf" in self.config
                       and len(self.config["electron_sf"]) > 0):
           weight, systematics = self.compute_electron_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))
       
    def apply_muon_sf(self,is_mc,data):
        if is_mc and ("muon_sf" in self.config
                       and len(self.config["muon_sf"]) > 0):
           weight, systematics = self.compute_muon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))
       
    def apply_psv_sf(self,is_mc,data):
        if is_mc and ("psv_sf" in self.config
                       and len(self.config["psv_sf"]) > 0):
           weight, systematics = self.compute_psv_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_nonprompt_photon_sf(self,data):
        if ("nonprompt_photon_sf" in self.config 
             and len(self.config["nonprompt_photon_sf"]) > 0):
            weight = self.compute_nonprompt_photon_sf(data)
            #print("nonprompt_photon_sf",weight)
            return weight
        else:
            return np.ones(len(data))

    def apply_nonprompt_photonsf_all(self,data):
        if ("nonprompt_photonsf_all" in self.config
             and len(self.config["nonprompt_photonsf_all"]) > 0):
            weight = self.compute_nonprompt_photonsf_all(data)
            #print("nonprompt_photon_sf_all",weight)
            return weight
        else:
            return np.ones(len(data))

    def apply_nonprompt_photon_sf_ttbar(self,data):
        if ("nonprompt_photon_sf_ttbar" in self.config 
             and len(self.config["nonprompt_photon_sf_ttbar"]) > 0):
            weight = self.compute_nonprompt_photon_sf(data)
            #print("nonprompt_photon_sf_ttbar",weight)
            return weight
        else:
            return np.ones(len(data))

    def apply_nonprompt_photonsf_ttbar_all(self,data):
        if ("nonprompt_photonsf_ttbar_all" in self.config
             and len(self.config["nonprompt_photonsf_ttbar_all"]) > 0):
            weight = self.compute_nonprompt_photonsf_all(data)
            #print("nonprompt_photon_sf_ttbar_all",weight)
            return weight
        else:
            return np.ones(len(data))

    def apply_nonprompt_MCcorrection(self,data):
        if ("nonprompt_MCcorrection" in self.config 
             and len(self.config["nonprompt_MCcorrection"]) > 0):
            weight = self.compute_nonprompt_MCcorrection(data)
            #print("nonprompt_MCcorrection",weight)
            return weight
        else:
            return np.ones(len(data))

    def apply_photon_sf(self,is_mc,data):
        if is_mc and ("photon_sf" in self.config
                       and len(self.config["photon_sf"]) > 0):
           weight, systematics = self.compute_photon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_btag_sf(self, is_mc, data):
        """Apply btag scale factors."""
        if is_mc and (
                "btag_sf" in self.config and len(self.config["btag_sf"]) != 0):
            weight, systematics = self.compute_weight_btag(data)
            return weight, systematics
        else:
            return np.ones(len(data))

    def build_deltaR_topg(self,data):

        # not use the data["deltaR_lepTopg"] because 
        #the array.ndim is -1 when no events left which causes error for the following cut

        toplep = data["toplep"]
        photon = data["Photon"]
        deltaR_topg = ak.flatten(photon.metric_table(toplep),axis=2)

        return deltaR_topg[:,0]

    def DRtopg_recobins(self,dsname,diff_binning,data):

        cat = {}
        deltaR_topg = data["deltaR_topg"]

        for i,v in enumerate(diff_binning):
            if i < len(diff_binning)-1:
                sel = (deltaR_topg > diff_binning[i]) & (deltaR_topg <= diff_binning[i+1])
            else:
                sel = (deltaR_topg > diff_binning[i])
            cat['DRtopgRecoBin'+str(i+1)] = sel
        cat['DRtopgRecoBinAll'] = (ak.num(data["Lepton"])>0)

        return cat

    def SR_CR_definition(self,data):
        cat = {}
        num_btagged = data["nbtag"]
        njet = ak.num(data["Jet"])
        MVA_score = data["new_score"]
        MVA_score_cut = ak.flatten(MVA_score>0.5) if (MVA_score>0.5).ndim>1 else (MVA_score>0.5)

        cat["tgQ_SR"] = (num_btagged >= 1) & (njet >= 2) & MVA_score_cut
        cat["ttg_SR"] = (num_btagged >= 1) & (njet >= 2) & ~MVA_score_cut
        cat["CR"] = (num_btagged == 0) & (njet >= 2)

        return cat
