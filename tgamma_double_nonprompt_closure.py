# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.
# Here we create a very simplified version of the ttbar-to-dilep processor.
# One can run this processor using
# 'python3 -m pepper.runproc --debug example_processor.py example_config.json'
# Above command probably will need a little bit of time before all cuts are
# applied once. This is because a chunk of events are processed simultaneously.
# You change adjust the number of events in a chunk and thereby the memory
# usage by using the --chunksize parameter (the default value is 500000).

import pepper
import awkward as ak
import math
import logging
from functools import partial
from copy import copy
import numpy as np
from config_topgamma import ConfigTopGamma
from pepper.scale_factors import (TopPtWeigter, PileupWeighter, BTagWeighter,
                                  get_evaluator, ScaleFactors)

from tools import gen_col_defs
from tools import neutrino_reco
from tools import top_reco

from processor_basic import BasicFuncs

logger = logging.getLogger(__name__)

# All processors should inherit from pepper.ProcessorBasicPhysics
class Processor(BasicFuncs):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = ConfigTopGamma

    def __init__(self, config, eventdir):
        # Initialize the class, maybe overwrite some config variables and
        # load additional files if needed
        # Can set and modify configuration here as well
        # Need to call parent init to make histograms and such ready

        config["histogram_format"] = "root"

        super().__init__(config, eventdir)

    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, define objects and/or
        # compute event weights

        # Add a cut only allowing events according to the golden JSON
        # The good_lumimask method is specified in pepper.ProcessorBasicPhysics
        # It also requires a lumimask to be specified in config
        era = self.get_era(selector.data, is_mc)

        if dsname.startswith("TTTo"):
            selector.set_column("gent_lc", self.gentop, lazy=True)
            if "top_pt_reweighting" in self.config:
                selector.add_cut(
                    "Top_pt_reweighting", self.do_top_pt_reweighting,
                    no_callback=True)

        if is_mc:
            selector.set_column("GenLepton", partial(gen_col_defs.build_genlepton_column, is_mc))
            selector.set_column("GenPhoton", partial(gen_col_defs.build_genphoton_column, is_mc))

        if not is_mc:
            selector.add_cut("Lumi", partial(
                self.good_lumimask, is_mc, dsname))

        # apply MET filter
        selector.add_cut("MET_filters", partial(self.met_filters, is_mc))

        # apply the number of good PV is at least 1 
        selector.add_cut("atLeastOnePV", self.add_PV_cut)

        if is_mc and "pileup_reweighting" in self.config:
            selector.add_cut("Pileup_reweighting", partial(
                self.do_pileup_reweighting, dsname))

        if self.config["compute_systematics"] and is_mc:
            self.add_generator_uncertainies(dsname, selector)

        # Only allow events that pass triggers specified in config
        # This also takes into account a trigger order to avoid triggering
        # the same event if it's in two different data datasets.
        pos_triggers, neg_triggers = pepper.misc.get_trigger_paths_for(
            dsname, is_mc, self.config["dataset_trigger_map"],self.config["dataset_trigger_order"])
        #print("Trigger ",pos_triggers,neg_triggers)
        #selector.add_cut("Trigger", partial(self.passing_trigger, pos_triggers, neg_triggers))
    
        if is_mc and self.config["year"] in ("2016", "2017", "ul2016pre","ul2016post", "ul2017"):
            selector.add_cut("L1_prefiring", self.add_l1_prefiring_weights)

        # HEM issue cut
        if (self.config["hem_cut_if_ele"] or self.config["hem_cut_if_muon"]
                or self.config["hem_cut_if_jet"]):
            selector.add_cut("HEM cut", self.hem_cut)

        # Pick electrons satisfying our criterias
        selector.set_multiple_columns(self.pick_electrons)
        # Pick muons satisfying our criterias
        selector.set_multiple_columns(self.pick_muons)

        selector.add_cut("NoAddLeps",partial(self.no_additional_leptons, is_mc))

        # Combine electron and muon to lepton
        selector.set_column("Lepton", partial(self.build_lepton_column, is_mc, selector.rng))
        selector.add_cut("OneLep",self.one_lepton)

        # Define lepton categories, the number of lepton cut applied here
        selector.set_multiple_columns(self.lepton_categories)
        selector.set_cat("channel",{"good_ele", "good_muon","fake_ele","fake_muon"},safe=False)

        selector.add_cut("pass_trig_muon",partial(self.passing_hlt,self.config['trigger_muon_path']),
                          categories={"channel": ["good_muon","fake_muon"]})
        selector.add_cut("pass_trig_ele",partial(self.passing_hlt,self.config['trigger_ele_path']),
                          categories={"channel": ["good_ele","fake_ele"]})

        #selector.add_cut("exact_one_muon",self.exact_one_muon,categories={"channel": ["muon"]})
        #selector.add_cut("exact_one_ele",self.exact_one_ele,categories={"channel": ["ele"]})

        selector.set_column("Lepton_charge", self.lepton_charge)

        # Only accept MC events with prompt lepton 
        if is_mc:
           selector.set_multiple_columns( partial(self.lepton_source_cat,is_mc))
           selector.set_cat("lepton_type",{"prompt_lep", "nonprompt_lep"})
        else:
           selector.set_multiple_columns( partial(self.lepton_source_cat,is_mc))
           selector.set_cat("lepton_type",{"all_lepton"})

        selector.add_cut("preselection", self.dummycut)

        # JME unc
        if (is_mc and self.config["compute_systematics"]
                and dsname not in self.config["dataset_for_systematics"]):
            if hasattr(filler, "sys_overwrite"):
                assert filler.sys_overwrite is None
            for variarg in self.get_jetmet_variation_args():
                selector_copy = copy(selector)
                filler.sys_overwrite = variarg.name
                self.process_selection_jet_part(selector_copy, is_mc,
                                                variarg, dsname, filler, era)
                if self.eventdir is not None:
                    logger.debug(f"Saving per event info for variation"
                                 f" {variarg.name}")
                    self.save_per_event_info(
                        dsname + "_" + variarg.name, selector_copy, False)
            filler.sys_overwrite = None

        #JME selection -> remove for photon category
        self.process_selection_jet_part(selector, is_mc,
                                        self.get_jetmet_nominal_arg(),
                                        dsname, filler, era)
        logger.debug("Selection done")

    def process_selection_jet_part(self, selector, is_mc, variation, dsname,
                                   filler, era):

        # Pick Jets satisfying our criterias
        logger.debug(f"Running jet_part with variation {variation.name}")
        reapply_jec = ("reapply_jec" in self.config
                       and self.config["reapply_jec"])
        # comput jetfac from jer 
        selector.set_multiple_columns(partial(
            self.compute_jet_factors, is_mc, reapply_jec, variation.junc,
            variation.jer, selector.rng))

        selector.set_column("OrigJet", selector.data["Jet"])
        selector.set_column("Jet", partial(self.build_jet_column, is_mc))
        if "jet_puid_sf" in self.config and is_mc:
            selector.add_cut("JetPUIdSFs", partial(self.compute_jet_puid_sfs,is_mc))
        selector.set_column("Jet", self.jets_with_puid)
        selector.set_column("CentralJet", self.build_centraljet_column)
        selector.set_column("ForwardJet", self.build_forwardjet_column)

        smear_met = "smear_met" in self.config and self.config["smear_met"]
        selector.set_column(
            "MET", partial(self.build_met_column, is_mc, variation.junc,
                           variation.jer if smear_met else None, selector.rng,
                           era, variation=variation.met))

        selector.set_column("mTW",self.build_mtw_column)
        selector.set_column("bJet", self.build_bjet_column)
        selector.set_column("loose_bJet", self.build_loose_bjet_column)
        selector.set_column("nbtag", self.num_btags)
        selector.set_column("n_loose_btag", self.num_loose_btags)
        selector.set_column("ncentral_jets", self.num_centralJets)
        selector.set_column("nforward_jets", self.num_forwardJets)

        # Add nonprompt lepton SFs
        '''
           when use categories in add_cut, meet errors if run for systematics,
           not a problem for nominal run. And appear for weights application.
        '''
        #selector.add_cut("nonprompt_lepton_sf",self.apply_nonprompt_lepton_sf)
        selector.add_cut("nonprompt_lepton_sf",self.apply_nonprompt_leptonsf_ttbar_closure,
                               categories={"channel": ["fake_ele","fake_muon"]})

        '''Don't need the photon removal for nonprompt lepton closure'''
        #do first cuts: overlap removal and >=1 leptons
        #selector.add_cut("removeSToverlap", partial(self.remove_stnlo_overlap,dsname))
        #selector.add_cut("removeTToverlap", partial(self.remove_ttlo_overlap,dsname))
        #selector.add_cut("removeTTNLO_mg5_overlap", partial(self.remove_ttnlo_mg5_overlap,dsname))
        #selector.add_cut("removeTTNLO_powheg_overlap", partial(self.remove_ttnlo_powheg_overlap,dsname))
        #selector.add_cut("removeSTWoverlap", partial(self.remove_stW_overlap,dsname))
        #selector.add_cut("removeZoverlap", partial(self.remove_z_overlap,dsname))
        #selector.add_cut("removeWoverlap", partial(self.remove_w_overlap,dsname))

        # Pick photons satisfying our criterias
        selector.set_column("Photon", self.pick_loose_photons)
        #selector.set_column("Photon", self.pick_loose_photons)

        # Only accept events that have at least one photon
        selector.add_cut("atLeastOnePhoton",self.one_good_photon)

        selector.set_multiple_columns(self.ABCDmethod_masks)
        selector.set_cat("ABCD", {'A','B'},safe=False)
        selector.add_cut("after_ABCD", self.dummycut)

        '''
           when use categories in add_cut, meet errors if run for systematics,
           not a problem for nominal run. And appear for weights application.
        '''
        selector.add_cut("nonprompt_photon_sf",self.apply_nonprompt_photon_sf_ttbar_double_nonprompt_closure,categories={"ABCD": ["B"]})
        selector.add_cut("nonprompt_MCcorrection",self.apply_nonprompt_MCcorrection_double_nonprompt_closure,categories={"ABCD": ["B"]})

        selector.set_column("deltaR_lg",self.build_deltaR_lgamma)
        selector.set_column("mlg",self.mass_lg,no_callback=True)
        #selector.add_cut("Zcut", self.z_cut,categories={"channel": ["ele"]})

        if is_mc:
           selector.set_multiple_columns(self.photon_categories)        
           selector.set_cat("photon_type",{"prompt","ele_matched", "nonprompt","allphoton"},safe=False)
        else:
           selector.set_multiple_columns(self.photon_categories_data)
           selector.set_cat("photon_type", {'allphoton'},safe=False )

        selector.add_cut("before_photon_sf", self.dummycut)
        selector.add_cut("photon_sf",partial(self.apply_photon_sf, is_mc))
        selector.add_cut("psv_sf",partial(self.apply_psv_sf, is_mc))

        # Only accept events that have at least two jets and one bjet
        selector.add_cut("atLeast2jet",self.has_jets)
        selector.set_column("deltaR_lj",self.build_deltaR_ljet)
        selector.set_column("deltaR_jg",self.build_deltaR_jgamma)
        #selector.add_cut("HasBtags", partial(self.btag_cut, is_mc))

        #Build different categories according to the number of jets
        selector.set_multiple_columns(self.btag_categories)
        selector.set_cat("jet_btag", {"j2+_b0","j2+_b1+"},safe=False)
        selector.add_cut("btag_sf",partial(self.apply_btag_sf, is_mc))

        # Only accept events with MET pt more than 20 GeV
        selector.add_cut("Req_MET", self.met_requirement)

        selector.add_cut("finalselection", self.dummycut)

	#ttbar semileptnic reconstruction
        #selector.set_column("Neutrino1",neutrino_reco.neutrino_reco)          
        #selector.set_column("reco_W",self.build_W_column)

        #topVars = top_reco.topreco(selector.data)
        #selector.set_column("tophad_m", topVars["mtophad"])           
        #selector.set_column("tophad_pt", topVars["pttophad"])
        #selector.set_column("tophad_eta", topVars["etatophad"])
        #selector.set_column("tophad_phi", topVars["phitophad"])

        #selector.set_column("toplep_m",   topVars["mtoplep"])
        #selector.set_column("toplep_pt",  topVars["pttoplep"])
        #selector.set_column("toplep_eta", topVars["etatoplep"])
        #selector.set_column("toplep_phi", topVars["phitoplep"])

        #chi2_flat =  [y for x in topVars["chisquare"] for y in x]
        #chi2 = [i for i in chi2_flat if i is not None] 
        #selector.set_column("chi2",topVars["chisquare"])       

    def pick_electrons(self, data):
        ele = data["Electron"]

        # We do not want electrons that are between the barrel and the end cap
        # For this, we need the eta of the electron with respect to its
        # supercluster
        sc_eta_abs = abs(ele.eta + ele.deltaEtaSC)
        is_in_transreg = (1.4442 < sc_eta_abs) & (sc_eta_abs < 1.566)
        impact = ( (sc_eta_abs<1.4442) & (abs(ele.dz) < 0.1) & (abs(ele.dxy) < 0.05) ) | ( (sc_eta_abs>1.566) & (abs(ele.dz) < 0.2) & (abs(ele.dxy) < 0.1) )

        # Electron ID, as an example we use the MVA one here
#        has_id = ele.mvaFall17V2Iso_WP90
        has_id = ele.cutBased >= 3

        # Finally combine all the requirements
        is_good = (
            has_id
            & impact
            & (~is_in_transreg)
            & (self.config["ele_eta_min"] < ele.eta)
            & (ele.eta < self.config["ele_eta_max"])
            & (self.config["good_ele_pt_min"] < ele.pt))
 
        veto_id = ele.cutBased >=1

        is_veto = (
                veto_id
              & (~is_in_transreg)
              & (self.config["ele_eta_min"] < ele.eta)
              & (ele.eta < self.config["ele_eta_max"])
              & (self.config["veto_ele_pt_min"] < ele.pt))

        ele_hem1516 = self.in_hem1516(ele.phi, ele.eta)
        if self.config["hem_cut_if_ele"]:
           is_good = is_good & ~ele_hem1516
           is_veto = is_veto & ~ele_hem1516

        #fake_id = (veto_id & ~has_id)
        #is_fake = ( fake_id
        #          & impact
        #          & (~is_in_transreg)
        #          & (self.config["ele_eta_min"] < ele.eta)
        #          & (ele.eta < self.config["ele_eta_max"])
        #          & (self.config["good_ele_pt_min"] < ele.pt))

        is_fake = (is_veto & impact) & ~is_good & (self.config["good_ele_pt_min"] < ele.pt)

        ele["is_fake"] = is_fake
        ele["is_good"] = is_good

        # Return all electrons with are deemed to be good
        return {"Electron": ele[is_fake|is_good], "VetoEle": ele[is_veto]}

    def pick_muons(self, data):
        muon = data["Muon"]
        etacuts = (self.config["muon_eta_min"] < muon.eta) & (muon.eta < self.config["muon_eta_max"])

        good_id = muon.tightId
        good_iso = muon.pfIsoId > 3
        is_good = (
            good_id
            & good_iso
            & etacuts
            & (self.config["good_muon_pt_min"] < muon.pt))
        
        veto_id = muon.looseId
        veto_iso = muon.pfIsoId >= 1
        is_veto = (
            veto_id
            & veto_iso
            & etacuts
            & (self.config["veto_muon_pt_min"] < muon.pt))

        muon_hem1516 = self.in_hem1516(muon.phi, muon.eta)
        if self.config["hem_cut_if_muon"]:
           is_good = is_good & ~muon_hem1516
           is_veto = is_veto & ~muon_hem1516

        #fake_iso = (muon.pfRelIso04_all < 0.4) & (muon.pfRelIso04_all > 0.15)
        #is_fake = ( good_id
        #          & fake_iso
        #          & etacuts
        #          & (self.config["good_muon_pt_min"] < muon.pt) )

        is_fake = is_veto & ~is_good & (self.config["good_muon_pt_min"] < muon.pt)

        muon["is_fake"] = is_fake
        muon["is_good"] = is_good
        return {"Muon": muon[is_fake|is_good], "VetoMuon": muon[is_veto]}

    def ABCDmethod_masks(self, data):

        photons = data["Photon"]
        isBarrel = photons[:,0]["isScEtaEB"]
        sieie = photons[:,0]["sieie"]
        chIso = photons[:,0]["pfRelIso03_chg"]*photons[:,0]["pt"]

        atleast1j = ak.num(data["Jet"]) >= 1
        atleast2j = ak.num(data["Jet"]) >= 2
        atleast1b = (ak.sum(data["Jet"]["btagged"], axis=1) >= 1)

        #chIso medium 1.141 for B and 1.051 for E
        #sieie medium 0.01015 for B and 0.0272 for E

        is_appl = (ak.where(isBarrel,(chIso < 1.141),(chIso < 1.051)))

        is_meas = (ak.where(isBarrel,(chIso > 1.141),(chIso > 1.051)))

        is_A = (is_appl) & (ak.where(isBarrel,(sieie < 0.01015),(sieie < 0.0272)))
        is_B = (is_appl) & (ak.where(isBarrel,(sieie > 0.0110) ,(sieie > 0.028)))
        is_C = (is_meas) & (ak.where(isBarrel,(sieie < 0.01015),(sieie < 0.0272)))
        is_D = (is_meas) & (ak.where(isBarrel,(sieie > 0.0110) ,(sieie > 0.028)))

        region = {}
        region['A'] = (is_A)
        region['B'] = (is_B)

        return region
   
    def one_lepton(self,data):
        return (ak.num(data["Lepton"])>0)

    def no_additional_leptons(self, is_mc, data):
        """Veto events with >= 2 leptons."""
        add_ele = ak.num(data["VetoEle"])
        add_muon = ak.num(data["VetoMuon"])

        return (add_ele+add_muon)<=1

    def exact_one_ele(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nele==1) & (nmuon==0)

        return accept

    def exact_one_muon(self,data):
        nele = ak.num(data['VetoEle'])
        nmuon = ak.num(data['VetoMuon'])
        accept = (nmuon==1) & (nele==0)

        return accept

    def lepton_categories(self,data):
        cat = {}
        #print(">>>>> fake ele ",data["Electron"]["is_fake"], ak.sum(data["Electron"]["is_fake"],axis=1) )
        #print(">>>>> fake muon ",data["Muon"]["is_fake"], ak.sum(data["Muon"]["is_fake"],axis=1) )
        leps=data["Lepton"]
        cat['fake_ele']  =  (abs(leps[:, 0].pdgId) == 11) & (ak.sum(data["Electron"]["is_fake"],axis=1)>0)
        cat['good_ele']  =  (abs(leps[:, 0].pdgId) == 11) & (ak.sum(data["Electron"]["is_good"],axis=1)>0)
        cat['fake_muon'] =  (abs(leps[:, 0].pdgId) == 13) & (ak.sum(data["Muon"]["is_fake"],axis=1)>0)
        cat['good_muon'] =  (abs(leps[:, 0].pdgId) == 13) & (ak.sum(data["Muon"]["is_good"],axis=1)>0)

        return cat

    def passing_hlt(self,trigger_path,data):
        hlt = data["HLT"]
        triggered = np.full(len(data), False)
        channel_ele = (data["good_ele"]) | (data["fake_ele"])

        if "16" in self.config["year"]:
           for trig_p in trigger_path:
               triggered = triggered | np.asarray(hlt[trig_p])
        elif "17" in self.config["year"]:
           trigobjs_ele = ak.sum(data["TrigObj"].id == 11,axis=1)>0
           pass_L1SingleEGOrFilter = ak.sum((data["TrigObj"].filterBits & (1 << 10) == (1 << 10)),axis=1)>0
           pass_eleTrig_final = (np.asarray(hlt[trigger_path]) & trigobjs_ele & pass_L1SingleEGOrFilter)
           #print("<<<<<<",hlt[trigger_path])
           #print("<<<<<<",np.asarray(hlt[trigger_path]))

           pass_hlt = ak.where(channel_ele,pass_eleTrig_final,np.asarray(hlt[trigger_path]))
           triggered |= np.asarray(pass_hlt)

           #triggered_test = np.full(len(data), False)
           #triggered_test |= np.asarray(hlt[trigger_path])

           #print(">>>> channel",data["muon"],' Orig trig ',triggered_test,ak.sum(triggered_test))
           #print(">>>> channel",data["muon"],' new trig ', triggered,ak.sum(triggered))

        elif "18" in self.config["year"]:
           triggered |= np.asarray(hlt[trigger_path])

        return triggered

    def isprompt_lepton(self, data):
        promptmatch = self.lepton_isprompt(data)
        n_prompt_lep = ak.sum(promptmatch,axis=1)
        accept = n_prompt_lep > 0

        return accept

    def build_lepton_prompt(self,data):
        lepton = data["Lepton"]
        prompt = self.lepton_isprompt(data)
        return ak.sum(prompt,axis=1)>0 

    def lepton_isprompt(self,data):
        lepton = data["Lepton"]
        genpart = data["GenPart"]

        genmatchID = lepton.genPartIdx[(lepton.genPartIdx!=-1)]
        matched_genlepton = genpart[genmatchID]
        promptmatch =  matched_genlepton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | ( matched_genlepton.hasFlags(['isPromptTauDecayProduct'])) |
                        ( matched_genlepton.hasFlags(["fromHardProcess"])))

        return promptmatch

    def lepton_source_cat(self,is_mc,data):
        cats = {}
        isprompt_lepton = self.isprompt_lepton(data)
        #print("TEST", isprompt_lepton)

        if is_mc:
           cats["prompt_lep"] = isprompt_lepton
           cats["nonprompt_lep"] = ~isprompt_lepton
        else:
           cats["all_lepton"] = ak.num(data["Lepton"])>0

        return cats

    def one_good_photon(self,data):
        return ak.num(data["Photon"])>0

    def mass_lg(self, data):
        """Return invariant mass of lepton plus photon"""
        return (data["Lepton"][:, 0] + data["Photon"][:, 0]).mass

    def z_cut(self,data):
        is_out_window = abs(data['mlg'] - 91.2) > 10
        return is_out_window    

    def btag_categories(self,data):
        cats = {}
        
        num_btagged = data["nbtag"]
        #num_btagged = data["n_loose_btag"]
        njet = ak.num(data["Jet"])

        cats["j2+_b0"] = (num_btagged == 0) & (njet >= 2)
        cats["j2+_b1+"] = (num_btagged >= 1) & (njet >= 2)

        return cats

    def met_requirement(self, data):
        met = data["MET"].pt
        return met > self.config["met_min_met"]

    def apply_electron_sf(self,is_mc,data):
        if is_mc and ("electron_sf" in self.config
                       and len(self.config["electron_sf"]) > 0):
           weight, systematics = self.compute_electron_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))
       
    def apply_muon_sf(self,is_mc,data):
        if is_mc and ("muon_sf" in self.config
                       and len(self.config["muon_sf"]) > 0):
           weight, systematics = self.compute_muon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))
       
    def apply_psv_sf(self,is_mc,data):
        if is_mc and ("psv_sf" in self.config
                       and len(self.config["psv_sf"]) > 0):
           weight, systematics = self.compute_psv_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_nonprompt_lepton_sf(self,data):
        if ("nonprompt_lepton_sf" in self.config
             and len(self.config["nonprompt_lepton_sf"]) > 0):
            weight = self.compute_nonprompt_lepton_sf(data)
            #print("nonprompt_lepton_sf",weight)
            return weight
        else:
            return np.ones(len(data))

    def apply_nonprompt_leptonsf_ttbar_closure(self,data):
        if ("nonprompt_leptonsf_all_ttbar_closure" in self.config
             and len(self.config["nonprompt_leptonsf_all_ttbar_closure"]) > 0):
            scale_factors = self.config["nonprompt_leptonsf_all_ttbar_closure"]
            #print("CHECK scale factors",scale_factors)
            weight = self.compute_nonprompt_leptonsf_closure(scale_factors,data)
            #print("nonprompt_lepton_sf_all",weight)
            return weight
        else:
            return np.ones(len(data))

    def apply_nonprompt_photon_sf_ttbar_double_nonprompt_closure(self,data):
        if ("nonprompt_photon_sf_ttbar" in self.config
             and len(self.config["nonprompt_photon_sf_ttbar"]) > 0):
            weight ,systematics = self.compute_nonprompt_photon_sf_ttbar_double_nonprompt_closure(data)
            #print("nonprompt_photon_sf_ttbar",weight)
            return weight,systematics
        else:
            return np.ones(len(data))

    def apply_nonprompt_MCcorrection_double_nonprompt_closure(self,data):
        if ("nonprompt_MCcorrection" in self.config
             and len(self.config["nonprompt_MCcorrection"]) > 0):
            weight = self.compute_nonprompt_MCcorrection_double_nonprompt_closure(data)
            #print("nonprompt_MCcorrection",weight)
            return weight
        else:
            return np.ones(len(data))

    def apply_photon_sf(self,is_mc,data):
        if is_mc and ("photon_sf" in self.config
                       and len(self.config["photon_sf"]) > 0):
           weight, systematics = self.compute_photon_sf(data)
           return weight, systematics
        else:
           return np.ones(len(data))

    def apply_btag_sf(self, is_mc, data):
        """Apply btag scale factors."""
        if is_mc and (
                "btag_sf" in self.config and len(self.config["btag_sf"]) != 0):
            weight, systematics = self.compute_weight_btag(data)
            return weight, systematics
        else:
            return np.ones(len(data))

    def compute_nonprompt_photon_sf_ttbar_double_nonprompt_closure(self, data):
        ones = np.ones(len(data))
        zeros = np.zeros(len(data))
        channels = ["fake_ele", "fake_muon"]
        nonprompt_photon_sf = self.config["nonprompt_photon_sf_ttbar_double_nonprompt_closure"]
        #print("check1",nonprompt_photon_sf[channels[0]].dimlabels)

        pho = data["Photon"]
        pho_pt = ak.where(pho.pt<500,pho.pt,499)

        njet = ak.num(data["Jet"])
        njet = ak.broadcast_arrays(njet,pho_pt)[0]

        central = ones
        for channel in channels:
            sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho_pt,
                                              Nj = njet
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)
            #print(channel,"central",central)

        # for mc closure, the way of applying weights is in add cut with categories option causing error in syst. run
        systematics = {}
        #if self.config["compute_systematics"] and self.config["only_nonprompt_syst"]:
        #    up = ones
        #    down = ones
        #    for channel in channels:
        #        key = "nonprompt_photon_sf"
        #        sf_up = ak.prod((nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
        #                                          pho_pt = pho_pt,
        #                                          Nj = njet,
        #                                          variation="up")),axis=1)
        #        up = ak.where(data[channel], sf_up, up)
        #        sf_down = ak.prod((nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
        #                                          pho_pt = pho_pt,
        #                                          Nj = njet,
        #                                          variation="down")),axis=1)
        #        down = ak.where(data[channel], sf_down, down)
        #        print(channel,"up",up)
        #        print(channel,"down",down)
        #        systematics[key] = (up / central, down / central)
        weights = ones * central
        #print("weights",weights)

        return weights, systematics

    def compute_nonprompt_MCcorrection_double_nonprompt_closure(self, data):
        pho = data["Photon"]
        pho = ak.pad_none(pho,1)
        ones = np.ones(len(data))
        central = ones
        channels = ["fake_ele", "fake_muon"]
        nonprompt_photon_sf = self.config["nonprompt_MCcorrection_double_nonprompt_closure"]
        pho_pt = ak.where(pho.pt<500,pho.pt,499)
        for channel in channels:
            sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho_pt,
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)

        # don't need syst for this k MC facotr, and due to the categories option in add cut for closure processor, 
        #                                       meet erros in syst. run 
        #if self.config["compute_systematics"] and self.config["only_nonprompt_syst"]:
        #    up = ones
        #    down = ones
        #    for channel in channels:
        #        sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
        #                                          pho_pt = pho_pt,
        #                                          variation="up")
        #        sf = ak.prod(sf,axis=1)
        #        up = ak.where(data[channel], sf, up)

        #        sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
        #                                          pho_pt = pho.pt,
        #                                          variation="down")
        #        sf = ak.prod(sf,axis=1)
        #        down = ak.where(data[channel], sf, down)
        #    return central, {"nonprompt_MCcorrection": (up / central, down / central)}
        return central
