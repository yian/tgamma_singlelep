# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.
# Here we create a very simplified version of the ttbar-to-dilep processor.
# One can run this processor using
# 'python3 -m pepper.runproc --debug example_processor.py example_config.json'
# Above command probably will need a little bit of time before all cuts are
# applied once. This is because a chunk of events are processed simultaneously.
# You change adjust the number of events in a chunk and thereby the memory
# usage by using the --chunksize parameter (the default value is 500000).

from functools import reduce
import numpy as np
import awkward as ak
import coffea
import coffea.lumi_tools
import coffea.jetmet_tools
import uproot
import logging
from dataclasses import dataclass
from typing import Optional, Tuple

import pepper
from pepper import sonnenschein, betchart
import pepper.config
from config_topgamma import ConfigTopGamma

from tools.utils import DeltaR

logger = logging.getLogger(__name__)

# All processors should inherit from pepper.ProcessorBasicPhysics
@dataclass
class BasicFuncs(pepper.ProcessorBasicPhysics):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = ConfigTopGamma

    def __init__(self, config, eventdir):
        super().__init__(config, eventdir)

    def pick_electrons(self, data):
        ele = data["Electron"]

        # We do not want electrons that are between the barrel and the end cap
        # For this, we need the eta of the electron with respect to its
        # supercluster
        sc_eta_abs = abs(ele.eta + ele.deltaEtaSC)
        is_in_transreg = (1.4442 < sc_eta_abs) & (sc_eta_abs < 1.566)
        impact = ( (sc_eta_abs<1.4442) & (abs(ele.dz) < 0.1) & (abs(ele.dxy) < 0.05) ) | ( (sc_eta_abs>1.566) & (abs(ele.dz) < 0.2) & (abs(ele.dxy) < 0.1) )

        # Electron ID, as an example we use the MVA one here
#        has_id = ele.mvaFall17V2Iso_WP90
        has_id = ele.cutBased >= 3

        # Finally combine all the requirements
        is_good = (
            has_id
            & impact
            & (~is_in_transreg)
            & (self.config["ele_eta_min"] < ele.eta)
            & (ele.eta < self.config["ele_eta_max"])
            & (self.config["good_ele_pt_min"] < ele.pt))

        veto_id = ele.cutBased >=1

        is_veto = (
                veto_id
              & (~is_in_transreg)
              & (self.config["ele_eta_min"] < ele.eta)
              & (ele.eta < self.config["ele_eta_max"])
              & (self.config["veto_ele_pt_min"] < ele.pt))

        ele_hem1516 = self.in_hem1516(ele.phi, ele.eta)
        if self.config["hem_cut_if_ele"]:
           is_good = is_good & ~ele_hem1516
           is_veto = is_veto & ~ele_hem1516

        # Return all electrons with are deemed to be good
        return {"Electron": ele[is_good], "VetoEle": ele[is_veto]}

    def pick_muons(self, data):
        muon = data["Muon"]
        etacuts = (self.config["muon_eta_min"] < muon.eta) & (muon.eta < self.config["muon_eta_max"])

        good_id = muon.tightId
        good_iso = muon.pfIsoId > 3
        is_good = (
            good_id
            & good_iso
            & etacuts
            & (self.config["good_muon_pt_min"] < muon.pt))

        veto_id = muon.looseId
        veto_iso = muon.pfIsoId >= 1
        is_veto = (
            veto_id
            & veto_iso
            & etacuts
            & (self.config["veto_muon_pt_min"] < muon.pt))

        muon_hem1516 = self.in_hem1516(muon.phi, muon.eta)
        if self.config["hem_cut_if_muon"]:
           is_good = is_good & ~muon_hem1516
           is_veto = is_veto & ~muon_hem1516

        return {"Muon": muon[is_good], "VetoMuon": muon[is_veto]}

    def add_PV_cut(self,data):
       PV = data["PV"]
       oneGoodPV = PV.npvsGood >0
       return oneGoodPV

    def build_lepton_column(self, is_mc, rng, data):
        """Build a lepton column containing electrons and muons."""
        electron = data["Electron"]
        muon = data["Muon"]
        # Apply Rochester corrections to muons
        if "muon_rochester" in self.config:
            muon = self.apply_rochester_corr(muon, rng, is_mc)
        columns = ["pt", "eta", "phi", "mass", "pdgId","charge"]
        if is_mc:
           columns.append("genPartIdx")
        lepton = {}
        for column in columns:
            lepton[column] = ak.concatenate([electron[column], muon[column]],
                                            axis=1)
        lepton = ak.zip(lepton, with_name="PtEtaPhiMLorentzVector",
                        behavior=data.behavior)

        # Sort leptons by pt
        # Also workaround for awkward bug using ak.values_astype
        # https://github.com/scikit-hep/awkward-1.0/issues/1288
        lepton = lepton[
            ak.values_astype(ak.argsort(lepton["pt"], ascending=False), int)]
        return lepton

    def lepton_charge(self, data):
        charge = data["Lepton"].charge
        return charge

    def pick_medium_photons(self, data):
        photon = data["Photon"]
        leptons = data["Lepton"]
        has_id = photon.cutBased>=2 # medium ID
        pass_psv = (photon.pixelSeed==False)

        is_in_EBorEE = (photon.isScEtaEB | photon.isScEtaEE)

        etacuts = (abs(photon["eta"])<2.5)
        ptcuts = (photon.pt>20)

        has_lepton_close = ak.any(
            photon.metric_table(leptons) < 0.4, axis=2)

        is_good = (
                has_id
          & (~has_lepton_close)
              & pass_psv
              & etacuts
              & is_in_EBorEE
              & ptcuts)

        pho_hem1516 = self.in_hem1516(photon.phi, photon.eta)
        if self.config["hem_cut_if_pho"]:
           is_good = is_good & ~pho_hem1516

        return photon[is_good]

    def pick_loose_photons(self, data):
        photon = data["Photon"]
        leptons = data["Lepton"]
        has_id = photon.cutBased>=2 # medium ID
        pass_psv = (photon.pixelSeed==False)

        is_in_EBorEE = (photon.isScEtaEB | photon.isScEtaEE)
        is_in_transreg = ( (1.4442 < abs(photon["eta"])) & (abs(photon["eta"]) < 1.566) )

        etacuts = (abs(photon["eta"])<2.5)
        ptcuts = (photon.pt>20)

        has_lepton_close = ak.any(
            photon.metric_table(leptons) < 0.4, axis=2)

        bitMap = photon["vidNestedWPBitmap"]

        passHoverE = (bitMap>>4&3)  >= 2
        passSIEIE = (bitMap>>6&3)  >= 2
        passChIso = (bitMap>>8&3)  >= 2
        passNeuIso = (bitMap>>10&3) >= 2
        passPhoIso = (bitMap>>12&3) >= 2

        isRelMediumPhoton = ((passHoverE) & (passNeuIso) & (passPhoIso))
        #isRelMediumPhoton = ((passHoverE) & (passNeuIso) & (passPhoIso) & (passChIso) & ~(passSIEIE))
        is_good = (
                isRelMediumPhoton
              & ~is_in_transreg
              & (is_in_EBorEE)
              & etacuts
              & pass_psv
              & (~has_lepton_close)
              & ptcuts)

        pho_hem1516 = self.in_hem1516(photon.phi, photon.eta)
        if self.config["hem_cut_if_pho"]:
           is_good = is_good & ~pho_hem1516

        return photon[is_good]

    def photon_categories(self,data):
        cats = {}
        photons = data["Photon"]
        genpart = data["GenPart"]

        # photons matched to a gen Pho
        true_photons = photons[ak.fill_none(abs(photons.matched_gen.pdgId) == 22, False)]
        # photons matched to a gen Ele
        electron_matched = photons[ak.fill_none(abs(photons.matched_gen.pdgId) == 11, False)]
        # photons can't matched to any gen Obj
        unmatched_photons = photons[ak.is_none(photons.matched_gen,axis=1)]

        promptmatch = true_photons.matched_gen.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | (true_photons.matched_gen.hasFlags(['isPromptTauDecayProduct'])) |
                        (true_photons.matched_gen.hasFlags(["fromHardProcess"])))

        #DR between gen and reco photon
        dr_reco_gen = ak.any(true_photons.matched_gen.metric_table(true_photons) < 0.3, axis=2)

        promptmatch = promptmatch & (dr_reco_gen)

        prompt_photons = true_photons[promptmatch]
        nonprompt_photons = true_photons[~(promptmatch)]

        prompt_photons_event = (ak.num(prompt_photons)>0)
        ele_matched_event = ( (ak.num(prompt_photons)==0) & (ak.num(electron_matched)>0) )
        nonprompt_event = ( (ak.num(prompt_photons)==0) & ((ak.num(nonprompt_photons)>0) | (ak.num(unmatched_photons)>0)) )

        cats["prompt"] = ak.fill_none(prompt_photons_event,False,axis=1)
        cats["ele_matched"] = ak.fill_none(ele_matched_event,False,axis=1)
        cats["nonprompt"] = ak.fill_none(nonprompt_event,False,axis=1)
        cats["allphoton"] = ak.num(photons)>0

        return cats

    def photon_categories_data(self,data):
        leptons = data["Lepton"]

        cats = {}
        cats["allphoton"] = (ak.num(leptons)>0)
        return cats

    def good_jet(self, data):
        """Apply some basic jet quality cuts."""
        jets = data["Jet"]
        leptons = data["Lepton"]
        photons = data["Photon"]

        j_id, j_puId, lep_dist, pho_dist, eta_min, eta_max, pt_min = self.config[[
            "good_jet_id", "good_jet_puId", "good_jet_lepton_distance", "good_jet_photon_distance",
            "good_jet_eta_min", "good_jet_eta_max", "good_jet_pt_min"]]

        if j_id == "skip":
            has_id = True
        elif j_id == "cut:loose":
            has_id = jets.isLoose
            # Always False in 2017 and 2018
        elif j_id == "cut:tight":
            has_id = jets.isTight
        elif j_id == "cut:tightlepveto":
            has_id = jets.isTightLeptonVeto
        else:
            raise pepper.config.ConfigError(
                    "Invalid good_jet_id: {}".format(j_id))

        j_pt = jets.pt
        if "jetfac" in ak.fields(data):
            j_pt = j_pt * data["jetfac"]

        has_lepton_close = ak.any(
            jets.metric_table(leptons) < lep_dist, axis=2)
        has_photon_close = ak.any(
            jets.metric_table(photons) < pho_dist, axis=2)
        is_good = (   (has_id)
                    & (j_pt > pt_min)
                    & (eta_min < jets.eta)
                    & (jets.eta < eta_max)
                    & (~has_lepton_close)
                    & (~has_photon_close)
                  )

        jet_hem1516 = self.in_hem1516(jets.phi, jets.eta)
        if self.config["hem_cut_if_jet"]:
           is_good = is_good & ~jet_hem1516

        return is_good

    def passing_hlt(self,trigger_path,data):
        hlt = data["HLT"]
        triggered = np.full(len(data), False)
        channel_ele = data["ele"]

        if "16" in self.config["year"]:
           for trig_p in trigger_path:
               triggered = triggered | np.asarray(hlt[trig_p])
        elif "17" in self.config["year"]:
           trigobjs_ele = ak.sum(data["TrigObj"].id == 11,axis=1)>0
           pass_L1SingleEGOrFilter = ak.sum((data["TrigObj"].filterBits & (1 << 10) == (1 << 10)),axis=1)>0
           pass_eleTrig_final = (np.asarray(hlt[trigger_path]) & trigobjs_ele & pass_L1SingleEGOrFilter)
           #print("<<<<<<",hlt[trigger_path])
           #print("<<<<<<",np.asarray(hlt[trigger_path]))

           pass_hlt = ak.where(channel_ele,pass_eleTrig_final,np.asarray(hlt[trigger_path]))
           triggered |= np.asarray(pass_hlt)

           #print(">>>> channel",data["muon"],' Orig trig ',triggered_test,ak.sum(triggered_test))
           #print(">>>> channel",data["muon"],' new trig ', triggered,ak.sum(triggered))

        elif "18" in self.config["year"]:
           triggered |= np.asarray(hlt[trigger_path])

        return triggered

    def compute_electron_sf(self, data):
        # Electron reconstruction and identification
        eles = data["Electron"]
        weight = np.ones(len(data))
        systematics = {}
        # Electron identification efficiency
        for i, sffunc in enumerate(self.config["electron_sf"]):
            pt_th = self.config["electron_sf_overflow_pt"][i]
            eles_pt =  ak.where(eles.pt<pt_th,eles.pt,pt_th-1)
            sceta = eles.eta + eles.deltaEtaSC
            params = {}
            for dimlabel in sffunc.dimlabels:
                if dimlabel == "abseta":
                    params["abseta"] = abs(sceta)
                elif dimlabel == "eta":
                    params["eta"] = sceta
                elif dimlabel == "pt":
                    params["pt"] = eles_pt
                else:
                    params[dimlabel] = getattr(eles, dimlabel)
            central = ak.prod(sffunc(**params), axis=1)
            key = "electronsf{}".format(i)
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                up = ak.prod(sffunc(**params, variation="up"), axis=1)
                down = ak.prod(sffunc(**params, variation="down"), axis=1)
                systematics[key] = (up / central, down / central)
            weight = weight * central

        return weight, systematics

    def compute_muon_sf(self, data):
        # Muon identification and isolation efficiency
        muons = data["Muon"]
        weight = np.ones(len(data))
        systematics = {}
        for i, sffunc in enumerate(self.config["muon_sf"]):
            pt_th = self.config["muon_sf_overflow_pt"][i]
            muons_pt = ak.where(muons.pt<pt_th,muons.pt,pt_th-1)
            #print(i,"print the overflow pt",self.config["muon_sf_overflow_pt"][i])
            params = {}
            for dimlabel in sffunc.dimlabels:
                if   dimlabel == "abseta":
                    params["abseta"] = abs(muons.eta)
                elif dimlabel == "pt": 
                    params["pt"] = muons_pt
                else:
                    params[dimlabel] = getattr(muons, dimlabel)
            central = ak.prod(sffunc(**params), axis=1)
            key = f"muonsf{i}"
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                if ("split_muon_uncertainty" not in self.config
                        or not self.config["split_muon_uncertainty"]):
                    unctypes = ("",)
                else:
                    unctypes = ("stat ", "syst ")
                for unctype in unctypes:
                    up = ak.prod(sffunc(
                        **params, variation=f"{unctype}up"), axis=1)
                    down = ak.prod(sffunc(
                        **params, variation=f"{unctype}down"), axis=1)
                    systematics[key + unctype.replace(" ", "")] = (
                        up / central, down / central)
            weight = weight * central
        return weight, systematics

    def compute_nonprompt_lepton_sf(self, data):
        pho = data["Photon"][:,0]
        ones = np.ones(len(data))
        central = ones
        channels = ["ele", "muon"]
        nonprompt_lepton_sf = self.config["nonprompt_lepton_sf"]
        njet =ak.num(data["Jet"])
        for channel in channels:
            sf = nonprompt_lepton_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho.pt,
                                              Nj = ak.num(data["Jet"])
                                             )
            central = ak.where(data[channel], sf, central)
        if self.config["compute_systematics"] and self.config["only_nonprompt_syst"]:
            up = ones
            down = ones
            for channel in channels:
                sf = nonprompt_lepton_sf[channel](pho_abseta = abs(pho.eta),
                                                  pho_pt = pho.pt,
                                                  Nj = ak.num(data["Jet"]),
                                                  variation="up")
                up = ak.where(data[channel], sf, up)
                sf = nonprompt_lepton_sf[channel](pho_abseta = abs(pho.eta),
                                                  pho_pt = pho.pt,
                                                  Nj = ak.num(data["Jet"]),
                                                  variation="down")
                down = ak.where(data[channel], sf, down)
            return central, {"nonprompt_lepton_sf": (up / central, down / central)}
        return central

    def compute_nonprompt_leptonsf_all(self, data):

        ones = np.ones(len(data))
        zeros = np.zeros(len(data))
        channels = ["ele", "muon"]
        nonprompt_lepton_sf = self.config["nonprompt_leptonsf_all"]

        lepton = data["Lepton"]
        lepton = ak.pad_none(lepton,1)
        lepton_pt = ak.where(lepton.pt<60,lepton.pt,59)

        central = ones
        for channel in channels:
            sf = nonprompt_lepton_sf[channel](lep_pt = lepton_pt,
                                              lep_abseta = abs(lepton.eta),
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)
            #print(channel,central)

        if self.config["compute_systematics"] and self.config["only_nonprompt_syst"]:
            systematics = {}
            for i, sffunc in enumerate(
                      [self.config["nonprompt_leptonsf_all_unc_SB"],self.config["nonprompt_leptonsf_all_unc_coarse_bin"]]):
                diff = zeros
                up = ones
                down = ones
                central_new = ones
                key = "nonprompt_lepton_sf{}".format(i)
                for channel in channels:
                    sf = nonprompt_lepton_sf[channel](lep_pt = lepton_pt,
                                                      lep_abseta = abs(lepton.eta))
                    sf_new = sffunc[channel](lep_pt = lepton_pt,
                                                      lep_abseta = abs(lepton.eta))

                    sf = ak.prod(sf,axis=1)
                    sf_new = ak.prod(sf_new,axis=1)
                    central_new = ak.where(data[channel], sf_new, central_new)

                    diff = ak.where(abs(diff)>sf,np.sign(diff)*sf,diff)
                    diff = ak.where(data[channel], (sf_new - sf), diff)

                    up = ak.where(data[channel], sf+diff, up)
                    down = ak.where(data[channel], sf-diff, down)
                    systematics[key] = (up / central, down / central)

                    #print(channel,"####### central ",central_new)
                    #print(channel,"####### diff ",diff)
                    #print(channel,"####### up ",up)
                    #print(channel,"####### down ",down)

            return central, systematics
        return central

    def compute_nonprompt_leptonsf_closure(self,scale_factors, data):
        lepton = data["Lepton"][:,0]
        lepton_pt = ak.where(lepton.pt<60,lepton.pt,59)
        ones = np.ones(len(data))
        central = ones
        channels = ["fake_ele", "fake_muon"]
        #nonprompt_lepton_sf = self.config["nonprompt_leptonsf_all_closure"]
        nonprompt_lepton_sf = scale_factors
        njet =ak.num(data["Jet"])
        for channel in channels:
            sf = nonprompt_lepton_sf[channel](lep_pt = lepton.pt,
                                              lep_abseta = abs(lepton.eta),
                                             )
            central = ak.where(data[channel], sf, central)

        # for mc closure, the way of applying weights is in add cut with categories option causing error in syst. run
        #if self.config["compute_systematics"]:
        #    up = ones
        #    down = ones
        #    for channel in channels:
        #        sf = nonprompt_lepton_sf[channel](lep_pt = lepton_pt,
        #                                          lep_abseta = abs(lepton.eta),
        #                                          variation="up")
        #        up = ak.where(data[channel], sf, up)
        #        sf = nonprompt_lepton_sf[channel](lep_pt = lepton_pt,
        #                                          lep_abseta = abs(lepton.eta),
        #                                          variation="down")
        #        down = ak.where(data[channel], sf, down)
        #    return central, {"nonprompt_lepton_sf": (up / central, down / central)}
        return central

    def compute_photon_sf(self, data):
        # Photon identification+PSV SFs
        phos = data["Photon"]
        weight = np.ones(len(data))
        systematics = {}
        for i, sffunc in enumerate(self.config["photon_sf"]):
            pt_th = self.config["photon_sf_overflow_pt"][i]
            phos_pt = ak.where(phos.pt<pt_th,phos.pt,pt_th-1)
            params = {}
            for dimlabel in sffunc.dimlabels:
                if dimlabel == "abseta":
                    params["abseta"] = abs(phos.eta)
                elif dimlabel == "pt":
                    params["pt"] = phos_pt
                else:
                    params[dimlabel] = getattr(phos, dimlabel)
            central = ak.prod(sffunc(**params), axis=1)
            key = "photonsf{}".format(i)
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                up = ak.prod(sffunc(**params, variation="up"), axis=1)
                down = ak.prod(sffunc(**params, variation="down"), axis=1)
                systematics[key] = (up / central, down / central)

            weight = weight * central
        return weight, systematics

    def compute_psv_sf(self, data):
        phos = data["Photon"]
        phos["etabin"] = ak.where(abs(phos.eta) < 1.5, 0.5, 3.5)
        weight = np.ones(len(data))
        systematics = {}
        # Photon identification+PSV SFs
        for i, sffunc in enumerate(self.config['psv_sf']):
            central = ak.prod(sffunc(etabin=phos["etabin"]),axis=1)
            key = "PSVsf{}".format(i)
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                up = ak.prod(sffunc(
                    etabin=phos["etabin"], variation="up"),axis=1)
                down = ak.prod(sffunc(
                    etabin=phos["etabin"], variation="down"),axis=1)
                systematics[key] = (up / central, down / central)
            weight = weight * central

        return weight, systematics

    def compute_nonprompt_photon_sf(self, data):
        ones = np.ones(len(data))
        zeros = np.zeros(len(data))
        channels = ["ele", "muon"]
        nonprompt_photon_sf = self.config["nonprompt_photon_sf"]
        #print("check1",nonprompt_photon_sf["ele"].dimlabels)

        pho = data["Photon"]
        pho_pt = ak.where(pho.pt<500,pho.pt,499)

        njet = ak.num(data["Jet"])
        njet = ak.broadcast_arrays(njet,pho_pt)[0]

        central = ones
        for channel in channels:
            sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho_pt,
                                              Nj = njet
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)
            #print(channel,central)

        if self.config["only_nonprompt_syst"] and self.config["compute_systematics"]:
           systematics = {}
           for i, sffunc in enumerate(
                      [self.config["nonprompt_photon_sf_unc_SB"],self.config["nonprompt_photon_sf_unc_coarse_bin"]]):
               diff = zeros
               up = ones
               down = ones
               central_new = ones
               key = "nonprompt_photon_sf{}".format(i)
               for channel in channels:
                   sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                                     pho_pt = pho_pt,
                                                     Nj = njet
                                                    )
                   sf_new = sffunc[channel](pho_abseta = abs(pho.eta),
                                                     pho_pt = pho_pt,
                                                     Nj = njet
                                           )
                   sf = ak.prod(sf,axis=1)
                   sf_new = ak.prod(sf_new,axis=1)
                   central_new = ak.where(data[channel], sf_new, central_new)
                   diff = ak.where(abs(diff)>sf,np.sign(diff)*sf,diff)
                   diff = ak.where(data[channel], (sf_new - sf), diff)

                   up = ak.where(data[channel], sf+diff, up)
                   down = ak.where(data[channel], sf-diff, down)
                   systematics[key] = (up / central, down / central)

                   #print(channel,"####### central ",central_new)
                   #print(channel,"####### diff ",diff)
                   #print(channel,"####### up ",up)
                   #print(channel,"####### down ",down)
           return central, systematics
        return central

    def compute_nonprompt_photon_sf_ttbar(self, data):
        ones = np.ones(len(data))
        zeros = np.zeros(len(data))
        channels = ["ele", "muon"]
        nonprompt_photon_sf = self.config["nonprompt_photon_sf_ttbar"]
        #print("check1",nonprompt_photon_sf["ele"].dimlabels)

        pho = data["Photon"]
        pho_pt = ak.where(pho.pt<500,pho.pt,499)

        njet = ak.num(data["Jet"])
        njet = ak.broadcast_arrays(njet,pho_pt)[0]

        central = ones
        for channel in channels:
            sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho_pt,
                                              Nj = njet
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)
            #print(channel,"central",central)

        # for mc closure, the way of applying weights is in add cut with categories option causing error in syst. run
        systematics = {}
        #if self.config["compute_systematics"] and self.config["only_nonprompt_syst"]:
        #    up = ones
        #    down = ones
        #    for channel in channels:
        #        key = "nonprompt_photon_sf"
        #        sf_up = ak.prod((nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
        #                                          pho_pt = pho_pt,
        #                                          Nj = njet,
        #                                          variation="up")),axis=1)
        #        up = ak.where(data[channel], sf_up, up)
        #        sf_down = ak.prod((nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
        #                                          pho_pt = pho_pt,
        #                                          Nj = njet,
        #                                          variation="down")),axis=1)
        #        down = ak.where(data[channel], sf_down, down)
        #        print(channel,"up",up)
        #        print(channel,"down",down)
        #        systematics[key] = (up / central, down / central)
        weights = ones * central
        #print("weights",weights)
        return weights, systematics

    def compute_nonprompt_photonsf_all(self, data):
        pho = data["Photon"]
        pho = ak.pad_none(pho,1)
        ones = np.ones(len(data))
        central = ones
        channels = ["ele", "muon"]
        nonprompt_photon_sf = self.config["nonprompt_photonsf_all"]
        pho_pt = ak.where(pho.pt<500,pho.pt,499)

        for channel in channels:
            sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho_pt
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)
        if self.config["compute_systematics"] and self.config["only_nonprompt_syst"]:
            up = ones
            down = ones
            for channel in channels:
                sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                                  pho_pt = pho_pt,
                                                  variation="up")
                sf = ak.prod(sf,axis=1)
                up = ak.where(data[channel], sf, up)

                sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                                  pho_pt = pho_pt,
                                                  variation="down")
                sf = ak.prod(sf,axis=1)
                down = ak.where(data[channel], sf, down)
            return central, {"nonprompt_photon_sf": (up / central, down / central)}
        return central

    def compute_nonprompt_MCcorrection(self, data):
        pho = data["Photon"]
        pho = ak.pad_none(pho,1)
        ones = np.ones(len(data))
        central = ones
        channels = ["ele", "muon"]
        nonprompt_photon_sf = self.config["nonprompt_MCcorrection"]
        pho_pt = ak.where(pho.pt<500,pho.pt,499)
        for channel in channels:
            sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho_pt,
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)

        # don't need syst for this k MC facotr, and due to the categories option in add cut for closure processor, 
        #                                       meet erros in syst. run 
        #if self.config["compute_systematics"] and self.config["only_nonprompt_syst"]:
        #    up = ones
        #    down = ones
        #    for channel in channels:
        #        sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
        #                                          pho_pt = pho_pt,
        #                                          variation="up")
        #        sf = ak.prod(sf,axis=1)
        #        up = ak.where(data[channel], sf, up)

        #        sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
        #                                          pho_pt = pho.pt,
        #                                          variation="down")
        #        sf = ak.prod(sf,axis=1)
        #        down = ak.where(data[channel], sf, down)
        #    return central, {"nonprompt_MCcorrection": (up / central, down / central)}
        return central

    def compute_jet_puid_sfs(self,is_mc,data):
        jets = data["Jet"][(data["Jet"].pt < 50) & (data["Jet"].has_gen_jet)]
        weight = np.ones(len(data))
        pass_puid = jets["pass_pu_id"]
        systematics = {}
        sf_eff = []
        for i, sffunc in enumerate(self.config["jet_puid_sf"]):
            params = {}
            #print(sffunc)
            for dimlabel in sffunc.dimlabels:
                if dimlabel == "abseta":
                    params["abseta"] = abs(jets.eta)
                else:
                    params[dimlabel] = getattr(jets, dimlabel)
            sf_eff.append(sffunc(pt=jets.pt,eta=jets.eta))
        prob_mc = ak.prod(ak.where(pass_puid,sf_eff[0],1-sf_eff[0]),axis=1)
        prob_data = ak.prod(ak.where(pass_puid,sf_eff[0]*sf_eff[1],1-sf_eff[0]*sf_eff[1]),axis=1)

        if is_mc:
           central = prob_data/prob_mc
        else:
           central = np.ones(len(data))

        if (self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]
               and len(self.config["jet_puid_sf_unc"])>0 and is_mc):
           for i, sffunc in enumerate(self.config["jet_puid_sf_unc"]):
               #print(sffunc)
               up = sf_eff[1]+sffunc(pt=jets.pt,eta=jets.eta)
               down = sf_eff[1]-sffunc(pt=jets.pt,eta=jets.eta)
               prob_data_up = ak.prod(ak.where(pass_puid,sf_eff[0]*up,1-sf_eff[0]*up),axis=1)
               prob_data_down = ak.prod(ak.where(pass_puid,sf_eff[0]*down,1-sf_eff[0]*down),axis=1)
               up = prob_data_up/prob_mc
               down = prob_data_down/prob_mc
               key="puid_sf{}".format(i)
               systematics[key] = (up / central, down / central)

        weight = weight * central

        return  weight,systematics

    def compute_weight_btag(self, data, efficiency="central", never_sys=False):
        """Compute event weights and systematics, if requested, for the b
        tagging"""
        jets = data["CentralJet"]
        wp = self.config["btag"].split(":", 1)[1]
        flav = jets["hadronFlavour"]
        eta = abs(jets.eta)
        pt = jets.pt
        discr = jets["btag"]
        weight = np.ones(len(data))
        systematics = {}
        for i, weighter in enumerate(self.config["btag_sf"]):
            central = weighter(wp, flav, eta, pt, discr, "central", efficiency)
            if not never_sys and self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                if "btag_splitting_scheme" in self.config:
                    scheme = self.config["btag_splitting_scheme"].lower()
                elif ("split_btag_year_corr" in self.config and
                        self.config["split_btag_year_corr"]):
                    scheme = "years"
                else:
                    scheme = None
                if scheme is None:
                    light_unc_splits = heavy_unc_splits = {"": ""}
                elif scheme == "years":
                    light_unc_splits = heavy_unc_splits = \
                        {"corr": "_correlated", "uncorr": "_uncorrelated"}
                elif scheme == "sources":
                    heavy_unc_splits = {name: f"_{name}"
                                        for name in weighter.sources}
                    light_unc_splits = {"corr": "_correlated",
                                        "uncorr": "_uncorrelated"}
                else:
                    raise ValueError(
                        f"Invalid btag uncertainty scheme {scheme}")

                for name, split in heavy_unc_splits.items():
                    systematics[f"btagsf{i}" + name] = self.compute_btag_sys(
                        central, "heavy up" + split, "heavy down" + split,
                        weighter, wp, flav, eta, pt, discr, efficiency)
                for name, split in light_unc_splits.items():
                    systematics[f"btagsf{i}light" + name] = \
                        self.compute_btag_sys(
                            central, "light up" + split, "light down" + split,
                            weighter, wp, flav, eta, pt, discr, efficiency)
            weight = weight * central
        if never_sys:
            return weight
        else:
            return weight, systematics

    def build_centraljet_column(self,data):
        jets = data["Jet"]
        central_region = (abs(jets.eta)<2.5)
        centraljets = jets[central_region]

        return centraljets

    def build_forwardjet_column(self,data):
        jets = data["Jet"]
        forward_region = (abs(jets.eta)>2.5)
        forwardjets = jets[forward_region]

        return forwardjets

    def build_noBjet_column(self,data):
        jets = data["Jet"]
        btagged = jets["btagged"]
        noBjet = jets[~btagged]

        return noBjet

    def build_bjet_column(self,data):
        jets = data["CentralJet"]
        bjets = jets[data["CentralJet"].btagged]

        return bjets

    def build_loose_bjet_column(self,data):
        jets = data["CentralJet"]
        tagger, wp = self.config["loose_btag"].split(":")
        if tagger == "deepcsv":
            jets["loose_btag"] = jets["btagDeepB"]
        elif tagger == "deepjet":
            jets["loose_btag"] = jets["btagDeepFlavB"]
        else:
            raise pepper.config.ConfigError(
                "Invalid tagger name: {}".format(tagger))
        year = self.config["year"]
        wptuple = pepper.scale_factors.BTAG_WP_CUTS[tagger][year]
        if not hasattr(wptuple, wp):
            raise pepper.config.ConfigError(
                "Invalid working point \"{}\" for {} in year {}".format(
                    wp, tagger, year))
        accept = jets["loose_btag"] > getattr(wptuple, wp)
        loose_bjets = jets[accept]

        return loose_bjets

    def num_jets(self, data):
        return ak.num(data['Jet'])

    def num_btags(self, data):
        return ak.num(data['bJet'])

    def num_noBjet(self, data):
        return ak.num(data['noBjet'])

    def num_loose_btags(self, data):
        return ak.num(data['loose_bJet'])

    def num_forwardJets(self, data):
        return ak.num(data['ForwardJet'])

    def num_centralJets(self, data):
        return ak.num(data['CentralJet'])

    def build_lepton_prompt(self,data):
        lepton = data["Lepton"]
        prompt = self.lepton_isprompt(data)
        return ak.sum(prompt,axis=1)>0

    def build_W_column(self,data):
        nu = data["Neutrino1"]
        lep = data["Lepton"]
        wpt = (nu[:,0]+lep[:,0]).pt
        weta = (nu[:,0]+lep[:,0]).eta
        wphi = (nu[:,0]+lep[:,0]).phi
        wmass = (nu[:,0]+lep[:,0]).mass
        wp = wpt*np.cosh(weta)
        wenergy = np.sqrt(wmass*wmass+wp*wp)

        w = ak.zip({"pt": wpt, "eta": weta , "phi": wphi, "energy": wenergy},
                   with_name="PtEtaPhiELorentzVector",behavior=data.behavior)
        #print(w.pt,w.eta,w.energy)
        return w

    def build_mtw_column(self,data):
        lepton = data["Lepton"]
        MET = data["MET"]
        #print(lepton.pt)
        #print((ak.num(lepton)>0))
        delta_phi = ak.where( (ak.num(lepton)>0),(lepton[:, 0].phi-MET.phi),0)
        mTW = ak.where( (ak.num(lepton)>0),(np.sqrt(lepton[:,0].pt*MET.pt*(1-np.cos(delta_phi)))),0)
        return mTW

    def build_deltaR_lgamma(self,data):
        lepton = data["Lepton"]
        photon = data["Photon"]
        deltaR_lg = DeltaR(lepton[:,0],photon[:,0])
        return deltaR_lg

    def build_deltaR_ljet(self,data):
        lepton = data["Lepton"]
        jets = data["Jet"]
        deltaR_lj = DeltaR(lepton[:,0],jets[:,0])
        return deltaR_lj

    def build_deltaR_jgamma(self,data):
        jets = data["Jet"]
        photon = data["Photon"]
        deltaR_jg = DeltaR(jets[:,0],photon[:,0])
        return deltaR_jg

    def dummycut(self,data):
        return ak.num(data["Lepton"]) >= 0

    def do_top_pt_reweighting(self, data):
        """Top pt reweighting according to
        https://twiki.cern.ch/twiki/bin/view/CMS/TopPtReweighting
        """
        pt = data["gent_lc"].pt
        weiter = self.config["top_pt_reweighting"]
        rwght = weiter(pt[:, 0], pt[:, 1])
        if weiter.sys_only:
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                return np.full(len(data), True), {"Top_pt_reweighting": rwght}
            else:
                return np.full(len(data), True)
        else:
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                return rwght, {"Top_pt_reweighting": 1/rwght}
            else:
                return rwght

    def do_pileup_reweighting(self, dsname, data):
        """Pileup reweighting"""
        ntrueint = data["Pileup"]["nTrueInt"]
        weighter = self.config["pileup_reweighting"]
        weight = weighter(dsname, ntrueint)
        if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
            # If central is zero, let up and down factors also be zero
            weight_nonzero = np.where(weight == 0, np.inf, weight)
            up = weighter(dsname, ntrueint, "up")
            down = weighter(dsname, ntrueint, "down")
            sys = {"pileup": (up / weight_nonzero, down / weight_nonzero)}
            return weight, sys
        return weight

    def remove_stnlo_overlap(self, dsname, data):
        if (not "ST_t-channel" in dsname and not dsname.startswith("TGJets_lepton")) :
            accept = ak.num(data["Lepton"]) > 0
            return accept
    
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
    
        eta_cut = abs(genphoton["eta"])<2.6
        pt_cut = genphoton["pt"]>10
        genphoton = genphoton[eta_cut & pt_cut]
    
        #isPrompt: not from hadron and tau decay
        promptmatch = genphoton.hasFlags(['isPrompt'])
    
        mother = genphoton.parent
        not_from_top = (genphoton['pdgId']==22) #always from True 
        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
            mother = mother.parent
    
        mother = genphoton.parent
        from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
        from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
                 & (~not_from_top) )
        from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
                 & (~not_from_top) )
    
        from_decayProd = (from_lepton | from_W | from_b)
    
        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))
    
        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.05, axis=2) # isolated in the cone size of 0.05, loose cut
    
        genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close]
    
        if "ST_t-channel" in dsname :
            accept = (ak.num(genphoton)==0)
        elif dsname.startswith("TGJets") :
            accept = (ak.num(genphoton)>0)

        return accept

    def remove_ttlo_overlap(self,dsname, data):
    
        if (not "TTTo" in dsname and not dsname.startswith("TTGamma")) :
            accept = ak.num(data["Lepton"]) > 0
            return accept
    
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
    
        eta_cut = abs(genphoton["eta"])<5.0
        pt_cut = genphoton["pt"]>10
        genphoton = genphoton[eta_cut & pt_cut]
    
        #isPrompt: not from hadron and tau decay
        promptmatch = genphoton.hasFlags(['isPrompt'])
    
        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))
    
        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.1, axis=2) # the ptg in run card is 0, the R0gamma setting is disabled, select the proper value of 0.1 tigher than the 0.05 option used in the NLO removal
    
        genphoton = genphoton[promptmatch & ~has_part_close]
    
        if "TTTo" in dsname :
            accept = (ak.num(genphoton)==0)
        elif dsname.startswith("TTGamma"):
            accept = (ak.num(genphoton)>0)
        return accept
    
    def remove_ttnlo_mg5_overlap(self,dsname, data):
    
        if (not "TTJets" in dsname and not dsname.startswith("TTGJets") ) :
            accept = ak.num(data["Lepton"]) > 0
            return accept
    
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
    
        pt_cut = genphoton["pt"]>10
        eta_cut = abs(genphoton["eta"])<2.6
        genphoton = genphoton[eta_cut & pt_cut]
    
        #isPrompt: not from hadron and tau decay
        promptmatch = genphoton.hasFlags(['isPrompt'])
    
        mother = genphoton.parent
        not_from_top = (genphoton['pdgId']==22) #always from True
        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
            mother = mother.parent
    
        mother = genphoton.parent
        from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
        from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
                 & (~not_from_top) )
        from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
                 & (~not_from_top) )
    
        from_decayProd = (from_lepton | from_W | from_b)
    
        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                            (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                            (abs(genpart["pdgId"])!=16))
    
        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.05, axis=2)
    
        genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close]
    
        if "TTJets" in dsname :
            accept = (ak.num(genphoton)==0)
        elif dsname.startswith("TTGJets"):
            accept = (ak.num(genphoton)>0)
        return accept
    
    def remove_ttnlo_powheg_overlap(self,dsname, data):
    
        if (not "TTTo" in dsname and not dsname.startswith("TTGJets") ) :
            accept = ak.num(data["Lepton"]) > 0
            return accept
    
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
    
        pt_cut = genphoton["pt"]>10
        eta_cut = abs(genphoton["eta"])<2.6
        genphoton = genphoton[eta_cut & pt_cut]
    
        #isPrompt: not from hadron and tau decay
        promptmatch = genphoton.hasFlags(['isPrompt'])
    
        mother = genphoton.parent
        not_from_top = (genphoton['pdgId']==22) #always from True 
        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
            mother = mother.parent
    
        mother = genphoton.parent
        from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
        from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
                 & (~not_from_top) )
        from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
                 & (~not_from_top) )
    
        from_decayProd = (from_lepton | from_W | from_b)
    
        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                            (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                            (abs(genpart["pdgId"])!=16))
    
        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.05, axis=2)
    
        genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close]
    
        if "TTTo" in dsname :
            accept = (ak.num(genphoton)==0)
        elif dsname.startswith("TTGJets"):
            accept = (ak.num(genphoton)>0)
        return accept
    
    def remove_stW_overlap(self,dsname, data):
    
        if (not "ST_tW" in dsname) :
            accept = ak.num(data["Lepton"]) > 0
            return accept
    
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
    
        eta_cut = abs(genphoton["eta"])<5.0
        pt_cut = genphoton["pt"]>10
        genphoton = genphoton[eta_cut & pt_cut]
    
        #isPrompt: not from hadron and tau decay
        promptmatch = genphoton.hasFlags(['isPrompt'])
    
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                            (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                            (abs(genpart["pdgId"])!=16))
    
        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.1, axis=2) # the ptg in run card is 0, so the R0gamma setting is disabled, select the proper value of 0.1
    
        genphoton = genphoton[promptmatch & ~has_part_close]
    
        if "ST_tW_" in dsname :
            accept = (ak.num(genphoton)==0)
        elif "ST_tWA" in dsname :
            accept = (ak.num(genphoton)>0)
        return accept
    
    def remove_z_overlap(self,dsname, data):
    
        if (not "DYJetsToLL" in dsname and not "ZG" in dsname) :
            accept = ak.num(data["Lepton"]) > 0
            return accept
    
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
    
        #for DY/Zgamma there is a madgraph cut of photon eta <2.6
        eta_cut = abs(genphoton["eta"])<2.6
        pt_cut = genphoton["pt"]>15
        genphoton = genphoton[eta_cut & pt_cut]
    
        #isPrompt: not from hadron and tau decay
        promptmatch = genphoton.hasFlags(['isPrompt'])
    
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                            (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                            (abs(genpart["pdgId"])!=16))
    
        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.05, axis=2)
    
        genphoton = genphoton[promptmatch & ~has_part_close]
    
        if "DYJetsToLL" in dsname :
            accept = ak.num(genphoton)==0
        elif "ZG" in dsname :
            accept = ak.num(genphoton)>0
    
        return accept
    
    def remove_w_overlap(self,dsname, data):
    
        if (not dsname.startswith("WJetsToLNu") and not "WG" in dsname) :
            accept = ak.num(data["Lepton"]) > 0
            return accept
    
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
    
        #for W+jets/Wgamma there is a madgraph cut of photon eta <2.6
        eta_cut = abs(genphoton["eta"])<2.6
        pt_cut = genphoton["pt"]>15
        genphoton = genphoton[eta_cut & pt_cut]
    
        #isPrompt: not from hadron and tau decay
        promptmatch = genphoton.hasFlags(['isPrompt'])
    
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                            (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                            (abs(genpart["pdgId"])!=16))
    
        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.05, axis=2)
    
        genphoton = genphoton[promptmatch & ~has_part_close]
    
        if dsname.startswith("WJetsToLNu"):
            accept = ak.num(genphoton)==0
        elif "WG" in dsname :
            accept = ak.num(genphoton)>0
    
        return accept

    def add_pdf_uncertainties_per_bin(self, dsname, selector, bins):
        """Add PDF uncertainties, using the methods described here:
        https://arxiv.org/pdf/1510.03865.pdf#section.6"""
        isSignal = ( (dsname.startswith("TGJets_")) or (dsname.startswith("ST_t-channel_")) or
                             (dsname.startswith("TTGJets_")) or (dsname.startswith("TTTo")) )
        data = selector.data
        if ("LHEPdfWeight" not in data.fields
                or "pdf_types" not in self.config):
            return
        split_pdf_uncs = False
        if "split_pdf_uncs" in self.config:
            split_pdf_uncs = self.config["split_pdf_uncs"]
        pdfs = data["LHEPdfWeight"]
        norm = np.abs(self.config["mc_lumifactors"][dsname + "_LHEPdfSumw"])
    
        if ak.sum(pdfs) == 0.:
            return
        pdf_type = None
        for LHA_ID, _type in self.config["pdf_types"].items():
            if LHA_ID in pdfs.__doc__:
                pdf_type = _type.lower()
        # Check if sample has alpha_s variations - currently assuming number of
        # regular variations is a multiple of 10
        if len(data) == 0:
            has_as_unc = False
        else:
            has_as_unc = (len(pdfs[0]) % 10) > 1
            # Workaround for "order of scale and pdf weights not consistent"
            # See https://twiki.cern.ch/twiki/bin/view/CMS/MCKnownIssues
            if ak.mean(pdfs[0]) < 0.6:  # approximate, see if factor 2 needed
                pdfs = ak.without_parameters(pdfs)
                pdfs = ak.concatenate([pdfs[:, 0:1], pdfs[:, 1:] * 2], axis=1)
        n_offset = -2 if has_as_unc else None
    
        if split_pdf_uncs:
            # Just output variations - user
            # will need to combine these for limit setting
            num_variation = len(pdfs[0]) + (n_offset or 0)
            if pdf_type == "true_hessian" or pdf_type == "hessian":
                # First element is central value - adjust all other
                # elements relative to this
                final_pdf_unc = np.empty(num_variation,dtype=object)
                for i in range(1, num_variation):
                    final_pdf_unc[i-1] = np.ones(len(data))
                    if isSignal and not (bins=='inclusive'):
                        #print(">>>> run for add pdf uncertainty per bin")
                        for bin in bins:
                            pdfs_bin = data[data[bin]]["LHEPdfWeight"]
                            pdf_unc = pdfs[:, i]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, i])  - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])
                            #awkward won't allow doing it all at once
                            current_unc = final_pdf_unc[i-1]
                            new_unc = ak.where(data[bin],pdf_unc,current_unc)
                            final_pdf_unc[i-1]=new_unc
                    else:
                        final_pdf_unc[i-1]=(pdfs[:, i]*norm[i] - pdfs[:, 0]*norm[0])
    
                    #symmetrize PDF uncertainties (not clear what recommendation is)
                    selector.set_systematic(
                        "PDF{}".format(i), 1+final_pdf_unc[i-1],1-final_pdf_unc[i-1])
    
                if has_as_unc:
                    final_as_weight_up = np.ones(len(data))
                    final_as_weight_down = np.ones(len(data))
    
                    if isSignal and not (bins=='inclusive'):
                        for bin in bins:
                            pdfs_bin = data[data[bin]]["LHEPdfWeight"]
                            pdf_as_up = (pdfs[:, -1]) * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, -1]) - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])
                            pdf_as_down = (pdfs[:, -2]) * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, -2]) - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])
    
                            current_as_up = final_as_weight_up
                            new_up = ak.where(data[bin],pdf_as_up,current_as_up)
                            final_as_weight_up=new_up
    
                            current_as_down = final_as_weight_down
                            new_down = ak.where(data[bin],pdf_as_down,current_as_down)
                            final_as_weight_down=new_down
    
                    else:
                        weight_up = (pdfs[:, -1])
                        weight_down = (pdfs[:, -2])
    
                        norm_factor_up =  ak.sum(data["genWeight"]) / ak.sum(data["genWeight"]*pdfs[:, -1])
                        norm_factor_down =  ak.sum(data["genWeight"]) / ak.sum(data["genWeight"]*pdfs[:, -2])
                        final_as_weight_up = weight_up*norm_factor_up - pdfs[:, 0]
                        final_as_weight_down = weight_down*norm_factor_down - pdfs[:, 0]
    
                    selector.set_systematic(
                        "PDFalphas",
                        final_as_weight_up+1,
                        final_as_weight_down+1)
    
            elif pdf_type.startswith("mc"):
                selector.set_systematic(
                    "PDF",
                    *[pdfs[:, i] for i in range(num_variation)],
                    scheme="numeric")
                if has_as_unc:
                    selector.set_systematic(
                        "PDFalphas", pdfs[:, -1], pdfs[:, -2])
            elif pdf_type is None:
                raise pepper.config.ConfigError(
                    "PDF LHA Id not included in config. PDF docstring is: "
                    + pdfs.__doc__)
            else:
                raise pepper.config.ConfigError(
                    f"PDF type {pdf_type} not recognised. Valid options "
                    "are 'True_Hessian', 'Hessian', 'MC' and 'MC_Gaussian'")
        else:
            if pdf_type == "true_hessian":
                # Treatment of true hessian uncertainties, for e.g. CTEQ
                # or HERA sets
                eigen_vals = ak.to_numpy(pdfs[:, 1:n_offset])
                eigen_vals = eigen_vals.reshape(
                    (eigen_vals.shape[0], eigen_vals.shape[1] // 2, 2))
                central, eigenvals = ak.broadcast_arrays(
                    pdfs[:, 0, None, None], eigen_vals)
                var_up = ak.max((eigen_vals - central), axis=2)
                var_up = ak.where(var_up > 0, var_up, 0)
                var_up = np.sqrt(ak.sum(var_up ** 2, axis=1))
                var_down = ak.max((central - eigen_vals), axis=2)
                var_down = ak.where(var_down > 0, var_down, 0)
                var_down = np.sqrt(ak.sum(var_down ** 2, axis=1))
                unc = None
    
            if pdf_type == "hessian":
                # Treatment of pseudo hessian uncertainties, for e.g. NNPDF
                # or pdf4LHC sets
                eigen_vals = ak.to_numpy(pdfs[:, 1:n_offset])
                variations = eigen_vals*norm[1:n_offset] - pdfs[:, 0, None]*norm[0]
                unc = np.sqrt(ak.sum(variations ** 2, axis=1))
    
            elif pdf_type == "mc":
                # ak.sort produces an error here. Work-around:
                variations = np.sort(ak.to_numpy(pdfs[:, 1:n_offset]))
                nvar = ak.num(variations)[0]
                unc = (variations[:, int(round(0.841344746*nvar))]
                       - variations[:, int(round(0.158655254*nvar))]) / 2
            elif pdf_type == "mc_gaussian":
                mean = ak.mean(pdfs[:, 1:n_offset], axis=1)
                unc = np.sqrt((ak.sum(pdfs[:, 1:n_offset] - mean) ** 2)
                              / (ak.num(pdfs)[0] - (3 if n_offset else 1)))
            elif pdf_type is None:
                raise pepper.config.ConfigError(
                    "PDF LHA Id not included in config. PDF docstring is: "
                    + pdfs.__doc__)
            else:
                raise pepper.config.ConfigError(
                    f"PDF type {pdf_type} not recognised. Valid options "
                    "are 'True_Hessian', 'Hessian', 'MC' and 'MC_Gaussian'")
    
            # Add PDF alpha_s uncertainties
            if has_as_unc:
                if ("combine_alpha_s" in self.config and
                        self.config["combine_alpha_s"]):
                    alpha_s_unc = (pdfs[:, -1]*norm[-1] - pdfs[:, -2]*norm[-2]) / 2
                    if unc is not None:
                        unc = np.sqrt(unc ** 2 + alpha_s_unc ** 2)
                        final_as_weight_up = unc
                        final_as_weight_down = unc
    
                    else:
                        var_up = np.sqrt(var_up ** 2 + alpha_s_unc ** 2)
                        var_down = np.sqrt(var_down ** 2 + alpha_s_unc ** 2)
                        final_as_weight_up = var_up
                        final_as_weight_down = var_down
    
                else:
                    final_as_weight_up = np.ones(len(data))
                    final_as_weight_down = np.ones(len(data))
    
                    if isSignal and not (bins=='inclusive'):
                        for bin in bins:
                            pdfs_bin = data[data[bin]]["LHEPdfWeight"]
                            pdf_as_up = (pdfs[:, -1]) * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, -1]) - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])
                            pdf_as_down = (pdfs[:, -2]) * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, -2]) - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])
    
                            current_as_up = final_as_weight_up
                            new_up = ak.where(data[bin],pdf_as_up,current_as_up)
                            final_as_weight_up=new_up
    
                            current_as_down = final_as_weight_down
                            new_down = ak.where(data[bin],pdf_as_down,current_as_down)
                            final_as_weight_down=new_down
    
                    else:
                        final_as_weight_up = (pdfs[:, -1]*norm[-1] - pdfs[:, 0]*norm[0])
                        final_as_weight_down = (pdfs[:, -2]*norm[-2] - pdfs[:, 0]*norm[0])
    
                    selector.set_systematic(
                        "PDFalphas",
                        final_as_weight_up+1,
                        final_as_weight_down+1)
    
            if unc is not None:
                selector.set_systematic("PDF", 1 + unc, 1 - unc)
            else:
                selector.set_systematic("PDF", 1 + var_up, 1 - var_down)
    
    def add_me_uncertainties_per_bin(self, dsname, selector, bins):
        """Matrix-element renormalization and factorization scale"""
        # Get describtion of individual columns of this branch with
        # Events->GetBranch("LHEScaleWeight")->GetTitle() in ROOT
        data = selector.data
        isSignal = ( (dsname.startswith("TGJets_")) or (dsname.startswith("ST_t-channel_")) or
                        (dsname.startswith("TTGJets_")) or (dsname.startswith("TTTo")) )
        if dsname + "_LHEScaleSumw" in self.config["mc_lumifactors"]:
            norm = self.config["mc_lumifactors"][dsname + "_LHEScaleSumw"]
    
            if len(norm) == 44:
                # See https://github.com/cms-nanoAOD/cmssw/issues/537
                idx = [34, 5, 24, 15]
            elif len(norm) == 9:
                # This appears to be the standard case for most data sets
                idx = [7, 1, 5, 3, 8, 0]
            elif len(norm) == 8:
                # Same as length 9, just missing the nominal weight at index 4
                idx = [6, 1, 4, 3, 7, 0]
            else:
                raise RuntimeError(
                    "Unexpected length of the norm for LHEScaleWeight: "
                    f"{len(norm)}")
            
            final_weights_ren_up = np.ones(len(data))
            final_weights_ren_down = np.ones(len(data))
            final_weights_fac_up = np.ones(len(data))
            final_weights_fac_down = np.ones(len(data))
            
            #weights = data["LHEScaleWeight"]
            if isSignal and not (bins=='inclusive'):
                #print(">>>> run for adding scale uncertainty per-bin")
                for bin in bins:
                    weights_ren_up = data["LHEScaleWeight"][:, idx[0]] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["LHEScaleWeight"][:, idx[0]])
                    weights_ren_down = data["LHEScaleWeight"][:, idx[1]] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["LHEScaleWeight"][:, idx[1]])
                    weights_fac_up = data["LHEScaleWeight"][:, idx[2]] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["LHEScaleWeight"][:, idx[2]])
                    weights_fac_down = data["LHEScaleWeight"][:, idx[3]] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["LHEScaleWeight"][:, idx[3]])
                    final_weights_ren_up = ak.where(data[bin],weights_ren_up,final_weights_ren_up)
                    final_weights_ren_down = ak.where(data[bin],weights_ren_down,final_weights_ren_down)
                    final_weights_fac_up = ak.where(data[bin],weights_fac_up,final_weights_fac_up)
                    final_weights_fac_down = ak.where(data[bin],weights_fac_down,final_weights_fac_down)
            else:
                final_weights_ren_up = data["LHEScaleWeight"][:, idx[0]] * abs(norm[idx[0]])
                final_weights_ren_down = data["LHEScaleWeight"][:, idx[1]] * abs(norm[idx[1]])
                final_weights_fac_up = data["LHEScaleWeight"][:, idx[2]] * abs(norm[idx[2]])
                final_weights_fac_down = data["LHEScaleWeight"][:, idx[3]] * abs(norm[idx[3]])
    
            selector.set_systematic(
                "MEren",
                final_weights_ren_up,
                final_weights_ren_down)
            
            selector.set_systematic(
                "MEfac",
                final_weights_fac_up,
                final_weights_fac_down)
    
    def add_ps_uncertainties_per_bin(self, dsname, selector, bins):
        """Parton shower scale uncertainties"""
        data = selector.data
        isSignal = ( (dsname.startswith("TGJets_")) or (dsname.startswith("ST_t-channel_")) or
                        (dsname.startswith("TTGJets_")) or (dsname.startswith("TTTo")) )
        psweight = data["PSWeight"]
        if len(psweight) == 0:
            return
        num_weights = ak.num(psweight)[0]
        if num_weights == 1:
            # NanoAOD containts one 1.0 per event in PSWeight if there are no
            # PS weights available, otherwise all counts > 1.
            return
        #fix this: don't know why there is only PSSum saved for some processes and not others
        try:
            norm = self.config["mc_lumifactors"][dsname + "_PSSumw"]
        except:
            return

        if num_weights == 4:
                
            final_weights_ISR_up = np.ones(len(data))
            final_weights_ISR_down = np.ones(len(data))
            final_weights_FSR_up = np.ones(len(data))
            final_weights_FSR_down = np.ones(len(data))
    
            if isSignal and not (bins=='inclusive'):
                #print(">>>> run for adding PS uncertainty per-bin")
                for bin in bins:
                    weights_FSR_up = psweight[:, 1] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["PSWeight"][:,1])
                    weights_FSR_down = psweight[:, 3] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["PSWeight"][:,3])
                    weights_ISR_up = psweight[:, 0] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["PSWeight"][:,0])
                    weights_ISR_down = psweight[:, 2] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["PSWeight"][:,2])
    
                    final_weights_FSR_up = ak.where(data[bin],weights_FSR_up,final_weights_FSR_up)
                    final_weights_FSR_down = ak.where(data[bin],weights_FSR_down,final_weights_FSR_down)
                    final_weights_ISR_up = ak.where(data[bin],weights_ISR_up,final_weights_ISR_up)
                    final_weights_ISR_down = ak.where(data[bin],weights_ISR_down,final_weights_ISR_down)
    
            else:
                final_weights_FSR_up = psweight[:, 1] *abs(norm[1])
                final_weights_FSR_down = psweight[:, 3] *abs(norm[3])
                final_weights_ISR_up = psweight[:, 0] *abs(norm[0])
                final_weights_ISR_down = psweight[:, 2] *abs(norm[2])
                
            selector.set_systematic(
                "PSisr", final_weights_ISR_up, final_weights_ISR_down)
            selector.set_systematic(
                "PSfsr", final_weights_FSR_up, final_weights_FSR_down)
        #else:
        #    raise RuntimeError(
        #        "Unexpected length of the PSWeight: "
        #        f"{num_weights}")

    def build_toplep_column(self,data):
        toplep_pt = data["toplep_pt"]
        toplep_eta = data["toplep_eta"]
        toplep_phi = data["toplep_phi"]
        toplep_m = data["toplep_m"]

        toplep = ak.zip({"pt": toplep_pt, "eta": toplep_eta , "phi": toplep_phi, "mass": toplep_m},
                      with_name="PtEtaPhiMLorentzVector",behavior=data.behavior)

        return toplep

    def build_tophad_column(self,data):
        tophad_pt  = data["tophad_pt"]
        tophad_eta = data["tophad_eta"]
        tophad_phi = data["tophad_phi"]
        tophad_m   = data["tophad_m"]

        tophad = ak.zip({"pt": tophad_pt, "eta": tophad_eta , "phi": tophad_phi, "mass": tophad_m},
                      with_name="PtEtaPhiMLorentzVector",behavior=data.behavior)

        return tophad

    def build_pseudo_toplep_column(self,data):
        toplep_pt =  data["pseudo_toplep_pt"]
        toplep_eta = data["pseudo_toplep_eta"]
        toplep_phi = data["pseudo_toplep_phi"]
        toplep_m =   data["pseudo_toplep_m"]

        toplep = ak.zip({"pt": toplep_pt, "eta": toplep_eta , "phi": toplep_phi, "mass": toplep_m},
                      with_name="PtEtaPhiMLorentzVector",behavior=data.behavior)

        return toplep

    def build_pseudo_tophad_column(self,data):
        tophad_pt  = data["pseudo_tophad_pt"]
        tophad_eta = data["pseudo_tophad_eta"]
        tophad_phi = data["pseudo_tophad_phi"]
        tophad_m   = data["pseudo_tophad_m"]

        tophad = ak.zip({"pt": tophad_pt, "eta": tophad_eta , "phi": tophad_phi, "mass": tophad_m},
                      with_name="PtEtaPhiMLorentzVector",behavior=data.behavior)

        return tophad

    def build_matched_toplep(self,is_mc,data):
        has_top = data["has_top"]
        gentop = ak.pad_none(data["GenTop"],1,axis=1)
        toplep = data["toplep"]
        tophad = data["tophad"]

        has_toplep_close = ak.any(
            toplep.metric_table(gentop) < 0.6, axis=2)

        return has_toplep_close

    def build_matched_tophad(self,is_mc,data):
        has_top = data["has_top"]
        gentop = ak.pad_none(data["GenTop"],1,axis=1)
        toplep = data["toplep"]
        tophad = data["tophad"]

        has_tophad_close = ak.any(
            tophad.metric_table(gentop) < 0.6, axis=2)

        return has_tophad_close


