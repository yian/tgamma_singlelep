import numpy as np
import awkward as ak

def remove_stnlo_overlap(dsname, data):
    if (not "ST_t-channel" in dsname and not dsname.startswith("TGJets_lepton")) :
        accept = ak.num(data["Lepton"]) > 0
        return accept

    genphoton = data["GenPhoton"]
    genpart = data["GenPart"]    

    eta_cut = abs(genphoton["eta"])<2.6
    pt_cut = genphoton["pt"]>10
    genphoton = genphoton[eta_cut & pt_cut]

    #isPrompt: not from hadron and tau decay
    promptmatch = genphoton.hasFlags(['isPrompt'])

    mother = genphoton.parent
    not_from_top = (genphoton['pdgId']==22) #always from True 
    while not ak.all(ak.is_none(mother, axis=1)):
        not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
        mother = mother.parent

    mother = genphoton.parent
    from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                      (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                      (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
    from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
             & (~not_from_top) )
    from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
             & (~not_from_top) )

    from_decayProd = (from_lepton | from_W | from_b)

    #check if the photon is isolated from all final state particles
    #(excluding neutrinos and low pt particles and the photons)
    #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
    is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                    (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                    (abs(genpart["pdgId"])!=16))

    relevant_part = genpart[is_relevant_part]

    has_part_close = ak.any(
            genphoton.metric_table(relevant_part) < 0.05, axis=2) # isolated in the cone size of 0.05, loose cut

    genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close]   

    if "ST_t-channel" in dsname : 
        accept = (ak.num(genphoton)==0)
    elif dsname.startswith("TGJets") :
        accept = (ak.num(genphoton)>0)
    return accept

def remove_ttlo_overlap(dsname, data):

    if (not "TTTo" in dsname and not dsname.startswith("TTGamma")) :
        accept = ak.num(data["Lepton"]) > 0
        return accept

    genphoton = data["GenPhoton"]
    genpart = data["GenPart"]    

    eta_cut = abs(genphoton["eta"])<5.0
    pt_cut = genphoton["pt"]>10
    genphoton = genphoton[eta_cut & pt_cut]

    #isPrompt: not from hadron and tau decay
    promptmatch = genphoton.hasFlags(['isPrompt'])

    #check if the photon is isolated from all final state particles
    #(excluding neutrinos and low pt particles and the photons)
    #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
    is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                    (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                    (abs(genpart["pdgId"])!=16))

    relevant_part = genpart[is_relevant_part]

    has_part_close = ak.any(
            genphoton.metric_table(relevant_part) < 0.1, axis=2) # the ptg in run card is 0, the R0gamma setting is disabled, select the proper value of 0.1 tigher than the 0.05 option used in the NLO removal

    genphoton = genphoton[promptmatch & ~has_part_close]   

    if "TTTo" in dsname : 
        accept = (ak.num(genphoton)==0)
    elif dsname.startswith("TTGamma"):
        accept = (ak.num(genphoton)>0)
    return accept

def remove_ttnlo_mg5_overlap(dsname, data):

    if (not "TTJets" in dsname and not dsname.startswith("TTGJets") ) :
        accept = ak.num(data["Lepton"]) > 0
        return accept

    genphoton = data["GenPhoton"]
    genpart = data["GenPart"]

    pt_cut = genphoton["pt"]>10
    eta_cut = abs(genphoton["eta"])<2.6
    genphoton = genphoton[eta_cut & pt_cut]

    #isPrompt: not from hadron and tau decay
    promptmatch = genphoton.hasFlags(['isPrompt'])

    mother = genphoton.parent
    not_from_top = (genphoton['pdgId']==22) #always from True 
    while not ak.all(ak.is_none(mother, axis=1)):
        not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
        mother = mother.parent

    mother = genphoton.parent
    from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                      (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                      (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
    from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
             & (~not_from_top) )
    from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
             & (~not_from_top) )

    from_decayProd = (from_lepton | from_W | from_b)

    #check if the photon is isolated from all final state particles
    #(excluding neutrinos and low pt particles and the photons)
    #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
    is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

    relevant_part = genpart[is_relevant_part]

    has_part_close = ak.any(
            genphoton.metric_table(relevant_part) < 0.05, axis=2)

    genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close]   

    if "TTJets" in dsname :
        accept = (ak.num(genphoton)==0)
    elif dsname.startswith("TTGJets"):
        accept = (ak.num(genphoton)>0)
    return accept

def remove_ttnlo_powheg_overlap(dsname, data):

    if (not "TTTo" in dsname and not dsname.startswith("TTGJets") ) :
        accept = ak.num(data["Lepton"]) > 0
        return accept

    genphoton = data["GenPhoton"]
    genpart = data["GenPart"]

    pt_cut = genphoton["pt"]>10
    eta_cut = abs(genphoton["eta"])<2.6
    genphoton = genphoton[eta_cut & pt_cut]

    #isPrompt: not from hadron and tau decay
    promptmatch = genphoton.hasFlags(['isPrompt'])

    mother = genphoton.parent
    not_from_top = (genphoton['pdgId']==22) #always from True 
    while not ak.all(ak.is_none(mother, axis=1)):
        not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
        mother = mother.parent

    mother = genphoton.parent
    from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                      (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                      (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
    from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
             & (~not_from_top) )
    from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
             & (~not_from_top) )

    from_decayProd = (from_lepton | from_W | from_b)

    #check if the photon is isolated from all final state particles
    #(excluding neutrinos and low pt particles and the photons)
    #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
    is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

    relevant_part = genpart[is_relevant_part]

    has_part_close = ak.any(
            genphoton.metric_table(relevant_part) < 0.05, axis=2)

    genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close]   

    if "TTTo" in dsname :
        accept = (ak.num(genphoton)==0)
    elif dsname.startswith("TTGJets"):
        accept = (ak.num(genphoton)>0)
    return accept

def remove_stW_overlap(dsname, data):

    if (not "ST_tW" in dsname) :
        accept = ak.num(data["Lepton"]) > 0
        return accept

    genphoton = data["GenPhoton"]
    genpart = data["GenPart"]    

    eta_cut = abs(genphoton["eta"])<5.0
    pt_cut = genphoton["pt"]>10
    genphoton = genphoton[eta_cut & pt_cut]

    #isPrompt: not from hadron and tau decay
    promptmatch = genphoton.hasFlags(['isPrompt'])

    #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
    is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

    relevant_part = genpart[is_relevant_part]

    has_part_close = ak.any(
            genphoton.metric_table(relevant_part) < 0.1, axis=2) # the ptg in run card is 0, so the R0gamma setting is disabled, select the proper value of 0.1

    genphoton = genphoton[promptmatch & ~has_part_close]   

    if "ST_tW_" in dsname : 
        accept = (ak.num(genphoton)==0)
    elif "ST_tWA" in dsname :
        accept = (ak.num(genphoton)>0)
    return accept

def remove_z_overlap(dsname, data):

    if (not "DYJetsToLL" in dsname and not "ZG" in dsname) :
        accept = ak.num(data["Lepton"]) > 0
        return accept

    genphoton = data["GenPhoton"]
    genpart = data["GenPart"]

    #for DY/Zgamma there is a madgraph cut of photon eta <2.6
    eta_cut = abs(genphoton["eta"])<2.6
    pt_cut = genphoton["pt"]>15
    genphoton = genphoton[eta_cut & pt_cut]

    #isPrompt: not from hadron and tau decay
    promptmatch = genphoton.hasFlags(['isPrompt'])

    #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
    is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

    relevant_part = genpart[is_relevant_part]

    has_part_close = ak.any(
            genphoton.metric_table(relevant_part) < 0.05, axis=2) 
            
    genphoton = genphoton[promptmatch & ~has_part_close]

    if "DYJetsToLL" in dsname : 
        accept = ak.num(genphoton)==0
    elif "ZG" in dsname :
        accept = ak.num(genphoton)>0 

    return accept

def remove_w_overlap(dsname, data):

    if (not dsname.startswith("WJetsToLNu") and not "WG" in dsname) :
        accept = ak.num(data["Lepton"]) > 0
        return accept

    genphoton = data["GenPhoton"]
    genpart = data["GenPart"]

    #for W+jets/Wgamma there is a madgraph cut of photon eta <2.6
    eta_cut = abs(genphoton["eta"])<2.6
    pt_cut = genphoton["pt"]>15
    genphoton = genphoton[eta_cut & pt_cut]

    #isPrompt: not from hadron and tau decay
    promptmatch = genphoton.hasFlags(['isPrompt'])

    #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
    is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

    relevant_part = genpart[is_relevant_part]

    has_part_close = ak.any(
            genphoton.metric_table(relevant_part) < 0.05, axis=2) 

    genphoton = genphoton[promptmatch & ~has_part_close]

    if dsname.startswith("WJetsToLNu"): 
        accept = ak.num(genphoton)==0
    elif "WG" in dsname :
        accept = ak.num(genphoton)>0 

    return accept

def remove_z_overlap_zmmg(dsname, data):

    if (not "DYJetsToMuMu" in dsname and not "ZGToLLG" in dsname) :
        accept = ak.num(data["Lepton"]) > 0
        return accept

    genphoton = data["GenPhoton"]
    genpart = data["GenPart"]

    #for DY/Zgamma there is a madgraph cut of photon eta <2.6
    eta_cut = abs(genphoton["eta"])<2.6
    pt_cut = genphoton["pt"]>15
    genphoton = genphoton[eta_cut & pt_cut]

    #isPrompt: not from hadron and tau decay
    promptmatch = genphoton.hasFlags(['isPrompt'])

    mother = genphoton.parent
    #print("#### TEST ",genphoton.pt)
    #print("#### TEST ",mother["pdgId"])
    from_ISR    = (ak.fill_none(abs(mother["pdgId"]), 0) < 7)
    from_lepton = (ak.fill_none(abs(mother["pdgId"]), 0)==13)

    #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
    is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

    relevant_part = genpart[is_relevant_part]

    has_part_close = ak.any(
            genphoton.metric_table(relevant_part) < 0.05, axis=2) 
            
    genphoton = genphoton[promptmatch & ~has_part_close & ~from_ISR]

    if "DYJetsToMuMu" in dsname : 
        accept = ak.num(genphoton)>0
    elif "ZG" in dsname :
        accept = ak.num(genphoton)==0 

    return accept

