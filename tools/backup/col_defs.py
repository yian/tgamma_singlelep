import awkward as ak
import numpy as np
from tools.utils import DeltaR,compute_four_momentum,combine_four_momentum_in_array

def build_genlepton_column(is_mc, data):

    genlepton = data["GenPart"]

    is_lepton = ( (abs(genlepton["pdgId"])==13) | (abs(genlepton["pdgId"])==11) )
    is_final_state = genlepton["status"]==1

    has_pt = genlepton["pt"]>30.
    has_eta = abs(genlepton["eta"])<2.5

    promptmatch = genlepton.hasFlags(['isPrompt'])
    promptmatch = ( (promptmatch) | (genlepton.hasFlags(['isPromptTauDecayProduct']))
                    | (genlepton.hasFlags(["fromHardProcess"]))
                  )

    genlepton = genlepton[is_lepton & is_final_state & has_pt & has_eta & promptmatch]

    genlepton = genlepton[ak.argsort(genlepton["pt"], ascending=False)]

    return genlepton

def build_gendressedlepton_column(is_mc, data):

    genpart = data["GenPart"]
    genlepton = data["GenPart"]

    is_lepton = ( (abs(genlepton["pdgId"])==13) | (abs(genlepton["pdgId"])==11) )
    is_final_state = genlepton["status"]==1

    promptmatch = genlepton.hasFlags(['isPrompt'])
    promptmatch = ( (promptmatch) | (genlepton.hasFlags(['isPromptTauDecayProduct']))
                    | (genlepton.hasFlags(["fromHardProcess"]))
                  )

    genlepton = genlepton[is_lepton & is_final_state & promptmatch]

    is_dressed = (genpart['status']==1) & (abs(genpart["pdgId"])==22) & (promptmatch)
    genphoton_for_dressed = genpart[is_dressed]

    has_lep_close = ak.any(
        genlepton.metric_table(genphoton_for_dressed) < 0.1, axis=1)

    genphoton_for_dressed = genphoton_for_dressed[has_lep_close]

    # calculate the four momentum of all photons for dressing lepton in each event
    total_px = ak.sum(genphoton_for_dressed.pt*np.cos(genphoton_for_dressed.phi),axis=-1)
    total_py = ak.sum(genphoton_for_dressed.pt*np.sin(genphoton_for_dressed.phi),axis=-1)
    total_pz = ak.sum(genphoton_for_dressed.pt*np.sinh(genphoton_for_dressed.eta),axis=-1)
    total_p = np.sqrt(total_px**2 + total_py**2 + total_pz**2)

    total_pt = np.sqrt(total_px**2 + total_py**2)
    total_phi = np.arctan2(total_py, total_px)
    total_eta = np.arcsinh(total_pz / total_pt)

    com_genpho_four_momenta = ak.zip({"px": total_px, "py": total_py, "pz": total_pz, "E": total_p})

    genlepton_four_momenta = compute_four_momentum(genlepton)

    broadcasted_com_genpho_for_dressed = ak.broadcast_arrays(com_genpho_four_momenta, genlepton_four_momenta)[0]

    # adding the four momentum for dressing all leptons in each event
    combined_px = broadcasted_com_genpho_for_dressed.px + genlepton_four_momenta.px
    combined_py = broadcasted_com_genpho_for_dressed.py + genlepton_four_momenta.py
    combined_pz = broadcasted_com_genpho_for_dressed.pz + genlepton_four_momenta.pz
    combined_E  = broadcasted_com_genpho_for_dressed.E  + genlepton_four_momenta.E

    combined_pt = np.sqrt(combined_px**2 + combined_py**2)
    combined_phi = np.arctan2(combined_py, combined_px)
    combined_eta = np.arcsinh(combined_pz / combined_pt)

    GenDressedLepton = ak.zip({"pt": combined_pt, "eta": np.nan_to_num(combined_eta) , "phi": combined_phi, "energy": combined_E},
               with_name="PtEtaPhiELorentzVector",behavior=data.behavior)

    has_pt = GenDressedLepton["pt"]>30.
    has_eta = abs(GenDressedLepton["eta"])<2.5

    GenDressedLepton = GenDressedLepton[has_pt & has_eta]

    #print("after dressing",GenDressedLepton.pt[ak.num(GenDressedLepton)>0],
    #        len(GenDressedLepton.pt[ak.num(GenDressedLepton)>0]))

    #nano_dressed_l = data["GenDressedLepton"][ (data["GenDressedLepton"].pt>30) & (abs(data["GenDressedLepton"].eta)<2.5)]
    #print("GenDressedLepton from nanoAOD",nano_dressed_l[ak.num(nano_dressed_l)>0].pt,
    #        len(nano_dressed_l[ak.num(nano_dressed_l)>0]))

    GenDressedLepton = GenDressedLepton[ak.argsort(GenDressedLepton["pt"], ascending=False)]

    return GenDressedLepton


def build_genphoton_column(is_mc, data):

    genphoton = data["GenPart"]

    genphoton = genphoton[genphoton["pdgId"]==22]
    genphoton = genphoton[(genphoton["status"]==1)]

    #pt filtering just to speed it up
    has_pt = (genphoton["pt"]>20)
    has_eta = (abs(genphoton["eta"])<2.5)

    promptmatch = genphoton.hasFlags(['isPrompt'])
    promptmatch = ( (promptmatch) | (genphoton.hasFlags(['isPromptTauDecayProduct'])) |
                    (genphoton.hasFlags(["fromHardProcess"])))

    # array1.metric_table(array2), output array shape is array1 when axis=2
    #                              output array shape is array2 when axis=1
    
    #genlepton = data["GenLepton"]
    #has_lep_close = ak.any(genphoton.metric_table(genlepton) < 0.4, axis=2)
    #genphoton = genphoton[has_pt & has_eta & promptmatch & ~has_lep_close]

    # remove dr lep cut to give a such full/complete prompt photon collection to the overlap procedure
    # the promptmatch is kept due to we only need prompt contribution from MC
    # if we want to study the nonprompt contribution of the MC, the promptmatch cut can be kept
    genphoton = genphoton[has_pt & has_eta & promptmatch]  

    genphoton = genphoton[ak.argsort(genphoton["pt"], ascending=False)]

    return genphoton

def build_particle_isolated_genphoton(is_mc, data):

    genpart = data["GenPart"]

    is_photon = genpart["pdgId"]==22
    is_final_state = genpart["status"]==1

    #pt filtering just to speed it up
    has_pt = (genpart["pt"]>20)
    has_eta = (abs(genpart["eta"])<2.5)

    promptmatch = genpart.hasFlags(['isPrompt'])
    promptmatch = ( (promptmatch) | (genpart.hasFlags(['isPromptTauDecayProduct'])) |
                    (genpart.hasFlags(["fromHardProcess"])))

    genlepton = data["ParticleLepton"]

    # array1.metric_table(array2), output array shape is array1 when axis=2
    #                              output array shape is array2 when axis=1
    has_lep_close = ak.any(genpart.metric_table(genlepton) < 0.4, axis=2)

    #method 1
    selected_photon_flag =  (is_photon) & (is_final_state) & has_pt & has_eta & promptmatch & ~has_lep_close
    genphotons = genpart[selected_photon_flag]

    # method 2
    #genphotons = ak.pad_none(data["GenPhoton"],1,axis=1) # from build_genphoton_column
    #selected_photon_flag = ak.any(genphotons.metric_table(genpart)<0.001,axis=1)

    #print("selected photon",genpart[selected_photon_flag][ak.num(genpart[selected_photon_flag])>0].pt,
    #                        len(genpart[selected_photon_flag][ak.num(genpart[selected_photon_flag])>0]))

    is_relevant_part = ((genpart['status']==1) &
                        (genpart["pt"]>5) &
                        (~selected_photon_flag) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) &
                        (abs(genpart["pdgId"])!=16))

    relevant_part = genpart[is_relevant_part]

    has_part_close = ak.any(
            genphotons.metric_table(relevant_part) < 0.1, axis=2)

    iso_genpho = genphotons[~has_part_close]
    iso_genpho = iso_genpho[ak.argsort(iso_genpho["pt"], ascending=False)]

    return iso_genpho

def build_parton_lepton_column(is_mc,data):

    genlepton = data["GenPart"]
    gentop = ak.pad_none(data["GenTop"],1,axis=1)

    is_lepton = ( (abs(genlepton["pdgId"])==13) | (abs(genlepton["pdgId"])==11) )
    is_lastCopy = genlepton.hasFlags(['isLastCopy'])
    has_pt = genlepton["pt"]>30
    has_eta = abs(genlepton["eta"])<2.5

    promptmatch = genlepton.hasFlags(['isPrompt'])
    promptmatch = ( (promptmatch) | (genlepton.hasFlags(['isPromptTauDecayProduct']))
                    | (genlepton.hasFlags(["fromHardProcess"]))
                    | (genlepton.hasFlags(['isHardProcess']))
                    | (genlepton.hasFlags(['isHardProcessTauDecayProduct']))
                  )

    genlepton = genlepton[is_lepton & is_lastCopy & promptmatch]# remove pt/eta cuts
    #genlepton = genlepton[is_lepton & is_lastCopy & has_pt & has_eta & promptmatch]

    genlepton = genlepton[ak.argsort(genlepton["pt"], ascending=False)]
    #print("isLastCopy parton level genlepton",genlepton[ak.num(genlepton)>0].pt,len(genlepton[ak.num(genlepton)>0]))

    return genlepton

def build_parton_photon_column(is_mc,data):

    genphoton = data["GenPart"]

    is_photon = genphoton["pdgId"]==22
    is_lastCopy = genphoton.hasFlags(['isLastCopy'])
    has_pt = (genphoton["pt"]>20)
    has_eta = (abs(genphoton["eta"])<2.5)

    promptmatch = genphoton.hasFlags(['isPrompt'])
    promptmatch = ( (promptmatch) | (genphoton.hasFlags(['isPromptTauDecayProduct']))
                    | (genphoton.hasFlags(["fromHardProcess"]))
                    | (genphoton.hasFlags(['isHardProcess']))
                    | (genphoton.hasFlags(['isHardProcessTauDecayProduct']))
                  )
    genlepton = data["PartonLepton"]
    has_lep_close = ak.any(genphoton.metric_table(genlepton) < 0.4, axis=2)

    genphoton = genphoton[ is_photon & is_lastCopy & promptmatch ]# remove pt/eta cuts
    #genphoton = genphoton[ is_photon & is_lastCopy & has_pt & has_eta & promptmatch & ~has_lep_close]

    genphoton = genphoton[ak.argsort(genphoton["pt"], ascending=False)]
    #print("isLastCopy parton level genphoton",genphoton[ak.num(genphoton)>0].pt,len(genphoton[ak.num(genphoton)>0]))
    return genphoton

def build_parton_neutrino_column(is_mc,data):

    genpart = data["GenPart"]

    is_neutrino = ( (abs(genpart["pdgId"])==12) | (abs(genpart["pdgId"])==14) | (abs(genpart["pdgId"])==16) )
    is_lastCopy = genpart.hasFlags(['isLastCopy'])

    promptmatch = genpart.hasFlags(['isPrompt'])
    promptmatch = ( (promptmatch) | (genpart.hasFlags(['isPromptTauDecayProduct']))
                    | (genpart.hasFlags(["fromHardProcess"]))
                    | (genpart.hasFlags(['isHardProcess']))
                    | (genpart.hasFlags(['isHardProcessTauDecayProduct']))
                  #  | (genpart.hasFlags(['isDirectHardProcessTauDecayProduct']))
                  #  | (genpart.hasFlags(['isPromptTauDecayProduct']))
                  )
    neutrino = genpart[ is_neutrino & is_lastCopy & promptmatch ]

    neutrino_four_momenta = compute_four_momentum(neutrino)

    sum_four_momenta = combine_four_momentum_in_array(neutrino,data)

    broadcasted_sum_four_momenta = ak.broadcast_arrays(sum_four_momenta, neutrino_four_momenta)[0]

    broadcasted_sum_neutrino_pt = np.sqrt(broadcasted_sum_four_momenta.px**2+broadcasted_sum_four_momenta.py**2)
    broadcasted_sum_neutrino_eta = np.arcsinh(broadcasted_sum_four_momenta.pz / broadcasted_sum_neutrino_pt)
    broadcasted_sum_neutrino_phi = np.arctan2(broadcasted_sum_four_momenta.py, broadcasted_sum_four_momenta.px)

    sum_neutrino = ak.zip({"pt": broadcasted_sum_neutrino_pt, "eta": np.nan_to_num(broadcasted_sum_neutrino_eta) ,
                           "phi": broadcasted_sum_neutrino_phi, "energy": broadcasted_sum_four_momenta.E},
                            with_name="PtEtaPhiELorentzVector",behavior=data.behavior)

    #has_pt = sum_neutrino.pt>20
    #has_eta = abs(sum_neutrino.eta)<5
    #sum_neutrino = sum_neutrino[has_pt & has_eta]
    #print(">>>> after sum ",sum_neutrino.pt,len(sum_neutrino))

    # without pt/eta cuts
    sum_neutrino = sum_neutrino[ak.argsort(sum_neutrino["pt"], ascending=False)]

    return sum_neutrino

def build_parton_tgq_jet_column(is_mc,data):

    genpart = data["GenPart"]
    gentop = ak.pad_none(data["GenTop"],1,axis=1)

    is_light_quark = ( (abs(genpart["pdgId"])<5) )
    is_b_quark = ( (abs(genpart["pdgId"])==5) )
    is_lastCopy = genpart.hasFlags(['isLastCopy'])
    has_parent = ~ak.is_none(genpart.parent, axis=1)
    has_pt = light_quark_collection.pt>30
    has_eta = abs(light_quark_collection.eta)<4.7

    light_quark_collection = genpart[ is_light_quark & is_lastCopy & has_parent]# remove pt/eta cuts

    vector_sum_pt = (light_quark_collection+gentop[:,0]).pt
    #print("light jet pt:",light_quark_collection.pt)
    #print("light jet eat:",light_quark_collection.eta)
    #print("vector_sum_pt:",vector_sum_pt)

    # rank the quark balancing best the top quark momentum in the transverse plane
    light_quark_collection = light_quark_collection[ak.argsort(vector_sum_pt, ascending=True)]
    #print("light jet pt:",light_quark_collection.pt)
    #print("light jet eat:",light_quark_collection.eta)

    return light_quark_collection

def build_parton_ttg_jet_column(is_mc,data):

    genpart = data["GenPart"]

    is_light_quark = ( (abs(genpart["pdgId"])<=5) )
    is_lastCopy = genpart.hasFlags(['isLastCopy'])
    has_parent = ~ak.is_none(genpart.parent, axis=1)
    has_pt = genpart.pt>30
    has_eta = abs(genpart.eta)<4.7

    light_quark_collection = genpart[ is_light_quark & is_lastCopy & has_parent]# remove pt/eta cuts
    #light_quark_collection = genpart[ is_light_quark & is_lastCopy & has_parent & has_pt & has_eta]

    light_quark_collection = light_quark_collection[ak.argsort(light_quark_collection["pt"], ascending=False)]

    return light_quark_collection

def build_parton_leptonic_top_column(is_mc,data):
    gentop = ak.pad_none(data["GenTop"],1,axis=1)
    parton_lepton = ak.pad_none(data["PartonLepton"],1,axis=1)
    #gentop = data["GenTop"]
    #parton_lepton = data["PartonLepton"]
    lepton_charge = ak.unflatten(np.sign(parton_lepton.pdgId[:,0]),counts=1)
    top_charge = np.sign(gentop.pdgId)
    #print("top charge",top_charge)
    #print("lep charge",lepton_charge)
    charge_match = ak.fill_none(top_charge != lepton_charge,False)
    #print(charge_match)
    #charge_match = ak.fill_none(np.sign(gentop[:, 0].pdgId),False) 

    gentop = gentop[charge_match]

    gentop = gentop[ak.argsort(gentop["pt"], ascending=False)]

    return gentop

def build_parton_jet_column(is_mc,dsname,data):

    genpart = data["GenPart"]

    is_light_quark = ( (abs(genpart["pdgId"])<5) )# without change for tgq if it's <=5
    if ( (dsname.startswith("TTGJets_")) or (dsname.startswith("TTTo")) ):
        is_light_quark = ( (abs(genpart["pdgId"])<=5) )

    is_lastCopy = genpart.hasFlags(['isLastCopy'])
    has_parent = ~ak.is_none(genpart.parent, axis=1)

    light_quark_collection = genpart[ is_light_quark & is_lastCopy & has_parent]

    light_quark_collection = light_quark_collection[ak.argsort(light_quark_collection["pt"], ascending=False)]

    return light_quark_collection

def build_particle_lepton_column(is_mc,data):

    genlepton = data["GenDressedLepton"]
    is_lepton = ( (abs(genlepton["pdgId"])==13) | (abs(genlepton["pdgId"])==11) )

    has_pt = genlepton["pt"]>30.
    has_eta = abs(genlepton["eta"])<2.5

    genlepton = genlepton[is_lepton & has_pt & has_eta]

    genlepton = genlepton[ak.argsort(genlepton["pt"], ascending=False)]

    return genlepton

def build_particle_photon_column(is_mc,data):

    genphoton = data["GenIsolatedPhoton"]

    has_pt = (genphoton["pt"]>20)
    has_eta = (abs(genphoton["eta"])<2.5)

    genlepton = data["ParticleLepton"]
    has_lep_close = ak.any(genphoton.metric_table(genlepton) < 0.4, axis=2)

    genphoton = genphoton[has_pt & has_eta & ~has_lep_close]
    genphoton = genphoton[ak.argsort(genphoton["pt"], ascending=False)]

    return genphoton

def build_particle_jet_column(is_mc, data):

    genjet = data["GenJet"]
    genlepton = data["ParticleLepton"]
    genphoton = data["ParticlePhoton"]

    has_pt = (genjet["pt"]>30)
    has_eta = (abs(genjet["eta"])<4.7)

    has_lep_close = ak.any(genjet.metric_table(genlepton) < 0.4, axis=2)
    has_pho_close = ak.any(genjet.metric_table(genphoton) < 0.1, axis=2)

    genjet = genjet[(~has_lep_close) & (~has_pho_close) & has_pt & has_eta]

    return genjet

def build_particle_neutrino_column(is_mc,data):

    MET = data["GenMET"]
    has_pt = np.full(len(data), True)
    #has_pt = (MET["pt"]>0)

    masked_MET = ak.mask(MET, has_pt)
    filter_none = ~ak.is_none(masked_MET)

    MET_2d_array = ak.where(filter_none,ak.unflatten(masked_MET,counts=1),[[]])

    return MET_2d_array
