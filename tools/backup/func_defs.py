import numpy as np
import awkward as ak

def add_pdf_uncertainties_per_bin(config, dsname, selector, bins):
    """Add PDF uncertainties, using the methods described here:
    https://arxiv.org/pdf/1510.03865.pdf#section.6"""
    isSignal = ( (dsname.startswith("TGJets_")) or (dsname.startswith("ST_t-channel_")) or
                         (dsname.startswith("TTGJets_")) or (dsname.startswith("TTTo")) )
    data = selector.data
    if ("LHEPdfWeight" not in data.fields
            or "pdf_types" not in config):
        return
    split_pdf_uncs = False
    if "split_pdf_uncs" in config:
        split_pdf_uncs = config["split_pdf_uncs"]
    pdfs = data["LHEPdfWeight"]
    norm = np.abs(config["mc_lumifactors"][dsname + "_LHEPdfSumw"])

    if ak.sum(pdfs) == 0.:
        return
    pdf_type = None
    for LHA_ID, _type in config["pdf_types"].items():
        if LHA_ID in pdfs.__doc__:
            pdf_type = _type.lower()
    # Check if sample has alpha_s variations - currently assuming number of
    # regular variations is a multiple of 10
    if len(data) == 0:
        has_as_unc = False
    else:
        has_as_unc = (len(pdfs[0]) % 10) > 1
        # Workaround for "order of scale and pdf weights not consistent"
        # See https://twiki.cern.ch/twiki/bin/view/CMS/MCKnownIssues
        if ak.mean(pdfs[0]) < 0.6:  # approximate, see if factor 2 needed
            pdfs = ak.without_parameters(pdfs)
            pdfs = ak.concatenate([pdfs[:, 0:1], pdfs[:, 1:] * 2], axis=1)
    n_offset = -2 if has_as_unc else None

    if split_pdf_uncs:
        # Just output variations - user
        # will need to combine these for limit setting
        num_variation = len(pdfs[0]) + (n_offset or 0)
        if pdf_type == "true_hessian" or pdf_type == "hessian":
            # First element is central value - adjust all other
            # elements relative to this
            final_pdf_unc = np.empty(num_variation,dtype=object)
            for i in range(1, num_variation):
                final_pdf_unc[i-1] = np.ones(len(data))
                if isSignal and not (bins=='inclusive'):
                    #print(">>>> run for add pdf uncertainty per bin")
                    for bin in bins:
                        pdfs_bin = data[data[bin]]["LHEPdfWeight"]
                        pdf_unc = pdfs[:, i]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, i])  - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])
                        #awkward won't allow doing it all at once
                        current_unc = final_pdf_unc[i-1]
                        new_unc = ak.where(data[bin],pdf_unc,current_unc)
                        final_pdf_unc[i-1]=new_unc
                else:
                    final_pdf_unc[i-1]=(pdfs[:, i]*norm[i] - pdfs[:, 0]*norm[0])

                #symmetrize PDF uncertainties (not clear what recommendation is)
                selector.set_systematic(
                    "PDF{}".format(i), 1+final_pdf_unc[i-1],1-final_pdf_unc[i-1])

            if has_as_unc:
                final_as_weight_up = np.ones(len(data))
                final_as_weight_down = np.ones(len(data))

                if isSignal and not (bins=='inclusive'):
                    for bin in bins:
                        pdfs_bin = data[data[bin]]["LHEPdfWeight"]
                        pdf_as_up = (pdfs[:, -1]) * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, -1]) - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])
                        pdf_as_down = (pdfs[:, -2]) * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, -2]) - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])

                        current_as_up = final_as_weight_up
                        new_up = ak.where(data[bin],pdf_as_up,current_as_up)
                        final_as_weight_up=new_up

                        current_as_down = final_as_weight_down
                        new_down = ak.where(data[bin],pdf_as_down,current_as_down)
                        final_as_weight_down=new_down

                else:
                    weight_up = (pdfs[:, -1])
                    weight_down = (pdfs[:, -2])

                    norm_factor_up =  ak.sum(data["genWeight"]) / ak.sum(data["genWeight"]*pdfs[:, -1])
                    norm_factor_down =  ak.sum(data["genWeight"]) / ak.sum(data["genWeight"]*pdfs[:, -2])
                    final_as_weight_up = weight_up*norm_factor_up - pdfs[:, 0]
                    final_as_weight_down = weight_down*norm_factor_down - pdfs[:, 0]

                selector.set_systematic(
                    "PDFalphas",
                    final_as_weight_up+1,
                    final_as_weight_down+1)

        elif pdf_type.startswith("mc"):
            selector.set_systematic(
                "PDF",
                *[pdfs[:, i] for i in range(num_variation)],
                scheme="numeric")
            if has_as_unc:
                selector.set_systematic(
                    "PDFalphas", pdfs[:, -1], pdfs[:, -2])
        elif pdf_type is None:
            raise pepper.config.ConfigError(
                "PDF LHA Id not included in config. PDF docstring is: "
                + pdfs.__doc__)
        else:
            raise pepper.config.ConfigError(
                f"PDF type {pdf_type} not recognised. Valid options "
                "are 'True_Hessian', 'Hessian', 'MC' and 'MC_Gaussian'")
    else:
        if pdf_type == "true_hessian":
            # Treatment of true hessian uncertainties, for e.g. CTEQ
            # or HERA sets
            eigen_vals = ak.to_numpy(pdfs[:, 1:n_offset])
            eigen_vals = eigen_vals.reshape(
                (eigen_vals.shape[0], eigen_vals.shape[1] // 2, 2))
            central, eigenvals = ak.broadcast_arrays(
                pdfs[:, 0, None, None], eigen_vals)
            var_up = ak.max((eigen_vals - central), axis=2)
            var_up = ak.where(var_up > 0, var_up, 0)
            var_up = np.sqrt(ak.sum(var_up ** 2, axis=1))
            var_down = ak.max((central - eigen_vals), axis=2)
            var_down = ak.where(var_down > 0, var_down, 0)
            var_down = np.sqrt(ak.sum(var_down ** 2, axis=1))
            unc = None

        if pdf_type == "hessian":
            # Treatment of pseudo hessian uncertainties, for e.g. NNPDF
            # or pdf4LHC sets
            eigen_vals = ak.to_numpy(pdfs[:, 1:n_offset])
            variations = eigen_vals*norm[1:n_offset] - pdfs[:, 0, None]*norm[0]
            unc = np.sqrt(ak.sum(variations ** 2, axis=1))

        elif pdf_type == "mc":
            # ak.sort produces an error here. Work-around:
            variations = np.sort(ak.to_numpy(pdfs[:, 1:n_offset]))
            nvar = ak.num(variations)[0]
            unc = (variations[:, int(round(0.841344746*nvar))]
                   - variations[:, int(round(0.158655254*nvar))]) / 2
        elif pdf_type == "mc_gaussian":
            mean = ak.mean(pdfs[:, 1:n_offset], axis=1)
            unc = np.sqrt((ak.sum(pdfs[:, 1:n_offset] - mean) ** 2)
                          / (ak.num(pdfs)[0] - (3 if n_offset else 1)))
        elif pdf_type is None:
            raise pepper.config.ConfigError(
                "PDF LHA Id not included in config. PDF docstring is: "
                + pdfs.__doc__)
        else:
            raise pepper.config.ConfigError(
                f"PDF type {pdf_type} not recognised. Valid options "
                "are 'True_Hessian', 'Hessian', 'MC' and 'MC_Gaussian'")

        # Add PDF alpha_s uncertainties
        if has_as_unc:
            if ("combine_alpha_s" in config and
                    config["combine_alpha_s"]):
                alpha_s_unc = (pdfs[:, -1]*norm[-1] - pdfs[:, -2]*norm[-2]) / 2
                if unc is not None:
                    unc = np.sqrt(unc ** 2 + alpha_s_unc ** 2)
                    final_as_weight_up = unc
                    final_as_weight_down = unc

                else:
                    var_up = np.sqrt(var_up ** 2 + alpha_s_unc ** 2)
                    var_down = np.sqrt(var_down ** 2 + alpha_s_unc ** 2)
                    final_as_weight_up = var_up
                    final_as_weight_down = var_down

            else:
                final_as_weight_up = np.ones(len(data))
                final_as_weight_down = np.ones(len(data))

                if isSignal and not (bins=='inclusive'):
                    for bin in bins:
                        pdfs_bin = data[data[bin]]["LHEPdfWeight"]
                        pdf_as_up = (pdfs[:, -1]) * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, -1]) - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])
                        pdf_as_down = (pdfs[:, -2]) * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, -2]) - pdfs[:, 0]*ak.sum(data[data[bin]]["genWeight"])/ak.sum(data[data[bin]]["genWeight"]*pdfs_bin[:, 0])

                        current_as_up = final_as_weight_up
                        new_up = ak.where(data[bin],pdf_as_up,current_as_up)
                        final_as_weight_up=new_up

                        current_as_down = final_as_weight_down
                        new_down = ak.where(data[bin],pdf_as_down,current_as_down)
                        final_as_weight_down=new_down

                else:
                    final_as_weight_up = (pdfs[:, -1]*norm[-1] - pdfs[:, 0]*norm[0])
                    final_as_weight_down = (pdfs[:, -2]*norm[-2] - pdfs[:, 0]*norm[0])

                selector.set_systematic(
                    "PDFalphas",
                    final_as_weight_up+1,
                    final_as_weight_down+1)

        if unc is not None:
            selector.set_systematic("PDF", 1 + unc, 1 - unc)
        else:
            selector.set_systematic("PDF", 1 + var_up, 1 - var_down)

def add_me_uncertainties_per_bin(config, dsname, selector, bins):
    """Matrix-element renormalization and factorization scale"""
    # Get describtion of individual columns of this branch with
    # Events->GetBranch("LHEScaleWeight")->GetTitle() in ROOT
    data = selector.data
    isSignal = ( (dsname.startswith("TGJets_")) or (dsname.startswith("ST_t-channel_")) or
                    (dsname.startswith("TTGJets_")) or (dsname.startswith("TTTo")) )
    if dsname + "_LHEScaleSumw" in config["mc_lumifactors"]:
        norm = config["mc_lumifactors"][dsname + "_LHEScaleSumw"]

        if len(norm) == 44:
            # See https://github.com/cms-nanoAOD/cmssw/issues/537
            idx = [34, 5, 24, 15]
        elif len(norm) == 9:
            # This appears to be the standard case for most data sets
            idx = [7, 1, 5, 3, 8, 0]
        elif len(norm) == 8:
            # Same as length 9, just missing the nominal weight at index 4
            idx = [6, 1, 4, 3, 7, 0]
        else:
            raise RuntimeError(
                "Unexpected length of the norm for LHEScaleWeight: "
                f"{len(norm)}")
        
        final_weights_ren_up = np.ones(len(data))
        final_weights_ren_down = np.ones(len(data))
        final_weights_fac_up = np.ones(len(data))
        final_weights_fac_down = np.ones(len(data))
        
        #weights = data["LHEScaleWeight"]
        if isSignal and not (bins=='inclusive'):
            #print(">>>> run for adding scale uncertainty per-bin")
            for bin in bins:
                weights_ren_up = data["LHEScaleWeight"][:, idx[0]] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["LHEScaleWeight"][:, idx[0]])
                weights_ren_down = data["LHEScaleWeight"][:, idx[1]] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["LHEScaleWeight"][:, idx[1]])
                weights_fac_up = data["LHEScaleWeight"][:, idx[2]] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["LHEScaleWeight"][:, idx[2]])
                weights_fac_down = data["LHEScaleWeight"][:, idx[3]] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["LHEScaleWeight"][:, idx[3]])
                final_weights_ren_up = ak.where(data[bin],weights_ren_up,final_weights_ren_up)
                final_weights_ren_down = ak.where(data[bin],weights_ren_down,final_weights_ren_down)
                final_weights_fac_up = ak.where(data[bin],weights_fac_up,final_weights_fac_up)
                final_weights_fac_down = ak.where(data[bin],weights_fac_down,final_weights_fac_down)
        else:
            final_weights_ren_up = data["LHEScaleWeight"][:, idx[0]] * abs(norm[idx[0]])
            final_weights_ren_down = data["LHEScaleWeight"][:, idx[1]] * abs(norm[idx[1]])
            final_weights_fac_up = data["LHEScaleWeight"][:, idx[2]] * abs(norm[idx[2]])
            final_weights_fac_down = data["LHEScaleWeight"][:, idx[3]] * abs(norm[idx[3]])

        selector.set_systematic(
            "MEren",
            final_weights_ren_up,
            final_weights_ren_down)
        
        selector.set_systematic(
            "MEfac",
            final_weights_fac_up,
            final_weights_fac_down)

def add_ps_uncertainties_per_bin(config, dsname, selector, bins):
    """Parton shower scale uncertainties"""
    data = selector.data
    isSignal = ( (dsname.startswith("TGJets_")) or (dsname.startswith("ST_t-channel_")) or
                    (dsname.startswith("TTGJets_")) or (dsname.startswith("TTTo")) )
    psweight = data["PSWeight"]
    if len(psweight) == 0:
        return
    num_weights = ak.num(psweight)[0]
    if num_weights == 1:
        # NanoAOD containts one 1.0 per event in PSWeight if there are no
        # PS weights available, otherwise all counts > 1.
        return
    #fix this: don't know why there is only PSSum saved for some processes and not others
    try:
        norm = config["mc_lumifactors"][dsname + "_PSSumw"]
    except:
        return
    if num_weights == 4:
            
        final_weights_ISR_up = np.ones(len(data))
        final_weights_ISR_down = np.ones(len(data))
        final_weights_FSR_up = np.ones(len(data))
        final_weights_FSR_down = np.ones(len(data))

        if isSignal and not (bins=='inclusive'):
            #print(">>>> run for adding PS uncertainty per-bin")
            for bin in bins:
                weights_FSR_up = psweight[:, 1] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["PSWeight"][:,1])
                weights_FSR_down = psweight[:, 3] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["PSWeight"][:,3])
                weights_ISR_up = psweight[:, 0] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["PSWeight"][:,0])
                weights_ISR_down = psweight[:, 2] * ak.sum(data[data[bin]]["genWeight"]) / ak.sum(data[data[bin]]["genWeight"]*data[data[bin]]["PSWeight"][:,2])

                final_weights_FSR_up = ak.where(data[bin],weights_FSR_up,final_weights_FSR_up)
                final_weights_FSR_down = ak.where(data[bin],weights_FSR_down,final_weights_FSR_down)
                final_weights_ISR_up = ak.where(data[bin],weights_ISR_up,final_weights_ISR_up)
                final_weights_ISR_down = ak.where(data[bin],weights_ISR_down,final_weights_ISR_down)

        else:
            final_weights_FSR_up = psweight[:, 1] *abs(norm[1])
            final_weights_FSR_down = psweight[:, 3] *abs(norm[3])
            final_weights_ISR_up = psweight[:, 0] *abs(norm[0])
            final_weights_ISR_down = psweight[:, 2] *abs(norm[2])
            
        selector.set_systematic(
            "PSisr", final_weights_ISR_up, final_weights_ISR_down)
        selector.set_systematic(
            "PSfsr", final_weights_FSR_up, final_weights_FSR_down)
    #else:
    #    raise RuntimeError(
    #        "Unexpected length of the PSWeight: "
    #        f"{num_weights}")
