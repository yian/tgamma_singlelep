import numpy as np
import awkward as ak
import hist

def pxpypz_from_ptetaphi(pt, eta, phi):
    pt = abs(pt)
    px = pt*np.cos(phi)
    py = pt*np.sin(phi)
    pz = pt*np.sinh(eta)
    return px, py, pz                     

def DeltaR(part1,part2):

    deltaphi = np.abs(part1.phi-part2.phi)
    deltaphi = ak.where(deltaphi<np.pi,deltaphi,2*np.pi-deltaphi)

    deltaeta = part1.eta-part2.eta

    deltar = np.hypot(deltaeta,deltaphi)

    return deltar

def DeltaR1(part1,part2):

    valid_mask = ~ak.is_none(part1) & ~ak.is_none(part2)

    deltaphi = ak.where(valid_mask,np.abs(part1.phi-part2.phi),-1)
    deltaphi = ak.where(deltaphi<np.pi,deltaphi,2*np.pi-deltaphi)

    deltaeta = ak.where(valid_mask,part1.eta-part2.eta,-1)

    deltar = ak.where(valid_mask,np.hypot(deltaeta,deltaphi),-1)

    return deltar

def dphi(part1,part2):

    deltaphi = part1.phi-part2.phi
    deltaphi = ak.where(np.abs(deltaphi)<np.pi,deltaphi,2*np.pi-np.abs(deltaphi))

    return abs(deltaphi)

def compute_four_momentum(particles):
    pt = particles.pt
    eta = particles.eta
    phi = particles.phi
    mass = particles.mass

    px = pt * np.cos(phi)
    py = pt * np.sin(phi)
    pz = pt * np.sinh(eta)
    E = np.sqrt(px**2 + py**2 + pz**2 + mass**2)

    return ak.zip({"px": px, "py": py, "pz": pz, "E": E})

def combine_four_momentum_in_array(genphoton_for_dressed,data):

    total_px = ak.unflatten(ak.sum(genphoton_for_dressed.pt*np.cos(genphoton_for_dressed.phi),axis=-1),1)
    total_py = ak.unflatten(ak.sum(genphoton_for_dressed.pt*np.sin(genphoton_for_dressed.phi),axis=-1),1)
    total_pz = ak.unflatten(ak.sum(genphoton_for_dressed.pt*np.sinh(genphoton_for_dressed.eta),axis=-1),1)
    total_p = np.sqrt(total_px**2 + total_py**2 + total_pz**2)

    total_pt = np.sqrt(total_px**2 + total_py**2)
    total_phi = np.arctan2(total_py, total_px)
    total_eta = np.arcsinh(total_pz / total_pt)

    com_genpho_four_momenta = ak.zip({"px": total_px, "py": total_py, "pz": total_pz, "E": total_p},
                                  with_name="PtEtaPhiELorentzVector",behavior=data.behavior)

    return com_genpho_four_momenta

def norm_hist(h,mc_lumi,nonprompt=False):

    new_axes = []
    old_axis="dataset"
    ax_index = None

    all_process  = list(h.axes[old_axis])

    for i, ax in enumerate(h.axes):
        if ax.name == old_axis:
            new_axes.append(hist.axis.StrCategory(categories=all_process,
                                                  name=old_axis))
            ax_index = i
        else:
            new_axes.append(ax)

    if ax_index is None:
        raise ValueError(f"Axes {old_axis} not found in histogram!")

    h_new = hist.Hist(*new_axes, name=h.name, storage=h._storage_type())
    h_new_view = h_new.view(flow=True)
    print(">>>>>",len(new_axes))
    for i, categories_for_group in enumerate(all_process):
        h_group = h.integrate(old_axis, [categories_for_group])
        index_tuple = [i if j == ax_index else None
                       for j in range(len(new_axes))]
        if (categories_for_group != "DoubleMuon" and categories_for_group != "EGamma" and
                categories_for_group != "SingleMuon" and categories_for_group != "SingleElectron"):
           norm = mc_lumi[categories_for_group]*(-1) if nonprompt else mc_lumi[categories_for_group]
           h_new_view[tuple(index_tuple)] = h_group.view(flow=True)*norm
        else:
           h_new_view[tuple(index_tuple)] = h_group.view(flow=True)
    return h_new
