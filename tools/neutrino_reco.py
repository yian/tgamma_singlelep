from tools.utils import pxpypz_from_ptetaphi
import math
import numpy as np
import awkward as ak

def neutrino_reco(data):

    met = data["MET"]  
    lepton = data["Lepton"][:,0] 
    pxl, pyl, pzl = pxpypz_from_ptetaphi(lepton.pt, lepton.eta, lepton.phi)
    pxnu, pynu, pznu = pxpypz_from_ptetaphi(met.pt, lepton.eta, met.phi)        
 
    Enu = pxnu**2 + pynu**2

    mWT = np.sqrt((lepton.pt + met.pt)**2 - (pxl + pxnu)**2 -
              (pyl + pynu)**2)        

    mW = ak.Array(np.full(len(mWT), 80.4, dtype=float))
    mask_mWT_GT_mW = mWT > mW

    dummy_mask = ak.full_like(mask_mWT_GT_mW, True)
    mask_mWT_GT_mW = ak.singletons(ak.mask(mask_mWT_GT_mW, dummy_mask))
    pxnu = ak.singletons(ak.mask(pxnu, dummy_mask))
    pynu = ak.singletons(ak.mask(pynu, dummy_mask))
    pznu = ak.singletons(ak.mask(pznu, dummy_mask))
    ptW = ak.singletons(ak.mask(lepton.pt, dummy_mask))
    pxl = ak.singletons(ak.mask(pxl, dummy_mask))
    pyl = ak.singletons(ak.mask(pyl, dummy_mask))
    pzl = ak.singletons(ak.mask(pzl, dummy_mask))   

    k = met.pt * lepton.pt - pxnu * pxl - pynu * pyl

    k = ak.fill_none(ak.pad_none(k[k > 0.0001], 1), 0.0001)
    scf = 0.5 * (mW * mW) / k
    pxnu = ak.concatenate([pxnu[mask_mWT_GT_mW] * scf[mask_mWT_GT_mW],
                       pxnu[~mask_mWT_GT_mW]], axis=1)
    pynu = ak.concatenate([pynu[mask_mWT_GT_mW] * scf[mask_mWT_GT_mW],
                       pynu[~mask_mWT_GT_mW]], axis=1)
    Etnu = np.sqrt(pxnu**2 + pynu**2)

    Lambda = (mW**2)/2. + pxl * pxnu + pyl * pynu
    discr = ((Lambda * pzl)**2)/(ptW**4) - (((lepton.energy * Etnu)**2) -
                                        (Lambda**2))/(ptW**2) 

    mask_posDiscr = discr > 0

    sol = Lambda * pzl/(lepton.pt * lepton.pt)
    pxnu_neg = pxnu[~mask_posDiscr]
    pynu_neg = pynu[~mask_posDiscr]
    pznu_neg = sol[~mask_posDiscr]
    Enu_neg = np.sqrt(pxnu_neg**2 + pynu_neg**2 + pznu_neg**2)

    sol1 = (
        Lambda[mask_posDiscr] * pzl[mask_posDiscr]/(ptW[mask_posDiscr]**2) +
        np.sqrt(discr[mask_posDiscr])
    )
    sol2 = (
        Lambda[mask_posDiscr] * pzl[mask_posDiscr]/(ptW[mask_posDiscr]**2) -
        np.sqrt(discr[mask_posDiscr])
    )

    pxnu_pos = ak.concatenate([pxnu[mask_posDiscr],
                          pxnu[mask_posDiscr]], axis=1)
    pynu_pos = ak.concatenate([pynu[mask_posDiscr],
                           pynu[mask_posDiscr]], axis=1)
    pznu_pos = ak.concatenate([sol1, sol2], axis=1)
    Enu_pos = np.sqrt(pxnu_pos**2 + pynu_pos**2 + pznu_pos**2)

    pxnu = ak.concatenate([pxnu_pos, pxnu_neg], axis=1)
    pynu = ak.concatenate([pynu_pos, pynu_neg], axis=1)
    pznu = ak.concatenate([pznu_pos, pznu_neg], axis=1)
    Enu = ak.concatenate([Enu_pos, Enu_neg], axis=1)

    mnu2 = np.minimum(10e20, np.maximum((Enu*Enu - (pxnu*pxnu + pynu*pynu + pznu*pznu)), 0))
    mnu = np.sqrt(mnu2)
    ptnu = np.hypot(pxnu, pynu)
    phinu = np.arctan2(pynu, pxnu)
    etanu = np.arcsinh(pznu/ptnu)
    neutrinos = ak.zip({"pt": ptnu, "eta": etanu, "phi": phinu, "mass": mnu},
                   with_name="PtEtaPhiMLorentzVector")

    neutrinos = neutrinos[
            ak.values_astype(ak.argsort(neutrinos["pt"], ascending=False), int)]

    return neutrinos
