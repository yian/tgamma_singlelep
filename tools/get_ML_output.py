from xgboost import XGBClassifier
import xgboost as xgb
import numpy as np
import awkward as ak
import json
def get_score(model_dir,year,data):

    model=xgb.Booster()
    #print("BDT model location ",model_dir)
    #model.load_model(model_dir)
    year = year[4:]
    #print(model_dir+"BDT_model_lep"+year+".json")
    model.load_model(model_dir+"BDT_model_lep"+year+".json")

    features = model.feature_names
    #print(len(features),features)
    un_names = ['nJet_pt','nPhoton_pt']
    vars_ml = {}
    for name in un_names:
        features.remove(name)
        obj = name.split('_')[0][1:]
        vars_ml[name] = ak.num(data[str(obj)])

    jet_variables = ['pt','eta', 'phi', 'mass']
    lepton_pho_variables = ['pt','eta','phi']
    reco_W_vars = ['pt','eta','energy']
    met_vars = ['pt','phi']

    for var in jet_variables:
        vars_ml["bJet_"+var] = data["bJet"][var]
        vars_ml["noBjet_"+var] = data["noBjet"][var]
        features.remove("bJet_"+var)
        features.remove("noBjet_"+var)

    for var in lepton_pho_variables:
        vars_ml["Lepton_"+var] = data["Lepton"][var]
        vars_ml["Photon_"+var] = data["Photon"][var]
        vars_ml["Neutrino1_"+var] = data["Neutrino1"][var]
        features.remove("Lepton_"+var)
        features.remove("Photon_"+var)
        features.remove("Neutrino1_"+var)
        if 'eta' not in var:
            vars_ml["MET_"+var] = data["MET"][var]
            features.remove("MET_"+var)

    for var in reco_W_vars:
        vars_ml["reco_W_"+var] = data["reco_W"][var]
        features.remove("reco_W_"+var)

    for var in features:
        vars_ml[var] = data[var]

    for var in vars_ml.keys():
        #print(var,vars_ml[var],len(vars_ml[var]))
        if len(vars_ml[var])<1:
           vars_ml[var] = ak.Array([])
        if len(vars_ml[var])>0 and vars_ml[var].ndim>1:
           vars_ml[var] = ak.where(ak.num(vars_ml[var])<1,[[-10]],vars_ml[var]) 
           vars_ml[var] = vars_ml[var][:,0]
        elif len(vars_ml[var])>0:
           vars_ml[var] = vars_ml[var][:]

    #print(vars_ml.keys(),len(vars_ml.keys())) 
    ml_vars = ak.zip(vars_ml)
    ml_vars = ak.to_pandas(ml_vars)
           
    features = model.feature_names 
    dmatrix_ml_vars = xgb.DMatrix(ml_vars[features])
    score = model.predict(dmatrix_ml_vars) 

    return score

def get_new_score(data):

    model_dir = "/afs/desy.de/user/y/yian/cms/topgamma/data/"
    model=xgb.Booster()
    model.load_model(model_dir+"new_BDT_model.json")

    features = model.feature_names
    #print(len(features),features)

    vars_ml = {}

    jet_variables = ['pt','eta']
    lepton_pho_variables = ['pt','eta']
    reco_W_vars = ['energy']
    met_vars = ['pt']

    for var in jet_variables:
        vars_ml["bJet_"+var] = data["bJet"][var]
        vars_ml["noBjet_"+var] = data["noBjet"][var]
        features.remove("bJet_"+var)
        features.remove("noBjet_"+var)

    for var in lepton_pho_variables:
        vars_ml["Lepton_"+var] = data["Lepton"][var]
        vars_ml["Photon_"+var] = data["Photon"][var]
        features.remove("Lepton_"+var)
        features.remove("Photon_"+var)
        if 'eta' not in var:
            vars_ml["MET_"+var] = data["MET"][var]
            features.remove("MET_"+var)

    for var in reco_W_vars:
        vars_ml["reco_W_"+var] = data["reco_W"][var]
        features.remove("reco_W_"+var)

    for var in features:
        vars_ml[var] = data[var]

    for var in vars_ml.keys():
        #print(var,vars_ml[var],len(vars_ml[var]))
        if len(vars_ml[var])<1:
           vars_ml[var] = ak.Array([])
        if len(vars_ml[var])>0 and vars_ml[var].ndim>1:
           vars_ml[var] = ak.where(ak.num(vars_ml[var])<1,[[-10]],vars_ml[var]) 
           vars_ml[var] = vars_ml[var][:,0]
        elif len(vars_ml[var])>0:
           vars_ml[var] = vars_ml[var][:]

    #print(vars_ml.keys(),len(vars_ml.keys())) 
    ml_vars = ak.zip(vars_ml)
    ml_vars = ak.to_pandas(ml_vars)
           
    features = model.feature_names 
    dmatrix_ml_vars = xgb.DMatrix(ml_vars[features])
    score = model.predict(dmatrix_ml_vars) 

    return score
