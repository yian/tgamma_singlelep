import numpy as np
import uproot
import hjson
import coffea
from coffea import lookup_tools
from functools import partial

import pepper
from pepper.scale_factors import ScaleFactors

class ConfigTopGamma(pepper.ConfigBasicPhysics):
    def __init__(self, path_or_file, textparser=hjson.load, cwd="."):
        """Initialize the configuration.

        Arguments:
        path_or_file -- Either a path to the file containing the configuration
                        or a file-like object of it
        textparser -- Callable to be used to parse the text contained in
                      path_or_file
        cwd -- A path to use as the working directory for relative paths in the
               config. The actual working directory of the process might change
        """
        super().__init__(path_or_file, textparser, cwd)
        self.behaviors.update(
	    {
            "electron_sf": partial(
                self._get_scalefactors, dimlabels=["eta", "pt"]),
            "muon_sf": partial(
                self._get_scalefactors, dimlabels=["abseta", "pt"]),
            "photon_sf": partial(
                self._get_scalefactors, dimlabels=["eta", "pt"]),
            "psv_sf": partial(
                self._get_scalefactors, dimlabels=["etabin"]),
            "csev_sf": partial(
                self._get_scalefactors, dimlabels=["etabin"]),
            "jet_puid_sf": partial(
                self._get_scalefactors, dimlabels=["pt", "eta"]),
            "jet_puid_sf_unc": partial(
                self._get_scalefactors, dimlabels=["pt", "eta"]),
            "nonprompt_photonsf_all": 
                self._get_nonprompt_photonsf_all,
            "nonprompt_photon_sf": 
                self._get_nonprompt_photon_sf,
            "nonprompt_photon_sf_unc_SB": 
                self._get_nonprompt_photon_sf,
            "nonprompt_photon_sf_unc_coarse_bin": 
                self._get_nonprompt_photon_sf,
            "nonprompt_photon_sf_ttbar": 
                self._get_nonprompt_photon_sf,
            "nonprompt_photon_sf_ttbar_double_nonprompt_closure": 
                self._get_nonprompt_photon_sf_ttbar_double_nonprompt_closure,
            "nonprompt_MCcorrection": 
                self._get_nonprompt_MCcorrection,
            "nonprompt_MCcorrection_double_nonprompt_closure": 
                self._get_nonprompt_MCcorrection_double_nonprompt_closure,
            "nonprompt_leptonsf_all": 
                self._get_nonprompt_leptonsf_all,
            "nonprompt_leptonsf_all_unc_SB": 
                self._get_nonprompt_leptonsf_all,
            "nonprompt_leptonsf_all_unc_coarse_bin": 
                self._get_nonprompt_leptonsf_all,
            "nonprompt_leptonsf_all_QCD_closure": 
                self._get_nonprompt_leptonsf_all_closure,
            "nonprompt_leptonsf_all_ttbar_closure":
                self._get_nonprompt_leptonsf_all_closure,
            "nonprompt_lepton_sf": 
                self._get_nonprompt_lepton_sf,
            "drellyan_sf": self._get_drellyan_sf,
	    }
	)
    def _get_drellyan_sf(self, value):
        if isinstance(value, list):
            path, histname = value
            with uproot.open(self._get_path(path)) as f:
                hist = f[histname]
            dy_sf = ScaleFactors.from_hist(hist)
        else:
            data = self._get_maybe_external(value)
            dy_sf = ScaleFactors(
                bins=data["bins"],
                factors=np.array(data["factors"]),
                factors_up=np.array(data["factors_up"]),
                factors_down=np.array(data["factors_down"]))
        return dy_sf

    def _get_scalefactors(self, value, dimlabels):
        sfs = []
        for sfpath in value:
            if not isinstance(sfpath, list) or len(sfpath) < 2:
                raise pepper.config.ConfigError(
                    "scale factors needs to be list of 2-element-lists in "
                    "form of [rootfile, histname]")
            with uproot.open(self._get_path(sfpath[0])) as f:
                hist = f[sfpath[1]]
            #try:
            #  print( (hist.to_hist()).axes["yaxis"].edges[-1])
            #except:
            #  print("pass")
            sf = ScaleFactors.from_hist(hist, dimlabels)
            sfs.append(sf)
        return sfs

    def _get_nonprompt_photon_sf(self, value):
        path, histnames = value
        ret = {}
        #print("hist name",histnames)
        if len(histnames) != 2:
            raise pepper.config.ConfigError(
                "Need 2 histograms for nonprompt photon scale factors. Got "
                f"{len(histnames)}")
        with uproot.open(self._get_path(path)) as f:
            for chn, histname in zip(("ele", "muon"), histnames):
                ret[chn] = ScaleFactors.from_hist(
                    f[histname], dimlabels=["pho_abseta","pho_pt","Nj"])
        return ret

    def _get_nonprompt_MCcorrection(self, value):
        path, histnames = value
        ret = {}
        if len(histnames) != 2:
            raise pepper.config.ConfigError(
                "Need 2 histograms for nonprompt photon scale factors. Got "
                f"{len(histnames)}")
        with uproot.open(self._get_path(path)) as f:
            for chn, histname in zip(("ele", "muon"), histnames):
                ret[chn] = ScaleFactors.from_hist(
                    f[histname], dimlabels=["pho_abseta","pho_pt"])
        return ret

    def _get_nonprompt_photonsf_all(self, value):
        path, histnames = value
        ret = {}
        if len(histnames) != 2:
            raise pepper.config.ConfigError(
                "Need 2 histograms for nonprompt photon scale factors. Got "
                f"{len(histnames)}")
        with uproot.open(self._get_path(path)) as f:
            for chn, histname in zip(("ele", "muon"), histnames):
                ret[chn] = ScaleFactors.from_hist(
                    f[histname], dimlabels=["pho_abseta","pho_pt"])
        return ret

    def _get_nonprompt_lepton_sf(self, value):
        path, histnames = value
        ret = {}
        if len(histnames) != 2:
            raise pepper.config.ConfigError(
                "Need 2 histograms for nonprompt photon scale factors. Got "
                f"{len(histnames)}")
        with uproot.open(self._get_path(path)) as f:
            for chn, histname in zip(("ele", "muon"), histnames):
                ret[chn] = ScaleFactors.from_hist(
                    f[histname], dimlabels=["lep_pt","lep_abseta","Nj"])
        return ret

    def _get_nonprompt_leptonsf_all(self, value):
        path, histnames = value
        ret = {}
        if len(histnames) != 2:
            raise pepper.config.ConfigError(
                "Need 2 histograms for nonprompt photon scale factors. Got "
                f"{len(histnames)}")
        with uproot.open(self._get_path(path)) as f:
            for chn, histname in zip(("ele", "muon"), histnames):
                ret[chn] = ScaleFactors.from_hist(
                    f[histname], dimlabels=["lep_pt","lep_abseta"])
        return ret

    def _get_nonprompt_leptonsf_all_closure(self, value):
        path, histnames = value
        ret = {}
        if len(histnames) != 2:
            raise pepper.config.ConfigError(
                "Need 2 histograms for nonprompt photon scale factors. Got "
                f"{len(histnames)}")
        with uproot.open(self._get_path(path)) as f:
            for chn, histname in zip(("fake_ele", "fake_muon"), histnames):
                ret[chn] = ScaleFactors.from_hist(
                    f[histname], dimlabels=["lep_pt","lep_abseta"])
        return ret

    def _get_nonprompt_photon_sf_ttbar_double_nonprompt_closure(self, value):
        path, histnames = value
        ret = {}
        #print("hist name",histnames)
        if len(histnames) != 2:
            raise pepper.config.ConfigError(
                "Need 2 histograms for nonprompt photon scale factors. Got "
                f"{len(histnames)}")
        with uproot.open(self._get_path(path)) as f:
            for chn, histname in zip(("fake_ele", "fake_muon"), histnames):
                ret[chn] = ScaleFactors.from_hist(
                    f[histname], dimlabels=["pho_abseta","pho_pt","Nj"])
        return ret

    def _get_nonprompt_MCcorrection_double_nonprompt_closure(self, value):
        path, histnames = value
        ret = {}
        if len(histnames) != 2:
            raise pepper.config.ConfigError(
                "Need 2 histograms for nonprompt photon scale factors. Got "
                f"{len(histnames)}")
        with uproot.open(self._get_path(path)) as f:
            for chn, histname in zip(("fake_ele", "fake_muon"), histnames):
                ret[chn] = ScaleFactors.from_hist(
                    f[histname], dimlabels=["pho_abseta","pho_pt"])
        return ret
