# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.

import pepper
import numpy as np
import coffea
import coffea.lumi_tools
# from coffea.analysis_objects import JaggedCandidateArray as Jca
from functools import partial, reduce
from collections import namedtuple
import uproot
import awkward as ak
import os
import os.path
import glob
import sys
import logging
import random
from numpy.random import RandomState
from copy import copy
import pandas as pd
#from helpers import cut_defs
#from helpers import col_defs
from pepper import sonnenschein, betchart
from pepper.processor_basic import ProcessorBasicPhysics 
from pepper.scale_factors import (TopPtWeigter, PileupWeighter, BTagWeighter,
                                  get_evaluator, ScaleFactors)
#from pepper.config_basic import ConfigBasicPhysics
import json

#/nfs/dust/cms/user/lopesbea/madgraph/CMSSW_12_4_8/src/tWa_LO_nanoGEN

#use this instead of basic config because of photon and PSV scale factors
class ConfigTTGamma(pepper.ConfigBasicPhysics):
    def __init__(self, path_or_file, textparser=json.load, cwd="."):
        """Initialize the configuration.

        Arguments:
        path_or_file -- Either a path to the file containing the configuration
                        or a file-like object of it
        textparser -- Callable to be used to parse the text contained in
                      path_or_file
        cwd -- A path to use as the working directory for relative paths in the
               config. The actual working directory of the process might change
        """
        super().__init__(path_or_file, textparser, cwd)


logger = logging.getLogger(__name__)

class Processor(ProcessorBasicPhysics):

    config_class = ConfigTTGamma
    
    def __init__(self, config, eventdir):
        
        config["histogram_format"] = "root"
        
        super().__init__(config, None)
        if "year" in self.config:
            self.year = self.config["year"]
        if "lumimask" in self.config:
            self.lumimask = self.config["lumimask"]
        else:
            logger.warning("No lumimask specified")
            self.lumimask = None
        if "pileup_reweighting" in self.config:
            self.puweighter = self.config["pileup_reweighting"]
        else:
            logger.warning("No pileup rewegithing specified")
            self.puweighter = None

        if "pdf_types" in config:
            self.pdf_types = config["pdf_types"]
        else:
            if config["compute_systematics"]:
                logger.warning(
                    "pdf_type not specified; will not compute pdf "
                    "uncertainties. (Options are 'Hessian', 'MC' and "
                    "'MC_Gaussian')")
            self.pdf_types = None

        
    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, definine objects and/or
        # compute event weights
        
        era = self.get_era(selector.data, is_mc)
        # Implement the selection steps: add cuts, definine objects and/or
        # compute event weights

        #if self.config["compute_systematics"] and is_mc:
        #    self.add_generator_uncertainies(dsname, selector)

        #GenPhotons
        if is_mc:
            selector.set_column("GenLepton", partial(self.build_genlepton_column, is_mc))
            selector.add_cut("FilterDilepton", self.filter_dilepton)
            selector.add_cut("FilterOSDilepton", self.filter_os_dilepton)
            selector.add_cut("mllcut", self.mll_cut)
            selector.set_column("GenPhoton", partial(self.build_genphoton_column, is_mc))
            selector.add_cut("1Photon", self.one_gen_photon)

        if "ST_tW_" in dsname:
            selector.add_cut("PhotonFromDecay", self.photon_from_decay)

        elif "tWa_NLO" in dsname:
            selector.add_cut("PhotonFromProduction", self.photon_from_production)

        logger.debug("Selection done")

    def filter_dilepton(self,data):

        genlep = data["GenLepton"]
        #make sure they are the leptons from W decay
        genlep = genlep[abs(genlep.parent.pdgId)==24]

        return (ak.num(genlep) >= 2)

    def filter_os_dilepton(self,data):
        
        genlep = data["GenLepton"]
        #make sure they are the leptons from W decay
        genlep = genlep[abs(genlep.parent.pdgId)==24]
        
        #opposite sign just to be sure
        #opposite_sign = ((genlep[:,0].pdgId*genlep[:,1].pdgId)<0)

        dileptons = ak.combinations(genlep, 2)

        are_opposite_sign = ak.any(dileptons['0'].pdgId*dileptons['1'].pdgId<0,axis=1)

        return (are_opposite_sign)

    def mll_cut(self,data):
        
        genlep = data["GenLepton"]
        #make sure they are the leptons from W decay
        genlep = genlep[abs(genlep.parent.pdgId)==24]
        
        #opposite sign just to be sure
        #opposite_sign = ((genlep[:,0].pdgId*genlep[:,1].pdgId)<0)

        dileptons = ak.combinations(genlep, 2)

        #mll minimum because there is a cut in the NLO card
        min_mll = ak.any((dileptons['0']+dileptons['1']).mass>30,axis=1)
        #min_mll = ((genlep[:,0]+genlep[:,1]).mass>30)

        return (min_mll)

    def one_gen_photon(self,data):
        genphoton = data["GenPhoton"]
        accept = (ak.num(genphoton) >= 1)
        return accept

    def photon_from_decay(self,data):

        genpho = data["GenPhoton"]

        #check if there is a top in the b origin
        mother = genpho.parent

        mother_is_lepton = ((abs(mother.pdgId)==11) | (abs(mother.pdgId)==13) | (abs(mother.pdgId)==15))

        mother_is_w_or_b = ((abs(mother.pdgId)==24) | (abs(mother.pdgId)==5))

        #starts by being always true
        not_from_top = (genpho.pdgId==22)

        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"]), 0) != 6))
            mother = mother.parent

        is_from_decay = ((mother_is_lepton) | (~not_from_top & mother_is_w_or_b))

        genpho_production = genpho[~is_from_decay]
        genpho_decay = genpho[is_from_decay]

        return ((ak.num(genpho_production)==0) & (ak.num(genpho_decay)>0))

    def photon_from_production(self,data):

        genpho = data["GenPhoton"]

        #check if there is a top in the b origin
        mother = genpho.parent

        mother_is_lepton = ((abs(mother.pdgId)==11) | (abs(mother.pdgId)==13) | (abs(mother.pdgId)==15))

        mother_is_w_or_b = ((abs(mother.pdgId)==24) | (abs(mother.pdgId)==5))

        #starts by being always true
        not_from_top = (genpho.pdgId==22)

        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & ((ak.fill_none(abs(mother["pdgId"]), 0)) != 6))
            mother = mother.parent

        is_from_decay = ((mother_is_lepton) | (~not_from_top & mother_is_w_or_b))

        genpho_production = genpho[~is_from_decay]

        return ((ak.num(genpho_production)>0))

    def build_genphoton_column(self, is_mc, data):

        genphoton = data["GenPart"]

        genphoton = genphoton[genphoton["pdgId"]==22]
        genphoton = genphoton[genphoton.hasFlags('isFirstCopy')]#(genphoton["status"]==1)

        #pt filtering just to speed it up
        genphoton = genphoton[genphoton.pt>5]

        mother = genphoton.parent

        mother_is_not_hadron = True

        while not ak.all(ak.is_none(mother, axis=1)):
            mother_is_not_hadron = (mother_is_not_hadron & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 37) | (ak.fill_none(abs(mother["pdgId"]), 0) == 2212) )) #mother is not hadron, except proton, proton is okay
            mother = mother.parent
 
        #genphoton = genphoton[mother_is_not_hadron]

        has_pt = (genphoton["pt"]>20)
        has_eta = (abs(genphoton["eta"])<2.5)

        genpart = data["GenPart"]   

        is_relevant_lep = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) & 
                    ((abs(genpart["pdgId"])==11) | (abs(genpart["pdgId"])==13)) | (abs(genpart["pdgId"])==15))

        relevant_lep = genpart[is_relevant_lep]

        has_lep_close = ak.any(genphoton.metric_table(relevant_lep) < 0.4, axis=2) 
        
        is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) & 
                    (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=16) & (abs(genpart["pdgId"])<37) & (abs(genpart["pdgId"])!=22))

        relevant_part = genpart[is_relevant_part]

        has_part_close = ak.any(
            genphoton.metric_table(relevant_part) < 0.1, axis=2) 

        genphoton = genphoton[has_pt & has_eta & ~has_lep_close & ~has_part_close & mother_is_not_hadron]

        return genphoton

    def build_genlepton_column(self, is_mc, data):

        genlepton = data["GenPart"]

        genlepton = genlepton[( (abs(genlepton["pdgId"])==15) | (abs(genlepton["pdgId"])==13) | (abs(genlepton["pdgId"])==11) )]
        is_first_copy = (genlepton.hasFlags('isFirstCopy'))

        #has_pt = (genlepton["pt"]>15)
        #has_eta = (abs(genlepton["eta"])<2.5)

        genlepton = genlepton[is_first_copy]# & has_pt & has_eta]

        try:
            genlepton = genlepton[ak.argsort(genlepton["pt"], ascending=False)]
        except:
            genlepton = genlepton

        return genlepton

    def build_genpart_column(self, is_mc, data):
        genpart = data["GenPart"]

        return genpart
