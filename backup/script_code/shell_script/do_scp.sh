#!/bin/bash

for dir in `ls -d *`
do
 if [[ -d $dir && $dir != "hists" && ! `grep -c $dir f_dir` -ne '0' ]]; then
    echo $dir
    sh scp_file.sh $dir
 fi
done
