#!/bin/bash
for f in `ls -d *`
do
   if [[ -d $f && $f != "hists" ]]; then
     echo $f
     cd $f
     if [[ -f "out.root" ]]; then
        echo "out.root exists"
     else
        hadd out.root *.root
     fi
     cd -
   fi
done
