#!/bin/bash
for dir in `ls -d *`
do
   if [[ -d $dir && $dir != "hists" ]]; then
     echo $dir
     cd $dir
     for f in `ls *root.duplicate`
     do
        if [[ $f != "out.root.duplicate" ]];then
           echo $f
           rm $f
        fi
     done
     cd -
   fi
done
