# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.

import pepper
import awkward as ak
import logging
from functools import partial
from copy import copy
import numpy as np
from tools.utils import DeltaR

logger = logging.getLogger(__name__)

class Processor(pepper.ProcessorBasicPhysics):
    config_class = pepper.ConfigTTbarLL
    def __init__(self, config, eventdir):
        super().__init__(config, eventdir)
        config["histogram_format"] = "root"
        
    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, definine objects and/or
        # compute event weights
        
        era = self.get_era(selector.data, is_mc)
        # Implement the selection steps: add cuts, definine objects and/or
        # compute event weights

        #MEPhotons
        if is_mc:
            selector.set_column("GenLepton", partial(self.build_genlepton_column, is_mc))
            selector.set_column("MEPhoton", partial(self.build_MEphoton_column, is_mc))

            selector.add_cut("removeSToverlap", partial(self.remove_stnlo_overlap,dsname))
            selector.add_cut("removeTToverlap", partial(self.remove_ttnlo_overlap,dsname))
            selector.add_cut("isolation_me", self.apply_menpho_isolation)

            selector.add_cut("filteronelepton", self.filter_onelepton)
            selector.set_cat("channel",{"ele", "muon"})
            selector.set_multiple_columns(self.lepton_cats)

            selector.set_column("GenPhoton", partial(self.build_genphoton_column, is_mc))
            selector.add_cut("isolation", self.apply_genpho_isolation)
            selector.set_column("GenJet",partial(self.build_genjet_column,is_mc))
            selector.set_column("GenbJet",partial(self.build_genbjet_column,is_mc))


            selector.set_cat("genphoton_type", {'All','PhotonFromDecay','PhotonFromProduction'} )
            selector.set_multiple_columns(self.genphoton_categories)

            selector.add_cut("1Photon", self.one_gen_photon)
            selector.add_cut("3Jet",self.atLeast3jet)
            selector.add_cut("1bJet",self.atLeast1bjet)
            
            selector.set_column("genDRphoLep",self.build_genDRphoLep)
            selector.set_column("genDRphoJet",self.build_genDRphoJet)
            selector.set_column("genDRphobJet",self.build_genDRphobJet)
            selector.set_column("genDRlepJet",self.build_genDRlepJet)
            selector.set_column("genDRlepbJet",self.build_genDRlepbJet)

        logger.debug("Selection done")

    def filter_onelepton(self,data):

        genlep = data["GenLepton"]
        #make sure they are the leptons from W decay
        mother = genlep.parent
        from_W = ( (ak.fill_none(abs(mother.pdgId)==24,False)) |
                   ((ak.fill_none(abs(mother.pdgId)==15,False) & ak.fill_none(abs(mother.parent.pdgId)==24,False))) )
        genlep = genlep[from_W]

        return (ak.num(genlep) == 1)

    def lepton_cats(self,data):
        genlepton = data["GenLepton"]
        cat = {}
        cat['ele'] = abs(genlepton[:,0].pdgId)==11
        cat['muon'] = abs(genlepton[:,0].pdgId)==13
        cat['lepton'] = (cat['ele']) | (cat['muon'])

        return cat

    def one_gen_photon(self,data):
        genphoton = data["GenPhoton"]
        accept = (ak.num(genphoton) >= 1)
        return accept

    def genphoton_categories(self,data):

        cats = {}
        genpho = data["GenPhoton"]

        #check if there is a top in the b origin
        mother = genpho.parent

        mother_is_lepton = ((abs(mother.pdgId)==11) | (abs(mother.pdgId)==13) | (abs(mother.pdgId)==15))

        mother_is_w_or_b = ((abs(mother.pdgId)==24) | (abs(mother.pdgId)==5))

        #starts by being always true
        not_from_top = (genpho.pdgId==22)

        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6,True ) ))
            mother = mother.parent

        is_from_decay = ((mother_is_lepton) | (~not_from_top & mother_is_w_or_b))

        genpho_production = genpho[~is_from_decay]
        genpho_decay = genpho[is_from_decay]

        cats["PhotonFromDecay"] = ( (ak.num(genpho_decay)>0) & (ak.num(genpho_production)==0) )
        cats["PhotonFromProduction"] = ( (ak.num(genpho_decay)==0) & (ak.num(genpho_production)>0) )
        cats["All"] = ( (ak.num(genpho_decay)>0) | (ak.num(genpho_production)>0) )
  
        return cats

    def photon_from_decay(self,data):
        genpho = data["GenPhoton"]

        #check if there is a top in the b origin
        mother = genpho.parent

        mother_is_lepton = ((abs(mother.pdgId)==11) | (abs(mother.pdgId)==13) | (abs(mother.pdgId)==15))

        mother_is_w_or_b = ((abs(mother.pdgId)==24) | (abs(mother.pdgId)==5))

        #starts by being always true
        not_from_top = (genpho.pdgId==22)

        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"]), 0) != 6))
            mother = mother.parent

        is_from_decay = ((mother_is_lepton) | (~not_from_top & mother_is_w_or_b))

        genpho_production = genpho[~is_from_decay]
        genpho_decay = genpho[is_from_decay]

        return  ((ak.num(genpho_production)==0) & (ak.num(genpho_decay)>0))
 
    def photon_from_production(self,data):

        genpho = data["GenPhoton"]

        #check if there is a top in the b origin
        mother = genpho.parent

        mother_is_lepton = ((abs(mother.pdgId)==11) | (abs(mother.pdgId)==13) | (abs(mother.pdgId)==15))

        mother_is_w_or_b = ((abs(mother.pdgId)==24) | (abs(mother.pdgId)==5))

        #starts by being always true
        not_from_top = (genpho.pdgId==22)

        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & ((ak.fill_none(abs(mother["pdgId"]), 0)) != 6))
            mother = mother.parent

        is_from_decay = ((mother_is_lepton) | (~not_from_top & mother_is_w_or_b))

        genpho_decay = genpho[is_from_decay]
        genpho_production = genpho[~is_from_decay]

#        return ((ak.num(genpho_production)>0))
        return (ak.num(genpho_production)>0) & (ak.num(genpho_decay)==0)

    def atLeast3jet(self,data):
        return ak.num(data["GenJet"])>=3
 
    def atLeast1bjet(self,data):
        return ak.num(data["GenbJet"])>=1
 
    def build_genjet_column(self, is_mc, data):

        genjet = data["GenJet"]

        genlepton = data["GenLepton"]
        genphoton = data["GenPhoton"]
        has_lep_close = ak.any(genjet.metric_table(genlepton) < 0.4, axis=2) 
        has_pho_close = ak.any(genjet.metric_table(genphoton) < 0.1, axis=2) 

        genjet = genjet[(~has_lep_close) & (~has_pho_close)]

        return genjet

    def build_genbjet_column(self, is_mc, data):

        genjet = data["GenJet"]
        genbjet = genjet[abs(genjet.partonFlavour)==5]

        genlepton = data["GenLepton"]
        genphoton = data["GenPhoton"]
        has_lep_close = ak.any(genbjet.metric_table(genlepton) < 0.4, axis=2) 
        has_pho_close = ak.any(genbjet.metric_table(genphoton) < 0.1, axis=2) 

        genbjet = genbjet[(~has_lep_close) & (~has_pho_close)]

        return genbjet

    def build_MEphoton_column(self, is_mc, data):

        genphoton = data["GenPart"]

        genphoton = genphoton[genphoton["pdgId"]==22]
        #genphoton = genphoton[(genphoton["status"]==1)]
        genphoton = genphoton[genphoton.hasFlags('isFirstCopy')]#FisrtCopy is more parton level, before radiation

        #pt filtering just to speed it up
        has_pt = (genphoton["pt"]>20)
        has_eta = (abs(genphoton["eta"])<2.6)
        genphoton = genphoton[has_pt & has_eta]

        mother = genphoton.parent
        mother_is_not_hadron = True

        while not ak.all(ak.is_none(mother, axis=1)):
            mother_is_not_hadron = (mother_is_not_hadron & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 37) | (ak.fill_none(abs(mother["pdgId"]), 0) == 2212) )) #mother is not hadron, except proton, proton is okay
            mother = mother.parent
 
        genphoton = genphoton[mother_is_not_hadron]

        return genphoton

    def build_genphoton_column(self, is_mc, data):

        genphoton = data["GenPart"]

        genphoton = genphoton[genphoton["pdgId"]==22]
        genphoton = genphoton[(genphoton["status"]==1)]
        #genphoton = genphoton[genphoton.hasFlags('isFirstCopy')]#FisrtCopy is more parton level, before radiation

        #pt filtering just to speed it up
        has_pt = (genphoton["pt"]>20)
        has_eta = (abs(genphoton["eta"])<2.6)
        genphoton = genphoton[has_pt & has_eta]

        mother = genphoton.parent
        mother_is_not_hadron = True

        while not ak.all(ak.is_none(mother, axis=1)):
            mother_is_not_hadron = (mother_is_not_hadron & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 37) | (ak.fill_none(abs(mother["pdgId"]), 0) == 2212) )) #mother is not hadron, except proton, proton is okay
            mother = mother.parent

        genlepton = data["GenLepton"]

        has_lep_close = ak.any(genphoton.metric_table(genlepton) < 0.4, axis=2) 
        genphoton = genphoton[~has_lep_close & mother_is_not_hadron]

        return genphoton

    def build_MElepton_column(self, is_mc, data):

        genlepton = data["GenPart"]

        genlepton = genlepton[( (abs(genlepton["pdgId"])==13) | (abs(genlepton["pdgId"])==11) )]
        is_stable = (genlepton['status']==1)
        is_first_copy = (genlepton.hasFlags('isFirstCopy'))# more parton-level
        is_last_copy = (genlepton.hasFlags('isLastCopy')) #particle-level

        has_pt = (genlepton["pt"]>30)
        has_eta = (abs(genlepton["eta"])<2.5)

        #genlepton = genlepton[is_stable & has_pt & has_eta]
        genlepton = genlepton[is_first_copy & has_pt & has_eta]
        #genlepton = genlepton[is_last_copy & has_pt & has_eta]

        mother = genlepton.parent
        mother_is_not_hadron = True

        while not ak.all(ak.is_none(mother, axis=1)):
            mother_is_not_hadron = (mother_is_not_hadron & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 37) | (ak.fill_none(abs(mother["pdgId"])== 2212, True) ) )) #mother is not hadron, except proton, proton is okay
            mother = mother.parent

        genlepton = genlepton[mother_is_not_hadron]

        try:
            genlepton = genlepton[ak.argsort(genlepton["pt"], ascending=False)]
        except:
            genlepton = genlepton

        return genlepton

    def build_genlepton_column(self, is_mc, data):

        genlepton = data["GenPart"]

        is_lepton = ( (abs(genlepton["pdgId"])==13) | (abs(genlepton["pdgId"])==11) )
        is_final_state = genlepton["status"]==1

        has_pt = genlepton["pt"]>30.
        has_eta = abs(genlepton["eta"])<5.

        genlepton = genlepton[is_lepton & is_final_state & has_pt & has_eta]

        return genlepton

    def build_genpart_column(self, is_mc, data):
        genpart = data["GenPart"]

        return genpart

    def remove_ttnlo_overlap(self,dsname, data):
    
        if (not "TTTo" in dsname and not dsname.startswith("TTGJets") ) :
            accept = ak.num(data["GenLepton"]) > 0
            return accept
    
        genphotons = data["MEPhoton"]
        genpart = data["GenPart"]
    
        pt_cut = genphotons["pt"]>10
        eta_cut = abs(genphotons["eta"])<2.6
        genphotons = genphotons[eta_cut & pt_cut]
    
        mother = genphotons.parent
        #starts by being always true
        mother_is_not_hadron = True
    
        while not ak.all(ak.is_none(mother, axis=1)):
            mother_is_not_hadron = (mother_is_not_hadron & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 37) | (ak.fill_none(abs(mother["pdgId"])== 2212, True) ) )) #mother is not hadron, except proton, proton is okay
            mother = mother.parent
    
        mother = genphotons.parent
        mother_is_ISR_or_top = True
        while not ak.all(ak.is_none(mother, axis=1)): #initial gluon mother is okay 
              mother_is_ISR_or_top = (mother_is_ISR_or_top & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 7) | (ak.fill_none(abs(mother["pdgId"])== 21, True) ) | (ak.fill_none(abs(mother["pdgId"]) == 2212, True) )) )
              #mother_is_ISR_or_top = (mother_is_ISR_or_top & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 7) | (ak.fill_none(abs(mother["pdgId"]) == 2212, True ))))
              mother = mother.parent
    
        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphotons.metric_table(relevant_part) < 0.05, axis=2)
    
        #genphotons = genphotons[mother_is_not_hadron & ~has_part_close]
        genphotons = genphotons[mother_is_ISR_or_top & ~has_part_close]   
        #genphotons = genphotons[mother_is_ISR_or_top]
    
        if "TTTo" in dsname :
            accept = (ak.num(genphotons)==0)
        elif dsname.startswith("TTGJets"):
            accept = (ak.num(genphotons)>0)
        return accept

    def remove_stnlo_overlap(slef,dsname, data):
        if (not "ST_t-channel" in dsname and not dsname.startswith("TGJets_lepton")) :
            accept = ak.num(data["GenLepton"]) > 0
            return accept
        genphotons = data["MEPhoton"]
        genpart = data["GenPart"]
  
        pt_cut = genphotons["pt"]>10
        eta_cut = abs(genphotons["eta"])<2.6
        genphotons = genphotons[eta_cut & pt_cut]
  
        #check if there is a hadron in the photon origin
        mother = genphotons.parent
  
        #starts by being always true
        mother_is_not_hadron = True
  
        while not ak.all(ak.is_none(mother, axis=1)):
            mother_is_not_hadron = (mother_is_not_hadron & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 37) | (ak.fill_none(abs(mother["pdgId"])== 2212, True) ) )) #mother is not hadron, except proton, proton is okay
            mother = mother.parent
  
        mother = genphotons.parent
        mother_is_ISR_or_top = True
        while not ak.all(ak.is_none(mother, axis=1)):#initial gluon mother is okay 
              mother_is_ISR_or_top = (mother_is_ISR_or_top & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 7) | (ak.fill_none(abs(mother["pdgId"])== 21, True) ) | (ak.fill_none(abs(mother["pdgId"])== 2212, True) )))
              #mother_is_ISR_or_top = (mother_is_ISR_or_top & ( (ak.fill_none(abs(mother["pdgId"]), 0) < 7) | (ak.fill_none(abs(mother["pdgId"])== 2212, True) )))
              mother = mother.parent
  
        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))
  
        relevant_part = genpart[is_relevant_part]
  
        has_part_close = ak.any(
                genphotons.metric_table(relevant_part) < 0.05, axis=2)
 
        #genphotons = genphotons[mother_is_not_hadron & ~has_part_close]
        genphotons = genphotons[mother_is_ISR_or_top & ~has_part_close]
        #genphotons = genphotons[mother_is_ISR_or_top]
  
        if "ST_t-channel" in dsname :
            accept = (ak.num(genphotons)==0)
        elif dsname.startswith("TGJets_lepton") :
            accept = (ak.num(genphotons)>0)
        return accept

    def apply_mepho_isolation(self, data):

        genphotons = data["MEPhoton"]
        genpart = data["GenPart"]

        is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

        relevant_part = genpart[is_relevant_part]

        has_part_close = ak.any(
                genphotons.metric_table(relevant_part) < 0.1, axis=2)

        genphotons = genphotons[~has_part_close]

        accept = (ak.num(genphotons)>0)

        return accept

    def apply_genpho_isolation(self, data):
    
        genphotons = data["GenPhoton"]
        genpart = data["GenPart"]
    
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphotons.metric_table(relevant_part) < 0.1, axis=2)
    
        genphotons = genphotons[~has_part_close]
    
        accept = (ak.num(genphotons)>0)

        return accept

    def build_genDRphoLep(self,data):

        genphoton = data["GenPhoton"]
        genlepton = data["GenLepton"]
        genDRphoLep = DeltaR(genphoton[:,0],genlepton[:,0])

        return genDRphoLep

    def build_genDRphoJet(self,data):

        genphoton = data["GenPhoton"]
        genjet = data["GenJet"]
        genDRphoJet = DeltaR(genphoton[:,0],genjet[:,0])

        return genDRphoJet

    def build_genDRphobJet(self,data):

        genphoton = data["GenPhoton"]
        genbjet = data["GenbJet"]
        genDRphobJet = DeltaR(genphoton[:,0],genbjet[:,0])

        return genDRphobJet

    def build_genDRlepJet(self,data):

        genlep = data["GenLepton"]
        genjet = data["GenbJet"]
        genDRlepJet = DeltaR(genlep[:,0],genjet[:,0])

        return genDRlepJet

    def build_genDRlepbJet(self,data):

        genlep = data["GenLepton"]
        genbjet = data["GenbJet"]
        genDRlepbJet = DeltaR(genlep[:,0],genbjet[:,0])

        return genDRlepbJet
