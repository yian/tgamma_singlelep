# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.
# Here we create a very simplified version of the ttbar-to-dilep processor.
# One can run this processor using
# 'python3 -m pepper.runproc --debug example_processor.py example_config.json'
# Above command probably will need a little bit of time before all cuts are
# applied once. This is because a chunk of events are processed simultaneously.
# You change adjust the number of events in a chunk and thereby the memory
# usage by using the --chunksize parameter (the default value is 500000).

from functools import reduce
import numpy as np
import awkward as ak
import coffea
import coffea.lumi_tools
import coffea.jetmet_tools
import uproot
import logging
from dataclasses import dataclass
from typing import Optional, Tuple

import pepper
from pepper import sonnenschein, betchart
import pepper.config
from config_topgamma import ConfigTopGamma


logger = logging.getLogger(__name__)

# All processors should inherit from pepper.ProcessorBasicPhysics
@dataclass
class BasicFuncs(pepper.ProcessorBasicPhysics):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = ConfigTopGamma

    def __init__(self, config, eventdir):
        super().__init__(config, eventdir)


    def add_PV_cut(self,data):
       PV = data["PV"]
       oneGoodPV = PV.npvsGood >0
       return oneGoodPV

    def build_lepton_column(self, is_mc, rng, data):
        """Build a lepton column containing electrons and muons."""
        electron = data["Electron"]
        muon = data["Muon"]
        # Apply Rochester corrections to muons
        if "muon_rochester" in self.config:
            muon = self.apply_rochester_corr(muon, rng, is_mc)
        columns = ["pt", "eta", "phi", "mass", "pdgId","charge"]
        if is_mc:
           columns.append("genPartIdx")
        lepton = {}
        for column in columns:
            lepton[column] = ak.concatenate([electron[column], muon[column]],
                                            axis=1)
        lepton = ak.zip(lepton, with_name="PtEtaPhiMLorentzVector",
                        behavior=data.behavior)

        # Sort leptons by pt
        # Also workaround for awkward bug using ak.values_astype
        # https://github.com/scikit-hep/awkward-1.0/issues/1288
        lepton = lepton[
            ak.values_astype(ak.argsort(lepton["pt"], ascending=False), int)]
        return lepton

    def pick_medium_photons(self, data):
        photon = data["Photon"]
        leptons = data["Lepton"]
        has_id = photon.cutBased>=2 # medium ID
        pass_psv = (photon.pixelSeed==False)

        is_in_EBorEE = (photon.isScEtaEB | photon.isScEtaEE)

        etacuts = (abs(photon["eta"])<2.5)
        ptcuts = (photon.pt>20)

        has_lepton_close = ak.any(
            photon.metric_table(leptons) < 0.4, axis=2)

        is_good = (
                has_id
          & (~has_lepton_close)
              & pass_psv
              & etacuts
              & is_in_EBorEE
              & ptcuts)

        pho_hem1516 = self.in_hem1516(photon.phi, photon.eta)
        if self.config["hem_cut_if_pho"]:
           is_good = is_good & ~pho_hem1516

        return photon[is_good]

    def pick_loose_photons(self, data):
        photon = data["Photon"]
        leptons = data["Lepton"]
        has_id = photon.cutBased>=2 # medium ID
        pass_psv = (photon.pixelSeed==False)

        is_in_EBorEE = (photon.isScEtaEB | photon.isScEtaEE)
        is_in_transreg = ( (1.4442 < abs(photon["eta"])) & (abs(photon["eta"]) < 1.566) )

        etacuts = (abs(photon["eta"])<2.5)
        ptcuts = (photon.pt>20)

        has_lepton_close = ak.any(
            photon.metric_table(leptons) < 0.4, axis=2)

        bitMap = photon["vidNestedWPBitmap"]

        passHoverE = (bitMap>>4&3)  >= 2
        passSIEIE = (bitMap>>6&3)  >= 2
        passChIso = (bitMap>>8&3)  >= 2
        passNeuIso = (bitMap>>10&3) >= 2
        passPhoIso = (bitMap>>12&3) >= 2

        isRelMediumPhoton = ((passHoverE) & (passNeuIso) & (passPhoIso))
        #isRelMediumPhoton = ((passHoverE) & (passNeuIso) & (passPhoIso) & (passChIso) & ~(passSIEIE))
        is_good = (
                isRelMediumPhoton
              & ~is_in_transreg
              & (is_in_EBorEE)
              & etacuts
              & pass_psv
              & (~has_lepton_close)
              & ptcuts)

        pho_hem1516 = self.in_hem1516(photon.phi, photon.eta)
        if self.config["hem_cut_if_pho"]:
           is_good = is_good & ~pho_hem1516

        return photon[is_good]

    def good_jet(self, data):
        """Apply some basic jet quality cuts."""
        jets = data["Jet"]
        leptons = data["Lepton"]
        photons = data["Photon"]

        j_id, j_puId, lep_dist, pho_dist, eta_min, eta_max, pt_min = self.config[[
            "good_jet_id", "good_jet_puId", "good_jet_lepton_distance", "good_jet_photon_distance",
            "good_jet_eta_min", "good_jet_eta_max", "good_jet_pt_min"]]

        if j_id == "skip":
            has_id = True
        elif j_id == "cut:loose":
            has_id = jets.isLoose
            # Always False in 2017 and 2018
        elif j_id == "cut:tight":
            has_id = jets.isTight
        elif j_id == "cut:tightlepveto":
            has_id = jets.isTightLeptonVeto
        else:
            raise pepper.config.ConfigError(
                    "Invalid good_jet_id: {}".format(j_id))

        j_pt = jets.pt
        if "jetfac" in ak.fields(data):
            j_pt = j_pt * data["jetfac"]

        has_lepton_close = ak.any(
            jets.metric_table(leptons) < lep_dist, axis=2)
        has_photon_close = ak.any(
            jets.metric_table(photons) < pho_dist, axis=2)
        is_good = (   (has_id)
                    & (j_pt > pt_min)
                    & (eta_min < jets.eta)
                    & (jets.eta < eta_max)
                    & (~has_lepton_close)
                    & (~has_photon_close)
                  )

        jet_hem1516 = self.in_hem1516(jets.phi, jets.eta)
        if self.config["hem_cut_if_jet"]:
           is_good = is_good & ~jet_hem1516

        return is_good

    def passing_hlt(self,trigger_path,data):
        hlt = data["HLT"]
        triggered = np.full(len(data), False)
        channel_ele = data["ele"]

        if "16" in self.config["year"]:
           for trig_p in trigger_path:
               triggered = triggered | np.asarray(hlt[trig_p])
        elif "17" in self.config["year"]:
           trigobjs_ele = ak.sum(data["TrigObj"].id == 11,axis=1)>0
           pass_L1SingleEGOrFilter = ak.sum((data["TrigObj"].filterBits & (1 << 10) == (1 << 10)),axis=1)>0
           pass_eleTrig_final = (np.asarray(hlt[trigger_path]) & trigobjs_ele & pass_L1SingleEGOrFilter)
           #print("<<<<<<",hlt[trigger_path])
           #print("<<<<<<",np.asarray(hlt[trigger_path]))

           pass_hlt = ak.where(channel_ele,pass_eleTrig_final,np.asarray(hlt[trigger_path]))
           triggered |= np.asarray(pass_hlt)

           #print(">>>> channel",data["muon"],' Orig trig ',triggered_test,ak.sum(triggered_test))
           #print(">>>> channel",data["muon"],' new trig ', triggered,ak.sum(triggered))

        elif "18" in self.config["year"]:
           triggered |= np.asarray(hlt[trigger_path])

        return triggered

    def compute_electron_sf(self, data):
        # Electron reconstruction and identification
        eles = data["Electron"]
        weight = np.ones(len(data))
        systematics = {}
        # Electron identification efficiency
        for i, sffunc in enumerate(self.config["electron_sf"]):
            pt_th = self.config["electron_sf_overflow_pt"][i]
            eles_pt =  ak.where(eles.pt<pt_th,eles.pt,pt_th-1)
            sceta = eles.eta + eles.deltaEtaSC
            params = {}
            for dimlabel in sffunc.dimlabels:
                if dimlabel == "abseta":
                    params["abseta"] = abs(sceta)
                elif dimlabel == "eta":
                    params["eta"] = sceta
                elif dimlabel == "pt":
                    params["pt"] = eles_pt
                else:
                    params[dimlabel] = getattr(eles, dimlabel)
            central = ak.prod(sffunc(**params), axis=1)
            key = "electronsf{}".format(i)
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                up = ak.prod(sffunc(**params, variation="up"), axis=1)
                down = ak.prod(sffunc(**params, variation="down"), axis=1)
                systematics[key] = (up / central, down / central)
            weight = weight * central

        return weight, systematics

    def compute_muon_sf(self, data):
        # Muon identification and isolation efficiency
        muons = data["Muon"]
        weight = np.ones(len(data))
        systematics = {}
        for i, sffunc in enumerate(self.config["muon_sf"]):
            pt_th = self.config["muon_sf_overflow_pt"][i]
            muons_pt = ak.where(muons.pt<pt_th,muons.pt,pt_th-1)
            print(i,"print the overflow pt",self.config["muon_sf_overflow_pt"][i])
            params = {}
            for dimlabel in sffunc.dimlabels:
                if   dimlabel == "abseta":
                    params["abseta"] = abs(muons.eta)
                elif dimlabel == "pt": 
                    params["pt"] = muons_pt
                else:
                    params[dimlabel] = getattr(muons, dimlabel)
            central = ak.prod(sffunc(**params), axis=1)
            key = f"muonsf{i}"
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                if ("split_muon_uncertainty" not in self.config
                        or not self.config["split_muon_uncertainty"]):
                    unctypes = ("",)
                else:
                    unctypes = ("stat ", "syst ")
                for unctype in unctypes:
                    up = ak.prod(sffunc(
                        **params, variation=f"{unctype}up"), axis=1)
                    down = ak.prod(sffunc(
                        **params, variation=f"{unctype}down"), axis=1)
                    systematics[key + unctype.replace(" ", "")] = (
                        up / central, down / central)
            weight = weight * central
        return weight, systematics

    def compute_photon_sf(self, data):
        # Photon identification+PSV SFs
        phos = data["Photon"]
        weight = np.ones(len(data))
        systematics = {}
        for i, sffunc in enumerate(self.config["photon_sf"]):
            pt_th = self.config["photon_sf_overflow_pt"][i]
            phos_pt = ak.where(phos.pt<pt_th,phos.pt,pt_th-1)
            params = {}
            for dimlabel in sffunc.dimlabels:
                if dimlabel == "abseta":
                    params["abseta"] = abs(phos.eta)
                elif dimlabel == "pt":
                    params["pt"] = phos_pt
                else:
                    params[dimlabel] = getattr(phos, dimlabel)
            central = ak.prod(sffunc(**params), axis=1)
            key = "photonsf{}".format(i)
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                up = ak.prod(sffunc(**params, variation="up"), axis=1)
                down = ak.prod(sffunc(**params, variation="down"), axis=1)
                systematics[key] = (up / central, down / central)

            weight = weight * central
        return weight, systematics

    def compute_psv_sf(self, data):
        phos = data["Photon"]
        phos["etabin"] = ak.where(abs(phos.eta) < 1.5, 0.5, 3.5)
        weight = np.ones(len(data))
        systematics = {}
        # Photon identification+PSV SFs
        for i, sffunc in enumerate(self.config['psv_sf']):
            central = ak.prod(sffunc(etabin=phos["etabin"]),axis=1)
            key = "PSVsf{}".format(i)
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                up = ak.prod(sffunc(
                    etabin=phos["etabin"], variation="up"),axis=1)
                down = ak.prod(sffunc(
                    etabin=phos["etabin"], variation="down"),axis=1)
                systematics[key] = (up / central, down / central)
            weight = weight * central

        return weight, systematics

    def compute_nonprompt_photon_sf(self, data):
        ones = np.ones(len(data))
        zeros = np.zeros(len(data))
        channels = ["ele", "muon"]
        nonprompt_photon_sf = self.config["nonprompt_photon_sf"]
        print("check1",nonprompt_photon_sf["ele"].dimlabels)

        pho = data["Photon"]
        pho_pt = ak.where(pho.pt<500,pho.pt,499)

        njet = ak.num(data["Jet"])
        njet = ak.broadcast_arrays(njet,pho_pt)[0]

        central = ones
        for channel in channels:
            sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho_pt,
                                              Nj = njet
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)
            print(channel,central)

        if self.config["only_nonprompt_syst"] and self.config["compute_systematics"]:
           systematics = {}
           for i, sffunc in enumerate(
                      [self.config["nonprompt_photon_sf_unc_SB"],self.config["nonprompt_photon_sf_unc_coarse_bin"]]):
               diff = zeros
               up = ones
               down = ones
               central_new = ones
               key = "nonprompt_photon_sf{}".format(i)
               for channel in channels:
                   sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                                     pho_pt = pho_pt,
                                                     Nj = njet
                                                    )
                   sf_new = sffunc[channel](pho_abseta = abs(pho.eta),
                                                     pho_pt = pho_pt,
                                                     Nj = njet
                                           )
                   sf = ak.prod(sf,axis=1)
                   sf_new = ak.prod(sf_new,axis=1)
                   central_new = ak.where(data[channel], sf_new, central_new)
                   diff = ak.where(abs(diff)>sf,np.sign(diff)*sf,diff)
                   diff = ak.where(data[channel], (sf_new - sf), diff)

                   up = ak.where(data[channel], sf+diff, up)
                   down = ak.where(data[channel], sf-diff, down)
                   systematics[key] = (up / central, down / central)

                   print(channel,"####### central ",central_new)
                   print(channel,"####### diff ",diff)
                   print(channel,"####### up ",up)
                   print(channel,"####### down ",down)
           return central, systematics
        return central

    def compute_nonprompt_photonsf_all(self, data):
        pho = data["Photon"]
        pho = ak.pad_none(pho,1)
        ones = np.ones(len(data))
        central = ones
        channels = ["ele", "muon"]
        nonprompt_photon_sf = self.config["nonprompt_photonsf_all"]
        pho_pt = ak.where(pho.pt<500,pho.pt,499)

        for channel in channels:
            sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho_pt
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)
        if self.config["compute_systematics"] and self.config["only_nonprompt_syst"]:
            up = ones
            down = ones
            for channel in channels:
                sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                                  pho_pt = pho_pt,
                                                  variation="up")
                sf = ak.prod(sf,axis=1)
                up = ak.where(data[channel], sf, up)

                sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                                  pho_pt = pho_pt,
                                                  variation="down")
                sf = ak.prod(sf,axis=1)
                down = ak.where(data[channel], sf, down)
            return central, {"nonprompt_photon_sf": (up / central, down / central)}
        return central

    def compute_nonprompt_MCcorrection(self, data):
        pho = data["Photon"]
        pho = ak.pad_none(pho,1)
        ones = np.ones(len(data))
        central = ones
        channels = ["ele", "muon"]
        nonprompt_photon_sf = self.config["nonprompt_MCcorrection"]
        pho_pt = ak.where(pho.pt<500,pho.pt,499)
        for channel in channels:
            sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                              pho_pt = pho_pt,
                                             )
            sf = ak.prod(sf,axis=1)
            central = ak.where(data[channel], sf, central)
        if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
            up = ones
            down = ones
            for channel in channels:
                sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                                  pho_pt = pho_pt,
                                                  variation="up")
                sf = ak.prod(sf,axis=1)
                up = ak.where(data[channel], sf, up)

                sf = nonprompt_photon_sf[channel](pho_abseta = abs(pho.eta),
                                                  pho_pt = pho.pt,
                                                  variation="down")
                sf = ak.prod(sf,axis=1)
                down = ak.where(data[channel], sf, down)
            return central, {"nonprompt_MCcorrection": (up / central, down / central)}
        return central

    def compute_jet_puid_sfs(self,is_mc,data):
        jets = data["Jet"][(data["Jet"].pt < 50) & (data["Jet"].has_gen_jet)]
        weight = np.ones(len(data))
        pass_puid = jets["pass_pu_id"]
        systematics = {}
        sf_eff = []
        for i, sffunc in enumerate(self.config["jet_puid_sf"]):
            params = {}
            #print(sffunc)
            for dimlabel in sffunc.dimlabels:
                if dimlabel == "abseta":
                    params["abseta"] = abs(jets.eta)
                else:
                    params[dimlabel] = getattr(jets, dimlabel)
            sf_eff.append(sffunc(pt=jets.pt,eta=jets.eta))
        prob_mc = ak.prod(ak.where(pass_puid,sf_eff[0],1-sf_eff[0]),axis=1)
        prob_data = ak.prod(ak.where(pass_puid,sf_eff[0]*sf_eff[1],1-sf_eff[0]*sf_eff[1]),axis=1)

        if is_mc:
           central = prob_data/prob_mc
        else:
           central = np.ones(len(data))

        if (self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]
               and len(self.config["jet_puid_sf_unc"])>0 and is_mc):
           for i, sffunc in enumerate(self.config["jet_puid_sf_unc"]):
               #print(sffunc)
               up = sf_eff[1]+sffunc(pt=jets.pt,eta=jets.eta)
               down = sf_eff[1]-sffunc(pt=jets.pt,eta=jets.eta)
               prob_data_up = ak.prod(ak.where(pass_puid,sf_eff[0]*up,1-sf_eff[0]*up),axis=1)
               prob_data_down = ak.prod(ak.where(pass_puid,sf_eff[0]*down,1-sf_eff[0]*down),axis=1)
               up = prob_data_up/prob_mc
               down = prob_data_down/prob_mc
               key="puid_sf{}".format(i)
               systematics[key] = (up / central, down / central)

        weight = weight * central

        return  weight,systematics

    def compute_weight_btag(self, data, efficiency="central", never_sys=False):
        """Compute event weights and systematics, if requested, for the b
        tagging"""
        jets = data["CentralJet"]
        wp = self.config["btag"].split(":", 1)[1]
        flav = jets["hadronFlavour"]
        eta = abs(jets.eta)
        pt = jets.pt
        discr = jets["btag"]
        weight = np.ones(len(data))
        systematics = {}
        for i, weighter in enumerate(self.config["btag_sf"]):
            central = weighter(wp, flav, eta, pt, discr, "central", efficiency)
            if not never_sys and self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                if "btag_splitting_scheme" in self.config:
                    scheme = self.config["btag_splitting_scheme"].lower()
                elif ("split_btag_year_corr" in self.config and
                        self.config["split_btag_year_corr"]):
                    scheme = "years"
                else:
                    scheme = None
                if scheme is None:
                    light_unc_splits = heavy_unc_splits = {"": ""}
                elif scheme == "years":
                    light_unc_splits = heavy_unc_splits = \
                        {"corr": "_correlated", "uncorr": "_uncorrelated"}
                elif scheme == "sources":
                    heavy_unc_splits = {name: f"_{name}"
                                        for name in weighter.sources}
                    light_unc_splits = {"corr": "_correlated",
                                        "uncorr": "_uncorrelated"}
                else:
                    raise ValueError(
                        f"Invalid btag uncertainty scheme {scheme}")

                for name, split in heavy_unc_splits.items():
                    systematics[f"btagsf{i}" + name] = self.compute_btag_sys(
                        central, "heavy up" + split, "heavy down" + split,
                        weighter, wp, flav, eta, pt, discr, efficiency)
                for name, split in light_unc_splits.items():
                    systematics[f"btagsf{i}light" + name] = \
                        self.compute_btag_sys(
                            central, "light up" + split, "light down" + split,
                            weighter, wp, flav, eta, pt, discr, efficiency)
            weight = weight * central
        if never_sys:
            return weight
        else:
            return weight, systematics

    def do_top_pt_reweighting(self, data):
        """Top pt reweighting according to
        https://twiki.cern.ch/twiki/bin/view/CMS/TopPtReweighting
        """
        pt = data["gent_lc"].pt
        weiter = self.config["top_pt_reweighting"]
        rwght = weiter(pt[:, 0], pt[:, 1])
        if weiter.sys_only:
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                return np.full(len(data), True), {"Top_pt_reweighting": rwght}
            else:
                return np.full(len(data), True)
        else:
            if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
                return rwght, {"Top_pt_reweighting": 1/rwght}
            else:
                return rwght

    def do_pileup_reweighting(self, dsname, data):
        """Pileup reweighting"""
        ntrueint = data["Pileup"]["nTrueInt"]
        weighter = self.config["pileup_reweighting"]
        weight = weighter(dsname, ntrueint)
        if self.config["compute_systematics"] and not self.config["only_nonprompt_syst"]:
            # If central is zero, let up and down factors also be zero
            weight_nonzero = np.where(weight == 0, np.inf, weight)
            up = weighter(dsname, ntrueint, "up")
            down = weighter(dsname, ntrueint, "down")
            sys = {"pileup": (up / weight_nonzero, down / weight_nonzero)}
            return weight, sys
        return weight
