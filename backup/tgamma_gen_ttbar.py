# This file illustrates how to implement a processor, realizing the selection
# steps and outputting histograms and a cutflow with efficiencies.

import pepper
import awkward as ak
import logging
from functools import partial
from copy import copy
import numpy as np
from tools.utils import DeltaR

logger = logging.getLogger(__name__)

class Processor(pepper.ProcessorBasicPhysics):
    config_class = pepper.ConfigTTbarLL
    def __init__(self, config, eventdir):
        super().__init__(config, eventdir)
        config["histogram_format"] = "root"
        
    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, definine objects and/or
        # compute event weights
        
        era = self.get_era(selector.data, is_mc)
        # Implement the selection steps: add cuts, definine objects and/or
        # compute event weights

        #MEPhotons
        if is_mc:
            selector.set_column("GenLepton", partial(self.build_genlepton_column, is_mc))
            selector.add_cut("filteronelepton", self.filter_onelepton)
            selector.set_cat("channel",{"ele", "muon"})
            selector.set_multiple_columns(self.lepton_cats)

            selector.set_column("GenPhoton", partial(self.build_genphoton_column, is_mc))
            selector.set_cat("genphoton_type", {'Prompt','Nonprompt'} )
            selector.set_multiple_columns(self.genphoton_categories)
            selector.add_cut("Cut_flow", self.loose_cut)

            selector.add_cut("removeSToverlap", partial(self.remove_stnlo_overlap,dsname))
            selector.add_cut("removeTToverlap", partial(self.remove_ttnlo_overlap,dsname))

            selector.set_column("GenJet",partial(self.build_genjet_column,is_mc))
            selector.set_column("GenbJet",partial(self.build_genbjet_column,is_mc))

            selector.add_cut("1Photon", self.one_gen_photon)
            selector.add_cut("3Jet",self.atLeast3jet)
            selector.add_cut("1bJet",self.atLeast1bjet)

            selector.add_cut("isolated", self.apply_genpho_isolation)
            
            selector.set_column("genDRphoLep",self.build_genDRphoLep)
            selector.set_column("genDRphoJet",self.build_genDRphoJet)
            selector.set_column("genDRphobJet",self.build_genDRphobJet)
            selector.set_column("genDRlepJet",self.build_genDRlepJet)
            selector.set_column("genDRlepbJet",self.build_genDRlepbJet)

        logger.debug("Selection done")

    def filter_onelepton(self,data):

        genlep = data["GenLepton"]

        #make sure they are the leptons from W decay
        mother = genlep.parent
        from_W = ( (ak.fill_none(abs(mother.pdgId)==24,False)) |
                   ((ak.fill_none(abs(mother.pdgId)==15,False) & ak.fill_none(abs(mother.parent.pdgId)==24,False))) )
        #genlep = genlep[from_W]

        return (ak.num(genlep) == 1)

    def lepton_cats(self,data):
        genlepton = data["GenLepton"]
        cat = {}
        cat['ele'] = abs(genlepton[:,0].pdgId)==11
        cat['muon'] = abs(genlepton[:,0].pdgId)==13

        return cat

    def one_gen_photon(self,data):
        genphoton = data["GenPhoton"]
        accept = (ak.num(genphoton) == 1)
        return accept

    def atLeast3jet(self,data):
        return ak.num(data["GenJet"])>=3
 
    def atLeast1bjet(self,data):
        return ak.num(data["GenbJet"])>=1
 
    def build_genjet_column(self, is_mc, data):

        genjet = data["GenJet"]
        genlepton = data["GenLepton"]
        genphoton = data["GenPhoton"]

        has_pt = (genjet["pt"]>30)
        has_eta = (abs(genjet["eta"])<2.6)

        has_lep_close = ak.any(genjet.metric_table(genlepton) < 0.4, axis=2) 
        has_pho_close = ak.any(genjet.metric_table(genphoton) < 0.1, axis=2) 

        genjet = genjet[(~has_lep_close) & (~has_pho_close) & has_pt & has_eta]

        return genjet

    def build_genbjet_column(self, is_mc, data):

        genjet = data["GenJet"]

        genlepton = data["GenLepton"]
        genphoton = data["GenPhoton"]

        has_pt = (genjet["pt"]>30)
        has_eta = (abs(genjet["eta"])<2.6)
        is_bjet = (abs(genjet.partonFlavour)==5)

        genbjet = genjet[has_pt & has_eta & is_bjet]

        has_lep_close = ak.any(genbjet.metric_table(genlepton) < 0.4, axis=2) 
        has_pho_close = ak.any(genbjet.metric_table(genphoton) < 0.1, axis=2) 

        genbjet = genbjet[(~has_lep_close) & (~has_pho_close)]

        return genbjet

    def build_genphoton_column(self, is_mc, data):

        genphoton = data["GenPart"]

        genphoton = genphoton[genphoton["pdgId"]==22]
        genphoton = genphoton[(genphoton["status"]==1)]

        #pt filtering just to speed it up
        has_pt = (genphoton["pt"]>20)
        has_eta = (abs(genphoton["eta"])<2.6)
        genphoton = genphoton[has_pt & has_eta]
 
        promptmatch = genphoton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | (genphoton.hasFlags(['isPromptTauDecayProduct'])) |
                        (genphoton.hasFlags(["fromHardProcess"])))

        mother = genphoton.parent
        not_from_top = (genphoton['pdgId']==22) #always from True 
        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
            mother = mother.parent

        mother = genphoton.parent
        from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
        from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
                 & (~not_from_top) )
        from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
                 & (~not_from_top) )

        from_decayProd = (from_lepton | from_W | from_b)

        genlepton = data["GenLepton"]
        has_lep_close = ak.any(genphoton.metric_table(genlepton) < 0.4, axis=2) 

        #genphoton = genphoton[~has_lep_close & promptmatch ]
        #genphoton = genphoton[~has_lep_close]

#        print("before sort",genphoton[ak.num(genphoton)>1].pt)

        genphoton = genphoton[ak.argsort(genphoton["pt"], ascending=False)]

#        print("after sort",genphoton[ak.num(genphoton)>1].pt,genphoton[ak.num(genphoton)>1].pt[2])

        return genphoton

    def build_genlepton_column(self, is_mc, data):

        genlepton = data["GenPart"]

        is_lepton = ( (abs(genlepton["pdgId"])==13) | (abs(genlepton["pdgId"])==11) )
        is_final_state = genlepton["status"]==1

        has_pt = genlepton["pt"]>30.
        has_eta = abs(genlepton["eta"])<2.5

        promptmatch = genlepton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | (genlepton.hasFlags(['isPromptTauDecayProduct'])) 
                        | (genlepton.hasFlags(["fromHardProcess"]))
		      )

        genlepton = genlepton[is_lepton & is_final_state & has_pt & has_eta & promptmatch]

#        print("before sort",genlepton[ak.num(genlepton)>1].pt)

        genlepton = genlepton[ak.argsort(genlepton["pt"], ascending=False)]

#        print("after sort",genlepton[ak.num(genlepton)>1].pt)

        return genlepton

    def build_genpart_column(self, is_mc, data):
        genpart = data["GenPart"]

        return genpart

    def genphoton_categories(self,data):

        cat = {}
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]

        promptmatch = genphoton.hasFlags(['isPrompt'])
        promptmatch = ( (promptmatch) | (genphoton.hasFlags(['isPromptTauDecayProduct'])) |
                        (genphoton.hasFlags(["fromHardProcess"])))    
        prompt_photon = genphoton[promptmatch]
        nonprompt_photon = genphoton[~promptmatch]
        
        cat['Prompt'] =  (ak.num(prompt_photon)>0)
        cat['Nonprompt'] = ( (ak.num(prompt_photon)==0) & (ak.num(nonprompt_photon)>0) )
 
        return cat

    def remove_ttnlo_overlap(self,dsname, data):
    
        if (not "TTTo" in dsname and not dsname.startswith("TTGJets") ) :
            accept = ak.num(data["GenLepton"]) > 0
            return accept
    
        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
   
        promptmatch = genphoton.hasFlags(['isPrompt'])

        mother = genphoton.parent
        not_from_top = (genphoton['pdgId']==22) #always from True 
        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
            mother = mother.parent

        mother = genphoton.parent
        from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
        from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
                 & (~not_from_top) )
        from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
                 & (~not_from_top) )

        from_decayProd = (from_lepton | from_W | from_b)

        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.05, axis=2)
    
        genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close]
    
        if "TTTo" in dsname :
            accept = (ak.num(genphoton)==0)
        elif dsname.startswith("TTGJets"):
            accept = (ak.num(genphoton)>0)
        return accept

    def remove_stnlo_overlap(slef,dsname, data):
        if (not "ST_t-channel" in dsname and not dsname.startswith("TGJets_lepton")) :
            accept = ak.num(data["GenLepton"]) > 0
            return accept

        genphoton = data["GenPhoton"]
        genpart = data["GenPart"]
  
        promptmatch = genphoton.hasFlags(['isPrompt'])

        mother = genphoton.parent
        not_from_top = (genphoton['pdgId']==22) #always from True 
        while not ak.all(ak.is_none(mother, axis=1)):
            not_from_top = (not_from_top & (ak.fill_none(abs(mother["pdgId"])!= 6, True) ))
            mother = mother.parent

        mother = genphoton.parent
        from_lepton = ( ( (ak.fill_none(abs(mother["pdgId"]), 0)==11) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==13) |
                          (ak.fill_none(abs(mother["pdgId"]), 0)==15) ) )
        from_W = ( (ak.fill_none(abs(mother["pdgId"]), 0)==24)
                 & (~not_from_top) )
        from_b = ( (ak.fill_none(abs(mother["pdgId"]), 0)==5)
                 & (~not_from_top) )

        from_decayProd = (from_lepton | from_W | from_b)

        #check if the photon is isolated from all final state particles
        #(excluding neutrinos and low pt particles and the photons)
        #is_relevant_part = ((genpart["pt"]>5.) & (genpart.hasFlags('isFirstCopy')) &
        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

        relevant_part = genpart[is_relevant_part]
    
        has_part_close = ak.any(
                genphoton.metric_table(relevant_part) < 0.05, axis=2)
    
        genphoton = genphoton[promptmatch & ~from_decayProd & ~has_part_close] 

        if "ST_t-channel" in dsname :
            accept = (ak.num(genphoton)==0)
        elif dsname.startswith("TGJets_lepton") :
            accept = (ak.num(genphoton)>0)
        return accept

    def build_genDRphoLep(self,data):

        genphoton = data["GenPhoton"]
        genlepton = data["GenLepton"]
        genDRphoLep = DeltaR(genphoton[:,0],genlepton[:,0])

        return genDRphoLep

    def build_genDRphoJet(self,data):

        genphoton = data["GenPhoton"]
        genjet = data["GenJet"]
        genDRphoJet = DeltaR(genphoton[:,0],genjet[:,0])

        return genDRphoJet

    def build_genDRphobJet(self,data):

        genphoton = data["GenPhoton"]
        genbjet = data["GenbJet"]
        genDRphobJet = DeltaR(genphoton[:,0],genbjet[:,0])

        return genDRphobJet

    def build_genDRlepJet(self,data):

        genlep = data["GenLepton"]
        genjet = data["GenbJet"]
        genDRlepJet = DeltaR(genlep[:,0],genjet[:,0])

        return genDRlepJet

    def build_genDRlepbJet(self,data):

        genlep = data["GenLepton"]
        genbjet = data["GenbJet"]
        genDRlepbJet = DeltaR(genlep[:,0],genbjet[:,0])

        return genDRlepbJet

    def apply_genpho_isolation(self, data):

        genphotons = data["GenPhoton"]
        genpart = data["GenPart"]

        is_relevant_part = ((genpart["pt"]>5.) & (genpart['status']==1) &
                        (abs(genpart["pdgId"])!=12) & (abs(genpart["pdgId"])!=14) & (abs(genpart["pdgId"])!=22) &
                        (abs(genpart["pdgId"])!=16))

        relevant_part = genpart[is_relevant_part]

        has_part_close = ak.any(
                genphotons.metric_table(relevant_part) < 0.1, axis=2)

        genlepton = data["GenLepton"]
        has_lep_close = ak.any(genphotons.metric_table(genlepton) < 0.4, axis=2)

        genphotons = genphotons[~has_part_close & ~has_lep_close]

        accept = (ak.num(genphotons)>0)

        return accept

    def loose_cut(self,data):

        genlep = data["GenLepton"]
        return (ak.num(genlep) >= 1)
