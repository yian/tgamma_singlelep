#!/bin/bash

source environment.sh

#For condor syst hists
#python3 -m pepper.runproc tgamma_photon_cat_processor.py config/config18.json --condor 2000 --condorinit ./environment.sh --out condor_output/syst -R


#For condor nominal per-event output
#python3 -m pepper.runproc tgamma_processor.py config/config18.json --condor 20 --condorinit ./environment.sh --out condor_normal_output -R

#For condor nomial hist
#python3 -m pepper.runproc tgamma_nonprompt_processor.py config/config18.json --condor 2500 --condorinit ./environment.sh --mc --out condor_nonprompt_photon -R 

#python3 -m pepper.runproc tgamma_nonprompt_processor.py config/config18.json --condor 1500 --condorinit ./environment.sh --dataset EGamma --out condor_nonprompt_photon -R 

#python3 -m pepper.runproc tgamma_nonprompt_processor.py config/config18.json --condor 1000 --condorinit ./environment.sh --dataset SingleMuon --out condor_nonprompt_photon -R 

#python3 -m pepper.runproc processor_btagging.py config/config_btag18.json --condor 500 --condorinit ./environment.sh --out condor_btag -R --mc

#python3 -m pepper.runproc tgamma_photon_overlap.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8 --out condor_overlap_output -R
#python3 -m pepper.runproc tgamma_photon_overlap.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset TTGamma_SingleLept_TuneCP5_13TeV-madgraph-pythia8 --out condor_overlap_output -R
#python3 -m pepper.runproc tgamma_photon_overlap.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8 --out condor_overlap_output -R
#
#python3 -m pepper.runproc tgamma_photon_overlap.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset TGJets_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8 --out condor_overlap_output -R 
#python3 -m pepper.runproc tgamma_photon_overlap.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out condor_overlap_output -R 
#python3 -m pepper.runproc tgamma_photon_overlap.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out condor_overlap_output -R 

#python3 -m pepper.runproc processor_gencomparison.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8 --out condor_genphoton_output -R
#python3 -m pepper.runproc processor_gencomparison.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset TTGamma_SingleLept_TuneCP5_13TeV-madgraph-pythia8 --out condor_genphoton_output -R
#python3 -m pepper.runproc processor_gencomparison.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8 --out condor_genphoton_output -R
#
#python3 -m pepper.runproc processor_gencomparison.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset TGJets_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8 --out condor_genphoton_output -R 
#python3 -m pepper.runproc processor_gencomparison.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out condor_genphoton_output -R 
#python3 -m pepper.runproc processor_gencomparison.py config/config_photon_cat18.json --condor 100 --condorinit ./environment.sh --dataset ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out condor_genphoton_output -R 

#python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json  --condor 100 --condorinit ./environment.sh  --dataset TTGamma_SingleLept_TuneCP5_13TeV-madgraph-pythia8 --out condor_origin_update -R

#python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json  --condor 100 --condorinit ./environment.sh  --dataset TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8 --out condor_origin_update -R 

#python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json  --condor 100 --condorinit ./environment.sh  --dataset TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8 --out condor_origin_update -R

python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json  --condor 100 --condorinit ./environment.sh  --dataset TGJets_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8 --out condor_origin_update -R 

python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json  --condor 100 --condorinit ./environment.sh  --dataset ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out condor_origin_update -R 

python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json   --condor 100 --condorinit ./environment.sh --dataset ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out condor_origin_update -R 

#For testing whether condor works
#python3 -m pepper.runproc tgamma_processor.py config/config18.json --condor 1 --condorinit ./environment.sh --dataset TGJets_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8 -R --out condor_output --eventdir condor_output -d
#python3 -m pepper.runproc tgamma_photon_cat_processor.py config/config_photon_cat18.json --condor 20 --condorinit ./environment.sh -R --out condor_output --eventdir condor_output -d

#For local test
#python3 -m pepper.runproc tgamma_photon_overlap.py config/config18.json  --dataset TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8 --out test_output -R -d
#python3 -m pepper.runproc tgamma_photon_overlap.py config/config18.json  --dataset TTGamma_SingleLept_TuneCP5_13TeV-madgraph-pythia8 --out test_output -R -d
#python3 -m pepper.runproc tgamma_photon_overlap.py config/config18.json  --dataset TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8 --out test_output -R -d
#python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json  --dataset TGJets_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8 --out test_output -R -d
#python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json  --dataset ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out test_output -R -d
#python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json  --dataset ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out test_output -R -d
