#!/bin/bash

# don't need to change the good jet eta to 4.7 when calculate the btagging efficiency in the config file

source environment.sh

python3 -m pepper.runproc processor_btagging.py config/config_btag18.json --condor 500 --condorinit ./environment.sh --out condor_btag -R --mc
