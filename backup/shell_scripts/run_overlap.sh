#!/bin/bash

source environment.sh

#python3 -m pepper.runproc processor_gencomparison.py config/config_gencomparisons_tWa.json --condor 300 --condorinit ./environment.sh --out Beatriz_genoutput 

python3 -m pepper.runproc tgamma_overlap_check.py config/config18.json --condor 300 --condorinit ./environment.sh --dataset DYJetsToLL_M-10to50_TuneCP5_13TeV-amcatnloFXFX-pythia8 --dataset DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8 --dataset DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8 --dataset ZGToLLG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8 --out condor_Z_overlap_check -R

python3 -m pepper.runproc tgamma_overlap_check.py config/config18.json --condor 300 --condorinit ./environment.sh  --dataset ZGToLLG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8 --out condor_Z_overlap_check -R

#python3 -m pepper.runproc tgamma_gen.py config/config_gen18.json --condor 300 --condorinit ./environment.sh --dataset TGJets_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8 --dataset ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --dataset ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out condor_genphoton_status -R 

#python3 -m pepper.runproc tgamma_gen.py config/config_gen18.json --condor 100 --condorinit ./environment.sh --dataset ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out genphoton_output -R 

#python3 -m pepper.runproc tgamma_gen.py config/config_gen18.json --condor 100 --condorinit ./environment.sh --dataset ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out genphoton_output -R
