#!/bin/bash

source environment.sh

python3 -m pepper.runproc tgamma_fakerate_photon.py config/config_photon_fakerate.json --condor 1000 --condorinit ./environment.sh --dataset SingleMuon --out condor_fakerate_pho -R 

python3 -m pepper.runproc tgamma_fakerate_photon.py config/config_photon_fakerate.json --condor 1000 --condorinit ./environment.sh --out condor_fakerate_pho -R --mc

python3 -m pepper.runproc tgamma_fakerate_photon.py config/config_photon_fakerate.json --condor 1000 --condorinit ./environment.sh --dnataset EGamma --out condor_fakerate_pho -R 

python3 -m pepper.runproc tgamma_fakerate_lepton.py config/config_lepton_fakerate18.json --condor 1000 --condorinit ./environment.sh --out condor_lepton_fakerate -R --mc

python3 -m pepper.runproc tgamma_fakerate_lepton.py config/config_lepton_fakerate18.json --condor 1000 --condorinit ./environment.sh --out condor_lepton_fakerate -R --dataset SingleMuon
