#!/bin/bash

source environment.sh

python3 -m pepper.runproc tgamma_processor.py config/config18.json --condor 1000 --condorinit ./environment.sh --out condor_normal -R --mc

python3 -m pepper.runproc tgamma_processor.py config/config18.json --condor 1000 --condorinit ./environment.sh --dataset EGamma --out condor_normal -R

python3 -m pepper.runproc tgamma_processor.py config/config18.json --condor 1000 --condorinit ./environment.sh --dataset SingleMuon --out condor_normal -R

