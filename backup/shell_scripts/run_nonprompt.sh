#!/bin/bash

source environment.sh

python3 -m pepper.runproc tgamma_nonprompt_ttbar_closure.py config/config18.json --condor 100 --condorinit ./environment.sh --out condor_nonprompt_ttbar_closure --dataset TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8 -R

python3 -m pepper.runproc tgamma_nonprompt_processor.py config/config18.json --condor 1000 --condorinit ./environment.sh --out condor_nonprompt -R --mc 

python3 -m pepper.runproc tgamma_nonprompt_processor.py config/config18.json --condor 1000 --condorinit ./environment.sh --out condor_nonprompt -R --dataset EGamma 

python3 -m pepper.runproc tgamma_nonprompt_processor.py config/config18.json --condor 1000 --condorinit ./environment.sh --out condor_nonprompt -R --dataset SingleMuon 


python3 -m pepper.runproc tgamma_lepton_nonprompt_processor.py config/config18.json --condor 1000 --condorinit ./environment.sh --out condor_lepton_nonprompt -R --mc 

python3 -m pepper.runproc tgamma_lepton_nonprompt_processor.py config/config18.json --condor 1000 --condorinit ./environment.sh --out condor_lepton_nonprompt -R --dataset SingleMuon 

