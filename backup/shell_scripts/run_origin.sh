#!/bin/bash

source environment.sh

#python3 -m pepper.runproc processor_gencomparison.py config/config_gencomparisons_tWa.json --condor 300 --condorinit ./environment.sh --out Beatriz_genoutput 

python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json --condor 300 --condorinit ./environment.sh --dataset TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8 --dataset TTGamma_SingleLept_TuneCP5_13TeV-madgraph-pythia8 --dataset TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8 --out condor_origin_gencol_status_new_remove_firstCopy -R

python3 -m pepper.runproc tgamma_photon_origin.py config/config18.json --condor 300 --condorinit ./environment.sh --dataset TGJets_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8 --dataset ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --dataset ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out condor_origin_gencol_status_new_remove_firstCopy -R

#python3 -m pepper.runproc tgamma_gen.py config/config_gen18.json --condor 300 --condorinit ./environment.sh --dataset TGJets_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8 --dataset ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --dataset ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out condor_genphoton_status -R 

#python3 -m pepper.runproc tgamma_gen.py config/config_gen18.json --condor 100 --condorinit ./environment.sh --dataset ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out genphoton_output -R 

#python3 -m pepper.runproc tgamma_gen.py config/config_gen18.json --condor 100 --condorinit ./environment.sh --dataset ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 --out genphoton_output -R
