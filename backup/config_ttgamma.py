import numpy as np
import uproot
import hjson
import coffea
from coffea import lookup_tools
from functools import partial

import pepper
from pepper.scale_factors import ScaleFactors

class ConfigTTGamma(pepper.ConfigBasicPhysics):
    def __init__(self, path_or_file, textparser=hjson.load, cwd="."):
        """Initialize the configuration.

        Arguments:
        path_or_file -- Either a path to the file containing the configuration
                        or a file-like object of it
        textparser -- Callable to be used to parse the text contained in
                      path_or_file
        cwd -- A path to use as the working directory for relative paths in the
               config. The actual working directory of the process might change
        """
        super().__init__(path_or_file, textparser, cwd)
        self.behaviors.update({
            "electron_sf": partial(
                self._get_scalefactors, dimlabels=["eta", "pt"]),
            "photon_sf": partial(
                self._get_scalefactors, dimlabels=["eta", "pt"]),
            "psv_sf": partial(
                self._get_scalefactors, dimlabels=["etabin"]),
            "fakephoton_sf": partial(
                self._get_scalefactors, dimlabels=["pt","abseta"]),
            "fakeMCcorrection_sf": partial(
                self._get_scalefactors, dimlabels=["pt","abseta"]),       
            "muon_sf": partial(
                self._get_scalefactors, dimlabels=["abseta","pt"]),
            "trigger_sfs":
                self._get_trigger_sfs,
            "hists": self._get_hists,
            "btag_sf16pre": self._get_btag_sf
        })
    
    def _get_scalefactors(self, value, dimlabels):
        sfs = []
        for sfpath in value:
            if not isinstance(sfpath, list) or len(sfpath) < 2:
                raise pepper.config.ConfigError(
                    "scale factors needs to be list of 2-element-lists in "
                    "form of [rootfile, histname]")
            with uproot.open(self._get_path(sfpath[0])) as f:
                hist = f[sfpath[1]]
            sf = ScaleFactors.from_hist(hist, dimlabels)
            sfs.append(sf)
        return sfs

    def _get_trigger_sfs(self, value):
        path, histnames = value
        ret = {}
        if len(histnames) != 3:
            raise pepper.config.ConfigError(
                "Need 3 histograms for trigger scale factors. Got "
                f"{len(histnames)}")
        with uproot.open(self._get_path(path)) as f:
            for chn, histname in zip(("ee", "em", "mm"), histnames):
                ret[chn] = ScaleFactors.from_hist(
                    f[histname], dimlabels=["lep1_pt", "lep2_pt"])
        return ret


